#include "Font.h"


CFont::CFont() 
{
	m_Font = nullptr;
	m_Texture = nullptr;
}
CFont::CFont(const CFont &other) {}
CFont::~CFont() {}

/// ----------------------------------------------------------------------
/// Loads the font data and the font texture.
/// ----------------------------------------------------------------------
bool CFont::Initialize(ID3D11Device *device, ID3D11DeviceContext *deviceContext,
	char *fontFilename, char *textureFilename)
{
	bool result;


	// Load in the text file containing the font data.
	result = LoadFontData(fontFilename);
	if (!result)
	{
		return false;
	}

	// Load the texture that has the font characters on it.
	result = LoadTexture(device, deviceContext, textureFilename);
	if (!result)
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Releases the font data and the font texture.
/// ----------------------------------------------------------------------
void CFont::Shutdown()
{
	// Release the font texture.
	ReleaseTexture();

	// Release the font data.
	ReleaseFontData();

	return;
}

/// ----------------------------------------------------------------------
/// Loads the fontdata.txt file which contains the indexing 
/// information for the texture.
/// ----------------------------------------------------------------------
bool CFont::LoadFontData(char *filename)
{
	ifstream fin;
	char input;

	// Create the font spacing buffer.
	m_Font = new SFont[95]; // number of characters in the texture
	if (!m_Font)
	{
		return false;
	}

	// Read in the font size and spacing between chars.
	fin.open(filename);
	if (fin.fail())
	{
		return false;
	}

	// Read in the 95 used ascii characters for text.
	for (int i = 0; i < 95; i++)
	{
		fin.get(input);

		while (input != ' ')
		{
			fin.get(input);
		}

		fin.get(input);

		while (input != ' ')
		{
			fin.get(input);
		}

		fin >> m_Font[i].left;
		fin >> m_Font[i].right;
		fin >> m_Font[i].size;
	}

	// Close the file. 
	fin.close();

	return true;
}

/// ----------------------------------------------------------------------
/// Releases the array that holds the texture indexing data.
/// ----------------------------------------------------------------------
void CFont::ReleaseFontData()
{
	// Release the font data array.
	if (m_Font)
	{
		delete[] m_Font;
		m_Font = nullptr;
	}

	return;
}

/// ----------------------------------------------------------------------
/// Reads in the font.tga file into the texture shader resource.
/// ----------------------------------------------------------------------
bool CFont::LoadTexture(ID3D11Device *device, ID3D11DeviceContext *deviceContext,
	char *filename)
{
	bool result;


	// Create the texture object.
	m_Texture = new CTexture;
	if (!m_Texture)
	{
		return false;
	}

	// Initialize the texture object.
	result = m_Texture->Initialize(device, deviceContext, filename);
	if (!result)
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Releases the texture that was used for the font.
/// ----------------------------------------------------------------------
void CFont::ReleaseTexture()
{
	// Release the texture object.
	if (m_Texture)
	{
		m_Texture->Shutdown();
		delete m_Texture;
		m_Texture = nullptr;
	}

	return;
}

ID3D11ShaderResourceView * CFont::GetTexture()
{
	return m_Texture->GetTexture();
}

/// ----------------------------------------------------------------------
/// Renders sentence given as an input to this function. drawX and drawY
/// are the screen coordinates
/// ----------------------------------------------------------------------
void CFont::BuildVertexArray(void* vertices, char *sentence,
	float drawX, float drawY)
{
	SVertex *vertexPtr;
	int numLetters, index, letter;

	// Coerce the input vertices into a SVertex structure
	vertexPtr = (SVertex*)vertices;

	// Get the number of letters in the sentence.
	numLetters = static_cast<int>(strlen(sentence));

	// Initialize the index to the vertex array
	index = 0;

	// Draw each letter onto a quad.
	for (int i = 0; i < numLetters; i++)
	{
		letter = (static_cast<int>(sentence[i] - 32));

		// If the letter is a space, then just move over three pixels.
		if (letter == 0)
		{
			drawX = drawX + 3.0f;
		}
		else
		{
			// First triangle in quad. Vertices:
			// Top left.
			vertexPtr[index].position = XMFLOAT3(drawX, drawY, 0.0f);	
			vertexPtr[index].texture = XMFLOAT2(m_Font[letter].left, 0.0f);
			index++;

			// Bottom right.
			vertexPtr[index].position = XMFLOAT3(drawX + m_Font[letter].size, drawY - 16, 0.0f);	
			vertexPtr[index].texture = XMFLOAT2(m_Font[letter].right, 1.0f);
			index++;

			// Bottom left.
			vertexPtr[index].position = XMFLOAT3(drawX, drawY - 16, 0.0f);
			vertexPtr[index].texture = XMFLOAT2(m_Font[letter].left, 1.0f);
			index++;

			// Second triangle in quad. Vertices:
			// Top left.
			vertexPtr[index].position = XMFLOAT3(drawX, drawY, 0.0f);	
			vertexPtr[index].texture = XMFLOAT2(m_Font[letter].left, 0.0f);
			index++;

			// Top right.
			vertexPtr[index].position = XMFLOAT3(drawX + m_Font[letter].size, drawY, 0.0f);
			vertexPtr[index].texture = XMFLOAT2(m_Font[letter].right, 0.0f);
			index++;

			// Bottom right.
			vertexPtr[index].position = XMFLOAT3(drawX + m_Font[letter].size, drawY - 16, 0.0f);
			vertexPtr[index].texture = XMFLOAT2(m_Font[letter].right, 1.0f);
			index++;

			// Update the x location for drawing by the size of the letter and one pixel.
			drawX = drawX + m_Font[letter].size + 1.0f;
		}
	}

	return;
}
