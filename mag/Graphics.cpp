#include "Graphics.h"

CGraphics::CGraphics() 
{
	m_readTimer     = true;
	m_modelTriangles = 0;
	m_elapsedTime = 0.0f;
	m_Direct3D		= nullptr;
	m_Camera		= nullptr;
	m_Text			= nullptr; 
	m_Grid			= nullptr;
	m_Model			= nullptr;
	m_GridShader	= nullptr;
	m_LightShader	= nullptr;
	m_Light			= nullptr;
}
CGraphics::CGraphics(const CGraphics &other) {}
CGraphics::~CGraphics() {}

/// ----------------------------------------------------------------------
/// Create the CDirect3D object and then call the CDirect3D Initialize function
/// ----------------------------------------------------------------------
bool CGraphics::Initialize(int screenWidth, int screenHeight, HWND hwnd) 
{
	bool result;
	XMMATRIX baseViewMatrix;

	// Create the Direct3D object.
	m_Direct3D = new CDirect3D;
	if (!m_Direct3D) { return false; }

	// Initialize the Direct3D object.
	result = m_Direct3D->Initialize(screenWidth, screenHeight, VSYNC_ENABLED,
		hwnd, FULLSCREEN, SCREEN_DEPTH, SCREEN_NEAR);
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize Direct3D", L"Error", MB_OK);
		return false;
	}



	// Create the camera object.
	m_Camera = new CCamera;
	if (!m_Camera) { return false; }

	// Initialize a base view matrix with the camera for 2D user interface rendering.
	m_Camera->SetPosition(0.0f, 0.0f, -10.0f);
	m_Camera->Render();
	m_Camera->GetViewMatrix(baseViewMatrix);

	/// ///////// ///
	/// INTERFACE ///
	/// ///////// ///
	
	// Create the texture shader object.
	m_Text = new CText;
	if (!m_Text) { return false; }

	// Initialize the text shader object.
	result = m_Text->Initialize(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(),
		hwnd, screenWidth, screenHeight, baseViewMatrix);
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize the text object.", L"Error", MB_OK);
		return false;
	}

	/// ///// ///
	/// SCENE ///
	/// ///// ///

	if (Config::Grid::SHOW)
	{
		// Create the grid object.
		m_Grid = new CGrid;
		if (!m_Grid) { return false; }

		// Initialize the grid object.
		result = m_Grid->Initialize(m_Direct3D->GetDevice());
		if (!result)
		{
			MessageBox(hwnd, L"Could not initialize the grid object.", L"Error", MB_OK);
			return false;
		}

		// Create the grid shader.
		m_GridShader = new CGridShader;
		if (!m_GridShader) { return false; }

		// Initialize the grid shader object.
		result = m_GridShader->Initialize(m_Direct3D->GetDevice(), hwnd);
		if (!result)
		{
			MessageBox(hwnd, L"Could not initialize the grid shader object.", L"Error", MB_OK);
			return false;
		}
	}

	// Create the model object.
	m_Model = new CModel;
	if (!m_Model) { return false; }

	// Initialize the model object.
	result = m_Model->Initialize(m_Direct3D->GetDevice(), m_Direct3D->GetDeviceContext(), 
		m_modelTriangles, m_elapsedTime);
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize the model object.", L"Error", MB_OK);
		return false;
	}		

	//// Create the light shader object.
	m_LightShader = new CLightShader;
	if (!m_LightShader) { return false; }

	//// Initialize the light shader object.
	result = m_LightShader->Initialize(m_Direct3D->GetDevice(), hwnd);
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize the light shader object.", L"Error", MB_OK);
		return false;
	}

	// Create the light object.
	m_Light = new CLight;
	if (!m_Light) { return false; }

	// Initialize the light object.
	m_Light->SetAmbientColor(0.15f, 0.15f, 0.15f, 1.0f);
	m_Light->SetDiffuseColor(1.0f, 1.0f, 1.0f, 1.0f);
	m_Light->SetDirection(1.0f, 0.0f, 0.0f);
		
	// Set the position from which the scene can be observed
	m_Camera->SetPosition(
		Config::Camera::STARTING_POSITION[0],	// x
		Config::Camera::STARTING_POSITION[1],	// y
		Config::Camera::STARTING_POSITION[2]);	// z

	return true;
}

/// ----------------------------------------------------------------------
/// Shut down all graphic objects
/// ----------------------------------------------------------------------
void CGraphics::Shutdown() 
{
	// Release the light object.
	if (m_Light)
	{
		delete m_Light;
		m_Light = nullptr;
	}

	// Release the color shader object.
	if (m_LightShader)
	{
		m_LightShader->Shutdown();
		delete m_LightShader;
		m_LightShader = nullptr;
	}

	// Release the model object.
	if (m_Model)
	{
		m_Model->Shutdown();
		delete m_Model;
		m_Model = nullptr;
	}

	if (Config::Grid::SHOW)
	{
		// Release the grid shader object.
		if (m_GridShader)
		{
			m_GridShader->Shutdown();
			delete m_GridShader;
			m_GridShader = nullptr;
		}

		// Release the grid object.
		if (m_Grid)
		{
			m_Grid->Shutdown();
			delete m_Grid;
			m_Grid = nullptr;
		}
	}

	// Release the text object.
	if (m_Text)
	{
		m_Text->Shutdown();
		delete m_Text;
		m_Text = nullptr;
	}

	// Release the camera object.
	if (m_Camera)
	{
		delete m_Camera;
		m_Camera = nullptr;
	}

	// Release the Direct3D object.
	if (m_Direct3D)
	{
		m_Direct3D->Shutdown();
		delete m_Direct3D;
		m_Direct3D = 0;
	}

	return;
}

/// ----------------------------------------------------------------------
///  Calls the Render function each frame
/// ----------------------------------------------------------------------
bool CGraphics::Frame(int mouseX, int mouseY,
	int dir, int mousemovementX, int mousemovementY,
	int fps, int cpu)
{
	bool result;
	char videoCard[128];
	int videoMemory;


	/// ///////// ///
	/// INTERFACE ///
	/// ///////// ///

	// Set video card info.
	m_Direct3D->GetVideoCardInfo(videoCard, videoMemory);
	result = m_Text->SetVideoCard(videoCard, videoMemory, m_Direct3D->GetDeviceContext());
	if (!result) { return false; }

	// Set the frames per second.
	result = m_Text->SetFPS(fps, m_Direct3D->GetDeviceContext());
	if (!result) { return false; }

	// Set the cpu usage.
	result = m_Text->SetCPU(cpu, m_Direct3D->GetDeviceContext());
	if (!result) { return false; }

	// Set the timer.
	m_Text->SetTimer(m_elapsedTime, m_Direct3D->GetDeviceContext());

	// Set the number of triangles. 
	m_Text->SetNumberOfTriangles(m_modelTriangles, m_Direct3D->GetDeviceContext());

	// Set the location of the mouse.
	result = m_Text->SetMousePosition(mouseX, mouseY, m_Direct3D->GetDeviceContext());
	if (!result) { return false; }

	/// //////// ///
	/// MOVEMENT ///
	/// //////// ///

	// Update the camera with set movement and rotation speeds.
	m_Camera->Update(mousemovementX, mousemovementY, dir);

	// Set the location and rotation of the camera.
	result = m_Text->SetPosRot(static_cast<int>(m_Camera->GetPosition().x), 
		static_cast<int>(m_Camera->GetPosition().y), static_cast<int>(m_Camera->GetPosition().z),
		static_cast<int>(m_Camera->GetRotation().x), static_cast<int>(m_Camera->GetRotation().y),
		static_cast<int>(m_Camera->GetRotation().z), m_Direct3D->GetDeviceContext());
	if (!result) { return false; }

	/// ///// ///
	/// SCENE ///
	/// ///// ///

	// Render the graphics scene.
	result = Render();
	if (!result) { return false; }


	return true;
}

/// ----------------------------------------------------------------------
/// Rendering!
/// ----------------------------------------------------------------------
bool CGraphics::Render() 
{
	XMMATRIX worldMatrix, viewMatrix, projectionMatrix, orthoMatrix;
	bool result;
	int i = 1;


	// Clear the buffers to begin the scene.
	m_Direct3D->BeginScene(0.0f, 0.0f, 0.0f, 1.0f);

	// Generate the view matrix based on the camera's position.
	m_Camera->Render();

	// Get the world, view, and projection matrices from the camera 
	// and direct3d objects.
	m_Camera->GetViewMatrix(viewMatrix);
	m_Direct3D->GetWorldMatrix(worldMatrix);
	m_Direct3D->GetProjectionMatrix(projectionMatrix);
	m_Direct3D->GetOrthoMatrix(orthoMatrix);
	
	/// ///// ///
	/// SCENE ///
	/// ///// ///
	if (Config::Grid::SHOW)
	{
		m_Grid->Render(m_Direct3D->GetDeviceContext());

		result = m_GridShader->Render(m_Direct3D->GetDeviceContext(),
			m_Grid->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix);
		if (!result) { return false; }
	}
	
	// Put the model vertex and index buffers on the graphics pipeline
	// to prepare them for drawing.
	m_Model->Render(m_Direct3D->GetDeviceContext());

	//// Render the model using the light shader.
	result = m_LightShader->Render(m_Direct3D->GetDeviceContext(),
		m_Model->GetIndexCount(),
		worldMatrix, viewMatrix, projectionMatrix,
		m_Model->GetTexture(),
		m_Light->GetDirection(), m_Light->GetAmbientColor(), m_Light->GetDiffuseColor());
	if (!result)
	{
		return false;
	}
	
	/// ///////// ///
	/// INTERFACE ///
	/// ///////// ///

	// Turn off the Z buffer to begin 2D rendering.
	m_Direct3D->TurnZBufferOff();

	// Turn on the alpha blending before rendering the text.
	m_Direct3D->TurnAlphaBlendingOn();

	// Render the text strings. viewMatrix to make the text independent of what 
	// happens to world matrix
	result = m_Text->Render(m_Direct3D->GetDeviceContext(), worldMatrix, orthoMatrix);
	if (!result)
	{
		return false;
	}

	// Turn off alpha blending after rendering the text.
	m_Direct3D->TurnAlphaBlendingOff();

	// Turn the Z buffer back on.
	m_Direct3D->TurnZBufferOn();

	// Present the rendered scene to the screen.
	m_Direct3D->EndScene();
	

	return true;
}