#include "FunctionParser.h"


CFunctionParser::CFunctionParser() {}
CFunctionParser::CFunctionParser(const CFunctionParser &) {}
CFunctionParser::~CFunctionParser() {}

/// ----------------------------------------------------------------------
/// Calculates whether given point satisfies the equation.
/// ----------------------------------------------------------------------
float CFunctionParser::Calculate(float x, float y, float z, string function)
{
	vector<string> args;
	vector<EXPR> exprs;
	vector<float> vals;
	vector<int> ids;
	bool isExpr;
	int id = 0; // argument ID


	GetArgumentsAndExpressions(args, exprs, function);

	// Check arguments
	for (string arg : args)
	{
		isExpr = false;

		// Check if the argument is a single variable, number or expression.
		for (int i = 0; i < arg.length(); i++)
		{
			if (arg[i] == '^' ||
				arg[i] == '*' ||
				arg[i] == '/' ||
				arg[i] == '+'/* ||
				arg[i] == '-'*/)
			{
				isExpr = true;
				break;
			}
		}

		// Fill non expression fields.
		if (!isExpr && arg[0] != '|')
		{
			switch (arg[0])
			{
			case 'x':
				vals.push_back(x);
				break;
			case 'y':
				vals.push_back(y);
				break;
			case 'z':
				vals.push_back(z);
				break;
			default:
				vals.push_back(stof(arg));
				break;
			}
		}
		else
		{
			// Calculate expression.
			// If there's absolute value to determine
			if (arg[0] == '|')
			{
				vals.push_back(abs(Calculate(x, y, z, StripFromAbsolute(arg))));
			}
			else
			{
				vals.push_back(Calculate(x, y, z, arg));
			}
		}

		// Prepare ID table
		ids.push_back(id++);
	}

	int temp;
	// Calculate powers.
	for (int i = 0; i < exprs.size(); i++)
	{
		if (exprs[i] == POW)
		{
			// Calculate expression.
			vals[ids[i]] = pow(vals[ids[i]], vals[ids[i + 1]]);

			// Apply changes to index table. 
			temp = ids[i + 1];
			for (int j = i + 1; ids[j] == temp;)
			{
				ids[j++] = ids[i];

				// Check if there are any more indexes to adjust.
				if (j >= ids.size())
				{
					break;
				}
			}
		}
	}

	// Calculate multiplications and divisions.
	for (int i = 0; i < exprs.size(); i++)
	{
		if (exprs[i] == MUL)
		{
			// Calculate expression.
			vals[ids[i]] *= vals[ids[i + 1]];

			// Apply changes to index table. 
			temp = ids[i + 1];
			for (int j = i + 1; ids[j] == temp;)
			{
				ids[j++] = ids[i];

				// Check if there are any more indexes to adjust.
				if (j >= ids.size())
				{
					break;
				}
			}
		}

		if (exprs[i] == DIV)
		{
			// Calculate expression.
			vals[ids[i]] /= vals[ids[i + 1]];

			// Apply changes to index table. 
			temp = ids[i + 1];
			for (int j = i + 1; ids[j] == temp;)
			{
				ids[j++] = ids[i];

				// Check if there are any more indexes to adjust.
				if (j >= ids.size())
				{
					break;
				}
			}
		}
	}

	// Calculate additions and subtractions.
	for (int i = 0; i < exprs.size(); i++)
	{
		if (exprs[i] == ADD)
		{
			// Calculate expression.
			vals[ids[i]] += vals[ids[i + 1]];

			// Apply changes to index table. 
			temp = ids[i + 1];
			for (int j = i + 1; ids[j] == temp;)
			{
				ids[j++] = ids[i];

				// Check if there are any more indexes to adjust.
				if (j >= ids.size())
				{
					break;
				}
			}
		}

		/// Probably DEPRECATED.
		// Subtraction is executed as addition of negative number.
		//if (exprs[i] == SUB)
		//{
		//	// Calculate expression.
		//	vals[ids[i]] -= vals[ids[i + 1]];

		//	// Apply changes to index table. 
		//	temp = ids[i + 1];
		//	for (int j = i + 1; ids[j] == temp;)
		//	{
		//		ids[j++] = ids[i];

		//		// Check if there are any more indexes to adjust.
		//		if (j >= ids.size())
		//		{
		//			break;
		//		}
		//	}
		//}
	}

	// Check if everything went ok. Algorithm should leave whole
	// array of indexes filled with zeros and the only significant
	// argument is the first one (i.e. arg[0])
	for (int i : ids)
	{
		if (i)
		{
			// ERROR 
			int k;
			k = 0;
		}
	}

	return vals[0];
}

/// ----------------------------------------------------------------------
/// Fills argument and expression vectors with proper data. For example:
/// x + (x - 2) * 3 + (x + 4) / (x - 5) is translated into vector of 
/// 5 arguments: [0]=x, [1]=x-2, [2]=3, [3]=x+4, [4]=x-5 and vector 
/// of 4 expressions: [0]=+, [1]=*, [2]=+, [3]=/.
/// ----------------------------------------------------------------------
void CFunctionParser::GetArgumentsAndExpressions(vector<string> &sv, vector<EXPR> &ev, string s)
{
	string temp;
	int par = 0; // Number of parentheses.
	int mod = 0;


	for (int i = 0; i < s.length(); i++)
	{
		// If there's an expression within parentheses, load it as a whole.
		if (s[i] == '(')
		{
			par++;
			while (par)
			{
				// Check if next character is the end of the string.
				if (++i >= s.length())
				{
					break;
				}

				// Check for other parentheses.
				if (s[i] == '(')
				{
					par++;
				}

				if (s[i] == ')')
				{
					par--;
				}

				// Load expression.
				if ((s[i] != '(' && s[i] != ')') || par > 0)
				{
					temp.push_back(s[i]);
				}

				// If all parentheses were parsed, go to the character after 
				// the last parenthesis.
				if (par == 0)
				{
					i++;
				}
			}
		}
		// If there's an expression within | lies, load it as a whole WITH |.
		if (s[i] == '|')
		{
			mod++;
			while (mod)
			{
				// Load expression.
				if (mod > 0)
				{
					temp.push_back(s[i]);
				}

				// Check if next character is the end of the string.
				if (++i >= s.length())
				{
					break;
				}

				// Check for other parentheses.
				if (s[i] == '|')
				{
					mod--;
				}

				// If all modulo-signs were parsed, save the last sign.
				if (mod == 0)
				{
					temp.push_back(s[i++]);
				}
			}
		}
		else
		{
			// If the character is a variable or number
			while (s[i] != '^' && s[i] != '*' && s[i] != '/' && s[i] != '+'/* && s[i] != '-'*/)
			{
				// Load the number.
				temp.push_back(s[i]);

				// Move to the next character.
				i++;

				// If the whole string is already parsed, move along.
				if (i >= s.length())
				{
					break;
				}

				// If next number is negative, move to the next argument
				if (s[i] == '-')
				{
					break;
				}
			}
		}

		// Load the argument.
		sv.push_back(temp);
		temp.clear();

		// Check if there's an expression to be loaded.
		if (i >= s.length())
		{
			break;
		}

		// Determine the expression type.
		switch (s[i])
		{
		case '^':
			ev.push_back(POW);
			break;
		case '*':
			ev.push_back(MUL);
			break;
		case '/':
			ev.push_back(DIV);
			break;
		case '+':
			ev.push_back(ADD);
			break;
		case '-':
			// Save argument as negative number
			i--;
			ev.push_back(/*SUB*/ADD);
			break;
		default:
			break;
		}
	}

	return;
}

/// ----------------------------------------------------------------------
/// Returns LEFT side of equation (strips right side).
/// DEPRECATED
/// ----------------------------------------------------------------------
string CFunctionParser::StripRight(string s)
{
	int i = static_cast<int>(s.length());


	while (s[--i] != '=')
	{
		s.pop_back();
	}
	s.pop_back();

	return s;
}

/// ----------------------------------------------------------------------
/// Returns RIGHT side of equation (strips left side).
/// DEPRECATED
/// ----------------------------------------------------------------------
string CFunctionParser::StripLeft(string s)
{
	string ns;
	int i = 0;


	while (s[i++] != '=') {}

	while (i < s.length())
	{
		ns.push_back(s[i++]);
	}

	return ns;
}

/// ----------------------------------------------------------------------
/// Strips the string from | characters
/// ----------------------------------------------------------------------
string CFunctionParser::StripFromAbsolute(string s)
{
	string ns;
	int i = 0;

	while (i < (s.length() - 2))
	{
		ns.push_back(s[i + 1]);
		i++;
	}

	return ns;
}