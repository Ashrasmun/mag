#pragma once

#include <d3d11.h>
#include <directxmath.h>
#include "Config.h"

using namespace std;
using namespace DirectX;
using namespace Config::Grid;

/// ----------------------------------------------------------------------
/// Grid in which all the data is calculated.
/// ----------------------------------------------------------------------
class CGrid
{
private:
	struct SVertex
	{
		XMFLOAT3 position;
		XMFLOAT4 color;
	};

public:
	CGrid();
	CGrid(const CGrid &);
	~CGrid();

	bool Initialize(ID3D11Device *);
	void Shutdown();
	void Render(ID3D11DeviceContext *);

	int GetIndexCount();
private:
	bool InitializeBuffers(ID3D11Device *);
	void ShutdownBuffers();
	void RenderBuffers(ID3D11DeviceContext *);

	void CreateVertex(SVertex *, unsigned long *, int &, float, float, float);

private:
	unsigned long int m_vertexCount, m_indexCount;
	ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;
};