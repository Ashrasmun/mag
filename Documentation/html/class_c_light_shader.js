var class_c_light_shader =
[
    [ "CLightShader", "class_c_light_shader.html#a7151ab39759177153ae8e7c757f8c292", null ],
    [ "CLightShader", "class_c_light_shader.html#ae2049c156a27fb838caf6bc15c7f0b09", null ],
    [ "~CLightShader", "class_c_light_shader.html#a24639ca8c36ee6725e211536281d566b", null ],
    [ "Initialize", "class_c_light_shader.html#af658a72b8fec226f95f06a414d813077", null ],
    [ "Render", "class_c_light_shader.html#a9a5c4a0e0396c41e3bd6f6153a388d33", null ],
    [ "Shutdown", "class_c_light_shader.html#a630a22f53e16a50cac4088c433c8f82d", null ]
];