#include "App.h"

/// ----------------------------------------------------------------------
/// Applications entry point
/// ----------------------------------------------------------------------
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow) 
{
	CApp *app;
	bool result;


	// Create the Application object.
	app = new CApp;
	if (!app) 
	{
		return 0;
	}

	// Initialize and run the Application object.
	result = app->Initialize();
	if (result) 
	{
		app->Run();
	}

	// Shutdown and release the Application object.
	app->Shutdown();
	delete app;
	app = nullptr;

	return 0;
}