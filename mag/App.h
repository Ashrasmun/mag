#pragma once

#define WIN32_LEAN_AND_MEAN 
// Speeds up the build process. It reduces the size of the Win32 header 
// files by excluding some of the less used APIs.

#include <windows.h>
#include "Config.h"
#include "Input.h"
#include "Graphics.h"
#include "FPS.h"
#include "CPU.h"
//#include "Timer.h"

/// ----------------------------------------------------------------------
/// Class that encapsulates whole application called within WinMain
/// ----------------------------------------------------------------------
class CApp 
{
public:
	CApp();
	CApp(const CApp&);
	~CApp();

	bool Initialize();
	void Shutdown();
	void Run();

	LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);

private:
	bool Frame();
	void InitializeWindows(int&, int&);
	void ShutdownWindows();

private:
	//float elapsedTime;

	LPCWSTR m_applicationName;
	HINSTANCE m_hinstance;
	HWND m_hwnd;

	CConfig		*m_Config;
	CInput		*m_Input;
	CGraphics	*m_Graphics;
	CFPS		*m_FPS;
	CCPU		*m_CPU;
	//CTimer		*m_Timer;
};

static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

static CApp *ApplicationHandle = nullptr;
// The WndProc function and ApplicationHandle pointer are also included 
// in this class file so we can re-direct the windows system messaging 
// into our MessageHandler function inside the system class.
