#include "Grid.h"

CGrid::CGrid() 
{
	m_vertexBuffer = nullptr;
	m_indexBuffer = nullptr;
}
CGrid::CGrid(const CGrid &other) {}
CGrid::~CGrid() {}


/// ----------------------------------------------------------------------
/// Initialization functions for the vertex and index buffers.
/// ----------------------------------------------------------------------
bool CGrid::Initialize(ID3D11Device *device)
{
	bool result;

	// Initialize the vertex and index buffers.
	result = InitializeBuffers(device);
	if (!result)
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Shutdown functions for the vertex and index buffers.
/// ----------------------------------------------------------------------
void CGrid::Shutdown()
{
	// Shutdown the vertex and index buffers.
	ShutdownBuffers();

	return;
}

/// ----------------------------------------------------------------------
/// Puts the vertex and index buffers on the graphics pipeline 
/// so the color shader will be able to render them.
/// ----------------------------------------------------------------------
void CGrid::Render(ID3D11DeviceContext *deviceContext)
{
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(deviceContext);

	return;
}

int CGrid::GetIndexCount()
{
	return m_indexCount;
}

/// ----------------------------------------------------------------------
/// Handles the process of creating the vertex and index buffers.
/// ----------------------------------------------------------------------
bool CGrid::InitializeBuffers(ID3D11Device *device)
{
	HRESULT hr;
	SVertex *vertices;
	unsigned long int *indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;

	// Calculate the number of vertices in the grid mesh.
	m_vertexCount = (unsigned long int)pow(RESOLUTION + 1, 2) * 3 * 2;

	// Set the index count to the same as the vertex count.
	m_indexCount = m_vertexCount;

	// Create the vertex array.
	vertices = new SVertex[m_vertexCount];
	if (!vertices)
	{
		return false;
	}

	// Create the index array.
	indices = new unsigned long[m_indexCount];
	if (!indices)
	{
		return false;
	}
	
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// Initialize the index to the vertex array.
	int index = 0;
	float s = SCALE; // Only to make the code more readable.


	// Load the vertex array and index array with data.
	// Each side should be populated with lines going through whole cube.
	// i -> x
	// j -> y
	// k -> z

	/// ///// ///
	/// FRONT ///
	/// ///// ///
	for (float j = 0; j < (RESOLUTION + 1) * s; j += s)
	{
		for (float i = 0; i < (RESOLUTION + 1) * s; i += s)
		{
			CreateVertex(vertices, indices, index, i, j, 0);
			CreateVertex(vertices, indices, index, i, j, RESOLUTION * s);
		}
	}

	/// ////// ///
	/// BOTTOM ///
	/// ////// ///
	for (float k = 0; k < (RESOLUTION + 1) * s; k += s) 
	{
		for (float i = 0; i < (RESOLUTION + 1) * s; i += s)
		{
			CreateVertex(vertices, indices, index, i, 0, k);
			CreateVertex(vertices, indices, index, i, RESOLUTION * s, k);
		}
	}

	/// ///////// ///
	/// LEFT SIDE ///
	/// ///////// ///
	for (float k = 0; k < (RESOLUTION + 1) * s; k += s)
	{
		for (float j = 0; j < (RESOLUTION + 1) * s; j += s)
		{
			CreateVertex(vertices, indices, index, 0, j, k);
			CreateVertex(vertices, indices, index, RESOLUTION * s, j, k);
		}
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// Set up the description of the static vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(SVertex) * m_vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now create the vertex buffer.
	hr = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if (FAILED(hr)) 
	{ 
		return false; 
	}

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * m_indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	hr = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if (FAILED(hr))
	{
		return false;
	}

	// Release the arrays now that the vertex and index buffers 
	// have been created and loaded.
	delete[] vertices;
	vertices = nullptr;

	delete[] indices;
	indices = nullptr;

	return true;
}

/// ----------------------------------------------------------------------
/// Releases the vertex buffer and index buffer.
/// ----------------------------------------------------------------------
void CGrid::ShutdownBuffers()
{
	// Release the index buffer.
	if (m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = nullptr;
	}

	// Release the vertex buffer.
	if (m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = nullptr;
	}

	return;
}

/// ----------------------------------------------------------------------
/// Sets the vertex buffer and index buffer as active on the 
/// input assembler in the GPU.
/// ----------------------------------------------------------------------
void CGrid::RenderBuffers(ID3D11DeviceContext *deviceContext)
{
	unsigned int stride;
	unsigned int offset;


	// Set vertex buffer stride and offset.
	stride = sizeof(SVertex);
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from 
	// this vertex buffer, in this case triangles.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);

	return;
}

/// ----------------------------------------------------------------------
/// Assembles data of singular vertex.
/// ----------------------------------------------------------------------
void CGrid::CreateVertex(SVertex *vertices, unsigned long *indices, int &index, 
	float x, float y, float z)
{
	// Grid should be in the center of the scene.
	float offset = ((float)RESOLUTION / 2) * SCALE * -1;

	x += offset;
	y += offset;
	z += offset;

	vertices[index].position = XMFLOAT3(x, y, z);
	vertices[index].color = XMFLOAT4(0.5f, 0.5f, 0.5f, 0.5f);
	indices[index] = index;
	index++;

	return;
}