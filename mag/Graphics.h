#pragma once

#include "Config.h"
#include "Direct3D.h"
#include "Camera.h"
#include "Text.h"
#include "Grid.h"
#include "Model.h"
#include "GridShader.h"
#include "LightShader.h"
#include "Light.h"

using namespace Config::Graphics;

/// ----------------------------------------------------------------------
/// All the graphics functionality in this application 
/// will be encapsulated in this class
/// ----------------------------------------------------------------------
class CGraphics
{
public:
	CGraphics();
	CGraphics(const CGraphics&);
	~CGraphics();

	bool Initialize(int, int, HWND);
	void Shutdown();
	bool Frame(int, int, 
		int, int, int, 
		int, int);

private:
	bool Render();

private:
	bool			m_readTimer;
	int				m_modelTriangles;
	float			m_elapsedTime;
	CDirect3D		*m_Direct3D;
	CCamera			*m_Camera;
	CText			*m_Text;
	CGrid			*m_Grid;
	CModel			*m_Model;
	CGridShader		*m_GridShader;
	CLightShader	*m_LightShader;
	CLight			*m_Light;
};