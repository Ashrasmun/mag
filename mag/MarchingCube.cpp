#include "MarchingCube.h"

CMarchingCube::CMarchingCube(){}
CMarchingCube::~CMarchingCube(){}

/// ----------------------------------------------------------------------
/// Checks whether polygons should be created in chosen cube in space or not,
/// and creates them. x, y, z are positions in data struct, not in the space!
/// ----------------------------------------------------------------------
bool CMarchingCube::Check(int x, int y, int z, int optionIndex
	/*vector< vector< vector<bool> > > data*/, vector< array<float, 8> > &triangles)
{
	//int optionIndex = 0;

	// Determine edge positions.
	// edges[i][0] = ue[i][0] - ((float)RESOLUTION / 2) * SCALE + x * SCALE;
	for (int i = 0; i < 12; i++)
	{
		// x
		edges[i][0] = ue[i][0] + (x - ((float)RESOLUTION / 2)) * SCALE;

		// y
		edges[i][1] = ue[i][1] + (y - ((float)RESOLUTION / 2)) * SCALE;

		// z
		edges[i][2] = ue[i][2] + (z - ((float)RESOLUTION / 2)) * SCALE;
	}

	// This vector has to be empty!
	/*if (triangles.size() != 0)
	{
		return false;
	}*/
	
	//// Determine the option.
	//// ZYX
	//if (data[x][y][z])
	//{
	//	optionIndex += 1;
	//}

	//if (data[x+1][y][z])
	//{
	//	optionIndex += 2;
	//}

	//if (data[x][y+1][z])
	//{
	//	optionIndex += 4;
	//}

	//if (data[x+1][y+1][z])
	//{
	//	optionIndex += 8;
	//}

	//if (data[x][y][z+1])
	//{
	//	optionIndex += 16;
	//}

	//if (data[x+1][y][z+1])
	//{
	//	optionIndex += 32;
	//}

	//if (data[x][y+1][z+1])
	//{
	//	optionIndex += 64;
	//}

	//if (data[x+1][y+1][z+1])
	//{
	//	optionIndex += 128;
	//}

	
	switch (optionIndex)
	{
		// Vertices creation starts from vertex with the lowest index.
		// Numbers next to case help recognize which points were marked
		// 0 is none, 1, 2, 3, 4, 5, 6, 7, 8 are specific points.
		// Corners are marked from 1 to 8, and edges from 0 to 11 !!
		// Please refer to marching_cubes2.jpg to check each of 14 cases 
		// (don't look at corner indexes!).

	case 0b00000000: // 0
		// There are no vertices to create.
		break;
		/// 1 ---------------------------------------------------------------
	case 0b00000001: // 1
		// Case 1
		// 0-4-3
		
		CreateTriangle(triangles, 0, 4, 3);

		break;
		/// 2 ---------------------------------------------------------------
	case 0b00000010: // 2
		// Case 1
		// 0-1-5

		CreateTriangle(triangles, 0, 1, 5);

		break;
	case 0b00000011: // 1+2
		// Case 2
		// 1-5-3
		// 3-5-4

		CreateTriangle(triangles, 1, 5, 3); 
		CreateTriangle(triangles, 3, 5, 4);
		
		break;
		/// 3 ---------------------------------------------------------------
	case 0b00000100: // 3
		// Case 1
		// 4-8-11

		CreateTriangle(triangles, 4, 8, 11);

		break;
	case 0b00000101: // 1+3
		// Case 2
		// 0-8-3
		// 3-8-11

		CreateTriangle(triangles, 0, 8, 3);
		CreateTriangle(triangles, 3, 8, 11);

		break;
	case 0b00000110: // 2+3
		// Case 3
		// 0-1-5
		// 4-8-11

		CreateTriangle(triangles, 0, 1, 5);
		CreateTriangle(triangles, 4, 8, 11);

		break;
	case 0b00000111: // 1+2+3
		// Case 5
		// 1-11-3
		// 1-5-11
		// 5-8-11

		CreateTriangle(triangles, 1, 11, 3);
		CreateTriangle(triangles, 1, 5, 11);
		CreateTriangle(triangles, 5, 8, 11);

		break;
		/// 4 ---------------------------------------------------------------
	case 0b00001000: // 4
		// Case 1
		// 5-9-8

		CreateTriangle(triangles, 5, 9, 8);
		
		break;
	case 0b00001001: // 1+4
		// Case 3
		// 0-4-3
		// 5-9-8

		CreateTriangle(triangles, 0, 4, 3);
		CreateTriangle(triangles, 5, 9, 8);

		break;
	case 0b00001010: // 2+4
		// Case 2
		// 0-1-8
		// 1-9-8

		CreateTriangle(triangles, 0, 1, 8);
		CreateTriangle(triangles, 1, 9, 8);

		break;
	case 0b00001011: // 1+2+4
		// Case 5
		// 1-9-3
		// 3-9-4
		// 4-9-8

		CreateTriangle(triangles, 1, 9, 3);
		CreateTriangle(triangles, 3, 9, 4);
		CreateTriangle(triangles, 4, 9, 8);

		break;
	case 0b00001100: // 3+4
		// Case 2
		// 4-5-11
		// 5-9-11

		CreateTriangle(triangles, 4, 5, 11);
		CreateTriangle(triangles, 5, 9, 11);

		break;
	case 0b00001101: // 1+3+4
		// 3-9-11
		// 0-9-3
		// 0-5-9

		CreateTriangle(triangles, 3, 9, 11);
		CreateTriangle(triangles, 0, 9, 3);
		CreateTriangle(triangles, 0, 5, 9);

		break;
	case 0b00001110: // 2+3+4
		// Case 5
		// 1-9-11 
		// 0-1-11
		// 0-11-4

		CreateTriangle(triangles, 1, 9, 11);
		CreateTriangle(triangles, 0, 1, 11);
		CreateTriangle(triangles, 0, 11, 4);

		break;
	case 0b00001111: // 1+2+3+4
		// Case 8
		// 1-9-3
		// 3-9-11

		CreateTriangle(triangles, 1, 9, 3);
		CreateTriangle(triangles, 3, 9, 11);

		break;
		/// 5 ---------------------------------------------------------------
	case 0b00010000: // 5
		// Case 1
		// 2-3-7

		CreateTriangle(triangles, 2, 3, 7);

		break;
	case 0b00010001: // 1+5
		// Case 2
		// 0-4-7
		// 0-7-2

		CreateTriangle(triangles, 0, 4, 7);
		CreateTriangle(triangles, 0, 7, 2);

		break;
	case 0b00010010: // 2+5
		// Case 3
		// 2-3-7
		// 0-1-5

		CreateTriangle(triangles, 2, 3, 7);
		CreateTriangle(triangles, 0, 1, 5);

		break;
	case 0b00010011: // 1+2+5
		// Case 5
		// 4-7-5
		// 2-5-7
		// 1-5-2

		CreateTriangle(triangles, 4, 7, 5);
		CreateTriangle(triangles, 2, 5, 7);
		CreateTriangle(triangles, 1, 5, 2);

		break;
	case 0b00010100: // 3+5
		// Case 3
		// 4-8-11
		// 2-3-7

		CreateTriangle(triangles, 4, 8, 11);
		CreateTriangle(triangles, 2, 3, 7);

		break;
	case 0b00010101: // 1+3+5
		// Case 5
		// 0-8-2
		// 2-8-11
		// 2-11-7

		CreateTriangle(triangles, 0, 8, 2);
		CreateTriangle(triangles, 2, 8, 11);
		CreateTriangle(triangles, 2, 11, 7);

		break;
	case 0b00010110: // 2+3+5
		// Case 7
		// 0-1-5
		// 4-8-11
		// 2-3-7

		CreateTriangle(triangles, 0, 1, 5);
		CreateTriangle(triangles, 4, 8, 11);
		CreateTriangle(triangles, 2, 3, 7);

		break;
	case 0b00010111: // 1+2+3+5
		// Case 9
		// 1-7-2
		// 1-11-7
		// 1-5-11
		// 5-8-11

		CreateTriangle(triangles, 1, 7, 2);
		CreateTriangle(triangles, 1, 11, 7);
		CreateTriangle(triangles, 1, 5, 11);
		CreateTriangle(triangles, 5, 8, 11);

		break;
	case 0b00011000: // 4+5
		// Case 4
		// 5-9-8
		// 2-3-7

		CreateTriangle(triangles, 5, 9, 8);
		CreateTriangle(triangles, 2, 3, 7);

		break;
	case 0b00011001: // 1+4+5
		// Case 6
		// 0-4-2
		// 2-4-7
		// 5-9-8
		
		CreateTriangle(triangles, 0, 4, 2);
		CreateTriangle(triangles, 2, 4, 7);
		CreateTriangle(triangles, 5, 9, 8);

		break;
	case 0b00011010: // 2+4+5
		// Case 6
		// 0-1-8
		// 1-9-8
		// 2-3-7

		CreateTriangle(triangles, 0, 1, 8);
		CreateTriangle(triangles, 1, 9, 8);
		CreateTriangle(triangles, 2, 3, 7);

		break;
	case 0b00011011: // 1+2+4+5
		// Case 14
		// 1-9-2
		// 2-9-6
		// 6-9-8
		// 2-6-7

		CreateTriangle(triangles, 1, 9, 2);
		CreateTriangle(triangles, 2, 9, 6);
		CreateTriangle(triangles, 6, 9, 8);
		CreateTriangle(triangles, 2, 6, 7);

		break;
	case 0b00011100: // 3+4+5
		// Case 6
		// 4-5-9
		// 4-9-11
		// 2-3-7

		CreateTriangle(triangles, 4, 5, 9);
		CreateTriangle(triangles, 4, 9, 11);
		CreateTriangle(triangles, 2, 3, 7);

		break;
	case 0b00011101: // 1+3+4+5
		// Case 14
		// 7-11-9
		// 0-2-7
		// 0-7-9
		// 0-9-5

		CreateTriangle(triangles, 7, 11, 9);
		CreateTriangle(triangles, 0, 2, 7);
		CreateTriangle(triangles, 0, 7, 9);
		CreateTriangle(triangles, 0, 9, 5);

		break;
	case 0b00011110: // 2+3+4+5
		// Case 12
		// 1-9-11
		// 1-11-4
		// 0-1-4
		// 2-3-7

		CreateTriangle(triangles, 1, 9, 11);
		CreateTriangle(triangles, 1, 11, 4);
		CreateTriangle(triangles, 0, 1, 4);
		CreateTriangle(triangles, 2, 3, 7);

		break;
	case 0b00011111: // 1+2+3+4+5
		// Case 5c
		// 1-9-11
		// 2-11-7
		// 1-11-2

		CreateTriangle(triangles, 1, 9, 11);
		CreateTriangle(triangles, 2, 11, 7);
		CreateTriangle(triangles, 1, 11, 2);

		break;
		/// 6 ---------------------------------------------------------------
	case 0b00100000: // 6
		// Case 1
		// 1-2-6

		CreateTriangle(triangles, 1, 2, 6);

		break;
	case 0b00100001: // 1+6
		// Case 3
		// 0-4-3
		// 1-2-6

		CreateTriangle(triangles, 0, 4, 3);
		CreateTriangle(triangles, 1, 2, 6);

		break;
	case 0b00100010: // 2+6
		// Case 2
		// 0-2-5
		// 2-6-5

		CreateTriangle(triangles, 0, 2, 5);
		CreateTriangle(triangles, 2, 6, 5);

		break;
	case 0b00100011: // 1+2+6
		// Case 5
		// 4-6-5
		// 3-6-4
		// 2-6-3

		CreateTriangle(triangles, 4, 6, 5);
		CreateTriangle(triangles, 3, 6, 4);
		CreateTriangle(triangles, 2, 6, 3);

		break;
	case 0b00100100: // 3+6
		// Case 4
		// 4-8-11
		// 1-2-6

		CreateTriangle(triangles, 4, 8, 11);
		CreateTriangle(triangles, 1, 2, 6);

		break;
	case 0b00100101: // 1+3+6
		// Case 6
		// 0-8-3
		// 3-8-11
		// 1-2-6

		CreateTriangle(triangles, 0, 8, 3);
		CreateTriangle(triangles, 3, 8, 11);
		CreateTriangle(triangles, 1, 2, 6);

		break;
	case 0b00100110: // 2+3+6
		// Case 6
		// 0-2-5
		// 2-6-5
		// 4-11-8

		CreateTriangle(triangles, 0, 2, 5);
		CreateTriangle(triangles, 2, 6, 5);
		CreateTriangle(triangles, 4, 11, 8);

		break;
	case 0b00100111: // 1+2+3+6
		// Case 14
		// 2-11-3
		// 2-5-11
		// 5-8-11
		// 2-6-5

		CreateTriangle(triangles, 2, 11, 3);
		CreateTriangle(triangles, 2, 5, 11);
		CreateTriangle(triangles, 5, 8, 11);
		CreateTriangle(triangles, 2, 6, 5);

		break;
	case 0b00101000: // 4+6
		//
		// 5-9-8
		// 1-2-6

		CreateTriangle(triangles, 5, 9, 8);
		CreateTriangle(triangles, 1, 2, 6);

		break;
	case 0b00101001: // 1+4+6
		//
		// 0-4-3
		// 5-9-8
		// 1-2-6

		CreateTriangle(triangles, 0, 4, 3);
		CreateTriangle(triangles, 5, 9, 8);
		CreateTriangle(triangles, 1, 2, 6);

		break;
	case 0b00101010: // 2+4+6
		//
		// 0-2-8
		// 6-9-8
		// 2-6-8

		CreateTriangle(triangles, 0, 2, 8);
		CreateTriangle(triangles, 6, 9, 8);
		CreateTriangle(triangles, 2, 6, 8);

		break;
	case 0b00101011: // 1+2+4+6
		// 
		// 4-9-8
		// 3-9-4
		// 3-6-9
		// 2-6-3

		CreateTriangle(triangles, 4, 9, 8);
		CreateTriangle(triangles, 3, 9, 4);
		CreateTriangle(triangles, 3, 6, 9);
		CreateTriangle(triangles, 2, 6, 3);

		break;
	case 0b00101100: // 3+4+6
		//
		// 4-5-11
		// 5-9-11
		// 1-2-6

		CreateTriangle(triangles, 4, 5, 11);
		CreateTriangle(triangles, 5, 9, 11);
		CreateTriangle(triangles, 1, 2, 6);

		break;
	case 0b00101101: // 1+3+4+6
		//
		// 3-9-11
		// 0-9-3
		// 0-5-9
		// 1-2-6

		CreateTriangle(triangles, 3, 9, 11);
		CreateTriangle(triangles, 0, 9, 3);
		CreateTriangle(triangles, 0, 5, 9);
		CreateTriangle(triangles, 1, 2, 6);

		break;
	case 0b00101110: // 2+3+4+6
		//
		// 2-6-9
		// 2-9-11
		// 0-2-11
		// 0-11-4

		CreateTriangle(triangles, 2, 6, 9);
		CreateTriangle(triangles, 2, 9, 11);
		CreateTriangle(triangles, 0, 2, 11);
		CreateTriangle(triangles, 0, 11, 4);

		break;
	case 0b00101111: // 1+2+3+4+6
		//
		// 3-9-11
		// 2-9-3
		// 2-6-9

		CreateTriangle(triangles, 3, 9, 11);
		CreateTriangle(triangles, 2, 9, 3);
		CreateTriangle(triangles, 2, 6, 9);

		break;
	case 0b00110000: // 5+6
		//
		// 1-3-6
		// 3-7-6

		CreateTriangle(triangles, 1, 3, 6);
		CreateTriangle(triangles, 3, 7, 6);

		break;
	case 0b00110001: // 1+5+6
		//
		// 4-7-6
		// 0-4-6
		// 0-6-1

		CreateTriangle(triangles, 4, 7, 6);
		CreateTriangle(triangles, 0, 4, 6);
		CreateTriangle(triangles, 0, 6, 1);

		break;
	case 0b00110010: // 2+5+6
		//
		// 5-7-6
		// 0-7-5
		// 0-3-7

		CreateTriangle(triangles, 5, 7, 6);
		CreateTriangle(triangles, 0, 7, 5);
		CreateTriangle(triangles, 0, 3, 7);

		break;
	case 0b00110011: // 1+2+5+6
		//
		// 4-7-5
		// 5-7-6

		CreateTriangle(triangles, 4, 7, 5);
		CreateTriangle(triangles, 5, 7, 6);

		break;
	case 0b00110100: // 3+5+6
		//
		// 1-3-7
		// 1-7-6
		// 4-8-11

		CreateTriangle(triangles, 1, 3, 7);
		CreateTriangle(triangles, 1, 7, 6);
		CreateTriangle(triangles, 4, 8, 11);

		break;
	case 0b00110101: // 1+3+5+6
		//
		// 0-8-11
		// 0-11-7
		// 0-7-1
		// 1-7-6

		CreateTriangle(triangles, 0, 8, 11);
		CreateTriangle(triangles, 0, 11, 7);
		CreateTriangle(triangles, 0, 7, 1);
		CreateTriangle(triangles, 1, 7, 6);

		break;
	case 0b00110110: // 2+3+5+6
		// 
		// 5-7-6
		// 0-7-5
		// 0-3-7
		// 4-8-11

		CreateTriangle(triangles, 5, 7, 6);
		CreateTriangle(triangles, 0, 7, 5);
		CreateTriangle(triangles, 0, 3, 7);
		CreateTriangle(triangles, 4, 8, 11);

		break;
	case 0b00110111: // 1+2+3+5+6
		//
		// 5-7-6
		// 5-8-11
		// 5-11-7

		CreateTriangle(triangles, 5, 7, 6);
		CreateTriangle(triangles, 5, 8, 11);
		CreateTriangle(triangles, 5, 11, 7);

		break;
	case 0b00111000: // 4+5+6
		//
		// 1-3-6
		// 3-7-6
		// 5-8-9

		CreateTriangle(triangles, 1, 3, 6);
		CreateTriangle(triangles, 3, 7, 6);
		CreateTriangle(triangles, 5, 8, 9);

		break;
	case 0b00111001: // 1+4+5+6
		//
		// 4-7-6
		// 0-4-6
		// 0-6-1
		// 5-8-9

		CreateTriangle(triangles, 4, 7, 6);
		CreateTriangle(triangles, 0, 4, 6);
		CreateTriangle(triangles, 0, 6, 1);
		CreateTriangle(triangles, 5, 8, 9);

		break;
	case 0b00111010: // 2+4+5+6
		//
		// 0-3-8
		// 3-7-8
		// 6-8-7
		// 6-9-8

		CreateTriangle(triangles, 0, 3, 8);
		CreateTriangle(triangles, 3, 7, 8);
		CreateTriangle(triangles, 6, 8, 7);
		CreateTriangle(triangles, 6, 9, 8);

		break;
	case 0b00111011: // 1+2+4+5+6
		//
		// 4-7-6
		// 4-6-8
		// 6-9-8

		CreateTriangle(triangles, 4, 7, 6);
		CreateTriangle(triangles, 4, 6, 8);
		CreateTriangle(triangles, 6, 9, 8);

		break;
	case 0b00111100: // 3+4+5+6
		//
		// 4-5-11
		// 5-9-11
		// 1-3-7
		// 1-7-6

		CreateTriangle(triangles, 4, 5, 11);
		CreateTriangle(triangles, 5, 9, 11);
		CreateTriangle(triangles, 1, 3, 7);
		CreateTriangle(triangles, 1, 7, 6);

		break;
	case 0b00111101: // 1+3+4+5+6
		// Case 6c
		// 0-5-9
		// 0-9-11
		// 0-11-7
		// 0-7-6
		// 0-6-1

		CreateTriangle(triangles, 0, 5, 9);
		CreateTriangle(triangles, 0, 9, 11);
		CreateTriangle(triangles, 0, 11, 7);
		CreateTriangle(triangles, 0, 7, 6);
		CreateTriangle(triangles, 0, 6, 1);

		break;
	case 0b00111110: // 2+3+4+5+6, 1+7+8 c
		// Case 6c
		// 0-11-4
		// 0-9-11
		// 0-6-9
		// 0-7-6
		// 0-3-7

		CreateTriangle(triangles, 0, 11, 4);
		CreateTriangle(triangles, 0, 9, 11);
		CreateTriangle(triangles, 0, 6, 9);
		CreateTriangle(triangles, 0, 7, 6);
		CreateTriangle(triangles, 0, 3, 7);

		break;
	case 0b00111111: // 1+2+3+4+5+6
		//
		// 6-9-7
		// 7-9-11
		
		CreateTriangle(triangles, 6, 9, 7);
		CreateTriangle(triangles, 7, 9, 11);

		break;
		/// 7 ---------------------------------------------------------------
	case 0b01000000: // 7
		//
		// 7-11-10
		
		CreateTriangle(triangles, 7, 11, 10);

		break;	
	case 0b01000001: // 1+7
		//
		// 0-4-3
		// 7-11-10

		CreateTriangle(triangles, 0, 4, 3);
		CreateTriangle(triangles, 7, 11, 10);

		break;
	case 0b01000010: // 2+7
		//
		// 0-1-5
		// 7-11-10

		CreateTriangle(triangles, 0, 1, 5);
		CreateTriangle(triangles, 7, 11, 10);

		break;
	case 0b01000011: // 1+2+7
		//
		// 1-3-5
		// 3-4-5
		// 7-11-10

		CreateTriangle(triangles, 1, 3, 5);
		CreateTriangle(triangles, 3, 4, 5);
		CreateTriangle(triangles, 7, 11, 10);

		break;
	case 0b01000100: // 3+7
		//
		// 4-8-7
		// 7-8-10

		CreateTriangle(triangles, 4, 8, 7);
		CreateTriangle(triangles, 7, 8, 10);

		break;
	case 0b01000101: // 1+3+7
		//
		// 0-8-10
		// 0-10-3
		// 3-10-7

		CreateTriangle(triangles, 0, 8, 10);
		CreateTriangle(triangles, 0, 10, 3);
		CreateTriangle(triangles, 3, 10, 7);

		break;
	case 0b01000110: // 2+3+7
		//
		// 0-1-5
		// 4-8-7
		// 7-8-10

		CreateTriangle(triangles, 0, 1, 5);
		CreateTriangle(triangles, 4, 8, 7);
		CreateTriangle(triangles, 7, 8, 10);

		break;
	case 0b01000111: // 1+2+3+7
		//
		// 7-8-10
		// 5-8-7
		// 3-5-7
		// 1-5-3

		CreateTriangle(triangles, 7, 8, 10);
		CreateTriangle(triangles, 5, 8, 7);
		CreateTriangle(triangles, 3, 5, 7);
		CreateTriangle(triangles, 1, 5, 3);

		break;
	case 0b01001000: // 4+7
		//
		// 5-9-8
		// 7-11-10

		CreateTriangle(triangles, 5, 9, 8);
		CreateTriangle(triangles, 7, 11, 10);

		break;
	case 0b01001001: // 1+4+7
		//
		// 0-4-3
		// 5-9-8
		// 7-11-10

		CreateTriangle(triangles, 0, 4, 3);
		CreateTriangle(triangles, 5, 9, 8);
		CreateTriangle(triangles, 7, 11, 10);

		break;
	case 0b01001010: // 2+4+7
		// 
		// 0-1-8
		// 1-9-8
		// 7-11-10

		CreateTriangle(triangles, 0, 1, 8);
		CreateTriangle(triangles, 1, 9, 8);
		CreateTriangle(triangles, 7, 11, 10);

		break;
	case 0b01001011: // 1+2+4+7
		//
		// 1-9-3
		// 3-9-8
		// 3-8-4

		CreateTriangle(triangles, 1, 9, 3);
		CreateTriangle(triangles, 3, 9, 8);
		CreateTriangle(triangles, 3, 8, 4);

		break;
	case 0b01001100: // 3+4+7
		//
		// 4-5-7
		// 5-9-10
		// 5-10-7
		
		CreateTriangle(triangles, 4, 5, 7);
		CreateTriangle(triangles, 5, 9, 10);
		CreateTriangle(triangles, 5, 10, 7);

		break;
	case 0b01001101: // 1+3+4+7
		//
		// 0-7-3
		// 0-5-7
		// 5-10-7
		// 5-9-10

		CreateTriangle(triangles, 0, 7, 3);
		CreateTriangle(triangles, 0, 5, 7);
		CreateTriangle(triangles, 5, 10, 7);
		CreateTriangle(triangles, 5, 9, 10);

		break;
	case 0b01001110: // 2+3+4+7
		//
		// 0-7-4
		// 0-1-9
		// 0-9-7
		// 7-9-10

		CreateTriangle(triangles, 0, 7, 4);
		CreateTriangle(triangles, 0, 1, 9);
		CreateTriangle(triangles, 0, 9, 7);
		CreateTriangle(triangles, 7, 9, 10);

		break;
	case 0b01001111: // 1+2+3+4+7, 5+6+8 C
		//
		// 1-9-3
		// 3-9-7
		// 9-10-7

		CreateTriangle(triangles, 1, 9, 3);
		CreateTriangle(triangles, 3, 9, 7);
		CreateTriangle(triangles, 9, 10, 7);

		break;
	case 0b01010000: // 5+7
		//
		// 2-3-11
		// 2-11-10

		CreateTriangle(triangles, 2, 3, 11);
		CreateTriangle(triangles, 2, 11, 10);

		break;
	case 0b01010001: // 1+5+7
		//
		// 0-10-2
		// 0-4-10
		// 4-11-10

		CreateTriangle(triangles, 0, 10, 2);
		CreateTriangle(triangles, 0, 4, 10);
		CreateTriangle(triangles, 4, 11, 10);
		
		break;
	case 0b01010010: // 2+5+7
		//
		// 0-1-5
		// 2-3-11
		// 2-11-10

		CreateTriangle(triangles, 0, 1, 5);
		CreateTriangle(triangles, 2, 3, 11);
		CreateTriangle(triangles, 2, 11, 10);

		break;
	case 0b01010011: // 1+2+5+7
		//
		// 1-5-2
		// 2-5-10
		// 5-11-10
		// 4-11-5

		CreateTriangle(triangles, 1, 5, 2);
		CreateTriangle(triangles, 2, 5, 10);
		CreateTriangle(triangles, 5, 11, 10);
		CreateTriangle(triangles, 4, 11, 5);

		break;
	case 0b01010100: // 3+5+7
		//
		// 2-8-10
		// 2-4-8
		// 2-3-4

		CreateTriangle(triangles, 2, 8, 10);
		CreateTriangle(triangles, 2, 4, 8);
		CreateTriangle(triangles, 2, 3, 4);

		break;
	case 0b01010101: // 1+3+5+7
		//
		// 0-8-2
		// 2-8-10

		CreateTriangle(triangles, 0, 8, 2);
		CreateTriangle(triangles, 2, 8, 10);

		break;
	case 0b01010110: // 2+3+5+7
		//
		// 0-1-5
		// 2-8-10
		// 0-8-2
		// 0-4-8

		CreateTriangle(triangles, 0, 1, 5);
		CreateTriangle(triangles, 2, 8, 10);
		CreateTriangle(triangles, 0, 8, 2);
		CreateTriangle(triangles, 0, 4, 8);

		break;
	case 0b01010111: // 1+2+3+5+7, 4+6+8 C
		//
		// 2-8-10
		// 1-5-2
		// 2-5-8

		CreateTriangle(triangles, 2, 8, 10);
		CreateTriangle(triangles, 1, 5, 2);
		CreateTriangle(triangles, 2, 5, 8);

		break;
	case 0b01011000: // 4+5+7
		//
		// 5-9-8
		// 2-3-11
		// 2-11-10

		CreateTriangle(triangles, 5, 9, 8);
		CreateTriangle(triangles, 2, 3, 11);
		CreateTriangle(triangles, 2, 11, 10);

		break;
	case 0b01011001: // 1+4+5+7
		//
		// 0-10-2
		// 0-4-10
		// 4-11-10
		// 5-9-8

		CreateTriangle(triangles, 0, 10, 2);
		CreateTriangle(triangles, 0, 4, 10);
		CreateTriangle(triangles, 4, 11, 10);
		CreateTriangle(triangles, 5, 9, 8);

		break;
	case 0b01011010: // 2+4+5+7
		//
		// 0-1-9
		// 0-9-8
		// 2-3-11
		// 2-11-10

		CreateTriangle(triangles, 0, 1, 9);
		CreateTriangle(triangles, 0, 9, 8);
		CreateTriangle(triangles, 2, 3, 11);
		CreateTriangle(triangles, 2, 11, 10);

		break;
	case 0b01011011: // 1+2+4+5+7, 3+6+8 C
		// Case 6c
		// 4-11-10
		// 4-10-2
		// 4-2-1
		// 4-1-9
		// 4-9-8

		CreateTriangle(triangles, 4, 11, 10);
		CreateTriangle(triangles, 4, 10, 2);
		CreateTriangle(triangles, 4, 2, 1);
		CreateTriangle(triangles, 4, 1, 9);
		CreateTriangle(triangles, 4, 9, 8);

		break;
	case 0b01011100: // 3+4+5+7
		//
		// 5-9-10
		// 5-10-4
		// 3-4-10
		// 2-3-10

		CreateTriangle(triangles, 5, 9, 10);
		CreateTriangle(triangles, 5, 10, 4);
		CreateTriangle(triangles, 3, 4, 10);
		CreateTriangle(triangles, 2, 3, 10);

		break;
	case 0b01011101: // 1+3+4+5+7, 2+6+8 C
		//
		// 0-10-2
		// 0-5-10
		// 5-9-10

		CreateTriangle(triangles, 0, 10, 2);
		CreateTriangle(triangles, 0, 5, 10);
		CreateTriangle(triangles, 5, 9, 10);

		break;
	case 0b01011110: // 2+3+4+5+7, 1+6+8 C
		// Case 6c 
		// 4-2-3
		// 4-10-2
		// 4-9-10
		// 4-1-9
		// 4-0-1

		CreateTriangle(triangles, 4, 2, 3);
		CreateTriangle(triangles, 4, 10, 2);
		CreateTriangle(triangles, 4, 9, 10);
		CreateTriangle(triangles, 4, 1, 9);
		CreateTriangle(triangles, 4, 0, 1);

		break;
	case 0b01011111: // 1+2+3+4+5+7, 6+8 C
		//
		// 1-9-2
		// 2-9-10

		CreateTriangle(triangles, 1, 9, 2);
		CreateTriangle(triangles, 2, 9, 10);

		break;
	case 0b01100000: // 6+7
		//
		// 1-2-6
		// 7-11-10

		CreateTriangle(triangles, 1, 2, 6);
		CreateTriangle(triangles, 7, 11, 10);

		break;
	case 0b01100001: // 1+6+7
		//
		// 0-4-3
		// 1-2-6
		// 7-11-10

		CreateTriangle(triangles, 0, 4, 3);
		CreateTriangle(triangles, 1, 2, 6);
		CreateTriangle(triangles, 7, 11, 10);

		break;
	case 0b01100010: // 2+6+7
		//
		// 0-2-5
		// 2-6-5
		// 7-11-10

		CreateTriangle(triangles, 0, 2, 5);
		CreateTriangle(triangles, 2, 6, 5);
		CreateTriangle(triangles, 7, 11, 10);

		break;
	case 0b01100011: // 1+2+6+7
		//
		// 4-6-5
		// 4-3-6
		// 2-6-3
		// 7-11-10

		CreateTriangle(triangles, 4, 6, 5);
		CreateTriangle(triangles, 4, 3, 6);
		CreateTriangle(triangles, 2, 6, 3);
		CreateTriangle(triangles, 7, 11, 10);

		break;
	case 0b01100100: // 3+6+7
		//
		// 4-8-7
		// 7-8-10
		// 1-2-6

		CreateTriangle(triangles, 4, 8, 7);
		CreateTriangle(triangles, 7, 8, 10);
		CreateTriangle(triangles, 1, 2, 6);

		break;
	case 0b01100101: // 1+3+6+7
		//
		// 0-8-10
		// 0-3-10
		// 3-7-10
		// 1-2-6

		CreateTriangle(triangles, 0, 8, 10);
		CreateTriangle(triangles, 0, 3, 10);
		CreateTriangle(triangles, 3, 7, 10);
		CreateTriangle(triangles, 1, 2, 6);

		break;
	case 0b01100110: // 2+3+6+7
		//
		// 4-8-7
		// 7-8-10
		// 0-2-5
		// 2-6-5

		CreateTriangle(triangles, 4, 8, 7);
		CreateTriangle(triangles, 7, 8, 10);
		CreateTriangle(triangles, 0, 2, 5);
		CreateTriangle(triangles, 2, 6, 5);

		break;
	case 0b01100111: // 1+2+3+6+7, 4+5+8 C
		// Case 6c
		// 3-10-7
		// 3-8-10
		// 3-5-8
		// 3-6-5
		// 3-2-6

		CreateTriangle(triangles, 3, 10, 7);
		CreateTriangle(triangles, 3, 8, 10);
		CreateTriangle(triangles, 3, 5, 8);
		CreateTriangle(triangles, 3, 6, 5);
		CreateTriangle(triangles, 3, 2, 6);

		break;
	case 0b01101000: // 4+6+7
		//
		// 5-9-8
		// 7-11-10
		// 1-2-6

		CreateTriangle(triangles, 5, 9, 8);
		CreateTriangle(triangles, 7, 11, 10);
		CreateTriangle(triangles, 1, 2, 6);

		break;
	case 0b01101001: // 1+4+6+7
		//
		// 0-4-3
		// 5-9-8
		// 7-11-10
		// 1-2-6

		CreateTriangle(triangles, 0, 4, 3);
		CreateTriangle(triangles, 5, 9, 8);
		CreateTriangle(triangles, 7, 11, 10);
		CreateTriangle(triangles, 1, 2, 6);

		break;
	case 0b01101010: // 2+4+6+7
		//
		// 0-2-8
		// 2-6-8
		// 6-9-8
		// 7-11-10

		CreateTriangle(triangles, 0, 2, 8);
		CreateTriangle(triangles, 2, 6, 8);
		CreateTriangle(triangles, 6, 9, 8);
		CreateTriangle(triangles, 7, 11, 10);

		break;
	case 0b01101011: // 1+2+4+6+7, 3+5+8 C
		//
		// 7-11-10
		// 4-10-11
		// 4-6-10
		// 3-6-4
		// 2-6-3
		
		CreateTriangle(triangles, 7, 11, 10);
		CreateTriangle(triangles, 4, 10, 11);
		CreateTriangle(triangles, 4, 6, 10);
		CreateTriangle(triangles, 3, 6, 4);
		CreateTriangle(triangles, 2, 6, 3);

		break;
	case 0b01101100: // 3+4+6+7
		//
		// 4-5-7
		// 5-9-7
		// 7-9-10
		// 1-2-6

		CreateTriangle(triangles, 4, 5, 7);
		CreateTriangle(triangles, 5, 9, 7);
		CreateTriangle(triangles, 7, 9, 10);
		CreateTriangle(triangles, 1, 2, 6);

		break;
	case 0b01101101: // 1+3+4+6+7, 2+5+8 C
		// 
		// 1-2-6
		// 0-7-3
		// 0-5-7
		// 5-10-7
		// 5-9-10

		CreateTriangle(triangles, 1, 2, 6);
		CreateTriangle(triangles, 0, 7, 3);
		CreateTriangle(triangles, 0, 5, 7);
		CreateTriangle(triangles, 5, 10, 7);
		CreateTriangle(triangles, 5, 9, 10);

		break;
	case 0b01101110: // 2+3+4+6+7, 1+5+8 C
		// Case 6c
		// 9-10-7
		// 9-7-4
		// 9-4-0
		// 9-0-2
		// 9-2-6

		CreateTriangle(triangles, 9, 10, 7);
		CreateTriangle(triangles, 9, 7, 4);
		CreateTriangle(triangles, 9, 4, 0);
		CreateTriangle(triangles, 9, 0, 2);
		CreateTriangle(triangles, 9, 2, 6);

		break;
	case 0b01101111: // 1+2+3+4+6+7, 5+8 C
		//
		// 3-9-10
		// 3-10-7
		// 2-9-3
		// 2-6-9

		CreateTriangle(triangles, 3, 9, 10);
		CreateTriangle(triangles, 3, 10, 7);
		CreateTriangle(triangles, 2, 9, 3);
		CreateTriangle(triangles, 2, 6, 9);

		break;
	case 0b01110000: // 5+6+7
		//
		// 1-3-11
		// 1-11-10
		// 1-10-6

		CreateTriangle(triangles, 1, 3, 11);
		CreateTriangle(triangles, 1, 11, 10);
		CreateTriangle(triangles, 1, 10, 6);

		break;
	case 0b01110001: // 1+5+6+7
		//
		// 0-6-1
		// 0-4-6
		// 4-10-6
		// 4-11-10

		CreateTriangle(triangles, 0, 6, 1);
		CreateTriangle(triangles, 0, 4, 6);
		CreateTriangle(triangles, 4, 10, 6);
		CreateTriangle(triangles, 4, 11, 10);

		break;
	case 0b01110010: // 2+5+6+7
		//
		// 0-3-11
		// 0-11-5
		// 5-11-10
		// 5-10-6

		CreateTriangle(triangles, 0, 3, 11);
		CreateTriangle(triangles, 0, 11, 5);
		CreateTriangle(triangles, 5, 11, 10);
		CreateTriangle(triangles, 5, 10, 6);

		break;
	case 0b01110011: // 1+2+5+6+7
		//
		// 4-6-5
		// 4-11-6
		// 6-11-10

		CreateTriangle(triangles, 4, 6, 5);
		CreateTriangle(triangles, 4, 11, 6);
		CreateTriangle(triangles, 6, 11, 10);

		break;
	case 0b01110100: // 3+5+6+7
		//
		// 4-8-10
		// 4-10-6
		// 3-4-6
		// 1-3-6

		CreateTriangle(triangles, 4, 8, 10);
		CreateTriangle(triangles, 4, 10, 6);
		CreateTriangle(triangles, 3, 4, 6);
		CreateTriangle(triangles, 1, 3, 6);

		break;
	case 0b01110101: // 1+3+5+6+7, 2+4+8 C
		//
		// 0-8-10
		// 0-10-1
		// 1-10-6

		CreateTriangle(triangles, 0, 8, 10);
		CreateTriangle(triangles, 0, 10, 1);
		CreateTriangle(triangles, 1, 10, 6);

		break;
	case 0b01110110: // 2+3+5+6+7, 1+4+8 C
		// Case 6c
		// 3-4-8
		// 3-8-10
		// 3-10-6
		// 3-6-5
		// 3-5-0

		CreateTriangle(triangles, 3, 4, 8);
		CreateTriangle(triangles, 3, 8, 10);
		CreateTriangle(triangles, 3, 10, 6);
		CreateTriangle(triangles, 3, 6, 5);
		CreateTriangle(triangles, 3, 5, 0);

		break;
	case 0b01110111: // 1+2+3+5+6+7, 4+8 C
		//
		// 5-8-10
		// 5-10-6

		CreateTriangle(triangles, 5, 8, 10);
		CreateTriangle(triangles, 5, 10, 6);

		break;
	case 0b01111000: // 4+5+6+7
		//
		// 5-9-8
		// 1-3-11
		// 1-11-10
		// 1-10-6

		CreateTriangle(triangles, 5, 9, 8);
		CreateTriangle(triangles, 1, 3, 11);
		CreateTriangle(triangles, 1, 11, 10);
		CreateTriangle(triangles, 1, 10, 6);

		break;
	case 0b01111001: // 1+4+5+6+7, 2+3+8 C
		//
		// 4-11-10
		// 4-10-6
		// 0-4-6
		// 0-6-1
		// 5-8-9

		CreateTriangle(triangles, 4, 11, 10);
		CreateTriangle(triangles, 4, 10, 6);
		CreateTriangle(triangles, 0, 4, 6);
		CreateTriangle(triangles, 0, 6, 1);
		CreateTriangle(triangles, 5, 8, 9);

		break;
	case 0b01111010: // 2+4+5+6+7, 1+3+8 C
		// Case 6c
		// 6-11-10
		// 6-3-11
		// 6-0-3
		// 6-8-0
		// 6-9-8

		CreateTriangle(triangles, 6, 11, 10);
		CreateTriangle(triangles, 6, 3, 11);
		CreateTriangle(triangles, 6, 0, 3);
		CreateTriangle(triangles, 6, 8, 0);
		CreateTriangle(triangles, 6, 9, 8);

		break;
	case 0b01111011: // 1+2+4+5+6+7, 3+8 C
		//
		// 4-11-10
		// 4-10-6
		// 4-9-8
		// 4-6-9

		CreateTriangle(triangles, 4, 11, 10);
		CreateTriangle(triangles, 4, 10, 6);
		CreateTriangle(triangles, 4, 9, 8);
		CreateTriangle(triangles, 4, 6, 9);

		break;
	case 0b01111100: // 3+4+5+6+7, 1+2+8 C
		// Case 6c
		// 10-5-9
		// 10-4-5
		// 10-3-4
		// 10-1-3
		// 10-6-1

		CreateTriangle(triangles, 10, 5, 9);
		CreateTriangle(triangles, 10, 4, 5);
		CreateTriangle(triangles, 10, 3, 4);
		CreateTriangle(triangles, 10, 1, 3);
		CreateTriangle(triangles, 10, 6, 1);

		break;
	case 0b01111101: // 1+3+4+5+6+7, 2+8 C
		//
		// 0-5-10
		// 5-9-10
		// 0-10-1
		// 1-10-6

		CreateTriangle(triangles, 0, 5, 10);
		CreateTriangle(triangles, 5, 9, 10);
		CreateTriangle(triangles, 0, 10, 1);
		CreateTriangle(triangles, 1, 10, 6);

		break;
	case 0b01111110: // 2+3+4+5+6+7, 1+8 C
		//
		// 0-3-4
		// 6-9-10

		CreateTriangle(triangles, 0, 3, 4);
		CreateTriangle(triangles, 6, 9, 10);

		break;
	case 0b01111111: // 1+2+3+4+5+6+7, 8 C
		//
	    // 6-9-10
		
		CreateTriangle(triangles, 6, 9, 10);

		break;
		/// 8 ---------------------------------------------------------------
	case 0b10000000: // 8
		//
		// 10-9-6

		CreateTriangle(triangles, 10, 9, 6);

		break;
	case 0b10000001: // 1+8
		//
		// 10-9-6
		// 0-4-3

		CreateTriangle(triangles, 10, 9, 6);
		CreateTriangle(triangles, 0, 4, 3);

		break;
	case 0b10000010: // 2+8
		//
		// 10-9-6
		// 0-1-5

		CreateTriangle(triangles, 10, 9, 6);
		CreateTriangle(triangles, 0, 1, 5);

		break;
	case 0b10000011: // 1+2+8
		//
		// 10-9-6
		// 1-5-3
		// 3-5-4

		CreateTriangle(triangles, 10, 9, 6);
		CreateTriangle(triangles, 1, 5, 3);
		CreateTriangle(triangles, 3, 5, 4);

		break;
	case 0b10000100: // 3+8
		//
		// 10-9-6
		// 4-8-11

		CreateTriangle(triangles, 10, 9, 6);
		CreateTriangle(triangles, 4, 8, 11);

		break;
	case 0b10000101: // 1+3+8
		//
		// 0-7-3
		// 3-8-11
		// 6-10-9

		CreateTriangle(triangles, 0, 7, 3);
		CreateTriangle(triangles, 3, 8, 11);
		CreateTriangle(triangles, 6, 10, 9);

		break;
	case 0b10000110: // 2+3+8
		//
		// 0-1-5
		// 4-8-11
		// 6-10-9

		CreateTriangle(triangles, 0, 1, 5);
		CreateTriangle(triangles, 4, 8, 11);
		CreateTriangle(triangles, 6, 10, 9);

		break;
	case 0b10000111: // 1+2+3+8
		//
		// 1-11-3
		// 1-8-11
		// 1-5-8
		// 6-10-9

		CreateTriangle(triangles, 1, 11, 3);
		CreateTriangle(triangles, 1, 8, 11);
		CreateTriangle(triangles, 1, 5, 8);
		CreateTriangle(triangles, 6, 10, 9);

		break;
	case 0b10001000: // 4+8
		//
		// 5-6-8
		// 6-10-8

		CreateTriangle(triangles, 5, 6, 8);
		CreateTriangle(triangles, 6, 10, 8);

		break;
	case 0b10001001: // 1+4+8
		//
		// 5-6-8
		// 6-10-8
		// 0-4-3

		CreateTriangle(triangles, 5, 6, 8);
		CreateTriangle(triangles, 6, 10, 8);
		CreateTriangle(triangles, 0, 4, 3);

		break;
	case 0b10001010: // 2+4+8
		//
		// 0-10-8
		// 0-1-10
		// 1-6-10

		CreateTriangle(triangles, 0, 10, 8);
		CreateTriangle(triangles, 0, 1, 10);
		CreateTriangle(triangles, 1, 6, 10);

		break;
	case 0b10001011: // 1+2+4+8
		//
		// 3-8-4
		// 1-6-3
		// 6-10-8
		// 3-6-8

		CreateTriangle(triangles, 3, 8, 4);
		CreateTriangle(triangles, 1, 6, 3);
		CreateTriangle(triangles, 6, 10, 8);
		CreateTriangle(triangles, 3, 6, 8);

		break;
	case 0b10001100: // 3+4+8
		//
		// 4-5-6
		// 4-6-11
		// 6-10-11

		CreateTriangle(triangles, 4, 5, 6);
		CreateTriangle(triangles, 4, 6, 11);
		CreateTriangle(triangles, 6, 10, 11);

		break;
	case 0b10001101: // 1+3+4+8
		//
		// 6-10-11
		// 0-6-11
		// 0-11-3
		// 0-5-6

		CreateTriangle(triangles, 6, 10, 11);
		CreateTriangle(triangles, 0, 6, 11);
		CreateTriangle(triangles, 0, 11, 3);
		CreateTriangle(triangles, 0, 5, 6);
		
		break;
	case 0b10001110: // 2+3+4+8
		//
		// 4-10-11
		// 0-10-4
		// 0-6-10
		// 0-1-6

		CreateTriangle(triangles, 4, 10, 11);
		CreateTriangle(triangles, 0, 10, 4);
		CreateTriangle(triangles, 0, 6, 10);
		CreateTriangle(triangles, 0, 1, 6);

		break;
	case 0b10001111: // 1+2+3+4+8, 5+6+7 C
		//
		// 1-11-3
		// 1-6-11
		// 6-10-11

		CreateTriangle(triangles, 1, 11, 3);
		CreateTriangle(triangles, 1, 6, 11);
		CreateTriangle(triangles, 6, 10, 11);

		break;
	case 0b10010000: // 5+8
		//
		// 6-10-9
		// 2-3-7

		CreateTriangle(triangles, 6, 10, 9);
		CreateTriangle(triangles, 2, 3, 7);

		break;
	case 0b10010001: // 1+5+8
		//
		// 0-4-2
		// 2-4-7
		// 6-10-9

		CreateTriangle(triangles, 0, 4, 2);
		CreateTriangle(triangles, 2, 4, 7);
		CreateTriangle(triangles, 6, 10, 9);

		break;
	case 0b10010010: // 2+5+8
		//
		// 0-1-5
		// 2-3-7
		// 6-10-9

		CreateTriangle(triangles, 0, 1, 5);
		CreateTriangle(triangles, 2, 3, 7);
		CreateTriangle(triangles, 6, 10, 9);

		break;
	case 0b10010011: // 1+2+5+8
		//
		// 4-7-5
		// 1-5-2
		// 2-5-7
		// 6-10-9

		CreateTriangle(triangles, 4, 7, 5);
		CreateTriangle(triangles, 1, 5, 2);
		CreateTriangle(triangles, 2, 5, 7);
		CreateTriangle(triangles, 6, 10, 9);

		break;
	case 0b10010100: // 3+5+8
		//
		// 4-8-11
		// 2-3-7
		// 6-10-9

		CreateTriangle(triangles, 4, 8, 11);
		CreateTriangle(triangles, 2, 3, 7);
		CreateTriangle(triangles, 6, 10, 9);

		break;
	case 0b10010101: // 1+3+5+8
		//
		// 0-8-2
		// 2-8-11
		// 2-11-7
		// 6-10-9

		CreateTriangle(triangles, 0, 8, 2);
		CreateTriangle(triangles, 2, 8, 11);
		CreateTriangle(triangles, 2, 11, 7);
		CreateTriangle(triangles, 6, 10, 9);

		break;
	case 0b10010110: // 2+3+5+8
		//
		// 1-5-0
		// 4-8-11
		// 2-3-7
		// 6-10-9

		CreateTriangle(triangles, 1, 5, 0);
		CreateTriangle(triangles, 4, 8, 11);
		CreateTriangle(triangles, 2, 3, 7);
		CreateTriangle(triangles, 6, 10, 9);

		break;
	case 0b10010111: // 1+2+3+5+8, 4+6+7 C
		//
		// 6-10-9
		// 7-8-11
		// 5-8-7
		// 2-5-7
		// 1-5-2

		CreateTriangle(triangles, 6, 10, 9);
		CreateTriangle(triangles, 7, 8, 11);
		CreateTriangle(triangles, 5, 8, 7);
		CreateTriangle(triangles, 2, 5, 7);
		CreateTriangle(triangles, 1, 5, 2);

		break;
	case 0b10011000: // 4+5+8
		//
		// 5-8-6
		// 6-8-10
		// 2-3-7

		CreateTriangle(triangles, 5, 8, 6);
		CreateTriangle(triangles, 6, 8, 10);
		CreateTriangle(triangles, 2, 3, 7);

		break;
	case 0b10011001: // 1+4+5+8
		//
		// 5-8-6
		// 6-8-10
		// 0-4-2
		// 2-4-7

		CreateTriangle(triangles, 5, 8, 6);
		CreateTriangle(triangles, 6, 8, 10);
		CreateTriangle(triangles, 0, 4, 2);
		CreateTriangle(triangles, 2, 4, 7);

		break;
	case 0b10011010: // 2+4+5+8
		//
		// 2-3-7
		// 0-10-8
		// 0-1-8
		// 1-6-10

		CreateTriangle(triangles, 2, 3, 7);
		CreateTriangle(triangles, 0, 10, 8);
		CreateTriangle(triangles, 0, 1, 8);
		CreateTriangle(triangles, 1, 6, 10);

		break;
	case 0b10011011: // 1+2+4+5+8, 3+6+7 C
		//
		// 4-7-8
		// 7-10-8
		// 1-6-2

		CreateTriangle(triangles, 4, 7, 8);
		CreateTriangle(triangles, 7, 10, 8);
		CreateTriangle(triangles, 1, 6, 2);

		break;
	case 0b10011100: // 3+4+5+8
		//
		// 2-3-7
		// 4-5-6
		// 4-6-11
		// 6-10-11

		CreateTriangle(triangles, 2, 3, 7);
		CreateTriangle(triangles, 4, 5, 6);
		CreateTriangle(triangles, 4, 6, 11);
		CreateTriangle(triangles, 6, 10, 11);

		break;
	case 0b10011101: // 1+3+4+5+8, 2+6+7 C
		//
		// 0-5-2
		// 2-5-6
		// 7-10-11

		CreateTriangle(triangles, 0, 5, 2);
		CreateTriangle(triangles, 2, 5, 6);
		CreateTriangle(triangles, 7, 10, 11);

		break;
	case 0b10011110: // 2+3+4+5+8, 1+6+7 C
		//
		// 0-3-4
		// 1-6-2
		// 7-10-11

		CreateTriangle(triangles, 0, 3, 4);
		CreateTriangle(triangles, 1, 6, 2);
		CreateTriangle(triangles, 7, 10, 11);

		break;
	case 0b10011111: // 1+2+3+4+5+8, 6+7 C
		//
		// 1-6-10
		// 1-10-11
		// 1-11-7
		// 1-7-2

		CreateTriangle(triangles, 1, 6, 10);
		CreateTriangle(triangles, 1, 10, 11);
		CreateTriangle(triangles, 1, 11, 7);
		CreateTriangle(triangles, 1, 7, 2);

		break;
	case 0b10100000: // 6+8
		//
		// 1-2-9
		// 2-10-9

		CreateTriangle(triangles, 1, 2, 9);
		CreateTriangle(triangles, 2, 10, 9);
		
		break;
	case 0b10100001: // 1+6+8
		//
		// 1-2-9
		// 2-10-9
		// 0-4-3
		
		CreateTriangle(triangles, 1, 2, 9);
		CreateTriangle(triangles, 2, 10, 9);
		CreateTriangle(triangles, 0, 4, 3);

		break;
	case 0b10100010: // 2+6+8
		//
		// 0-2-10
		// 5-10-9
		// 0-10-5

		CreateTriangle(triangles, 0, 2, 10);
		CreateTriangle(triangles, 5, 10, 9);
		CreateTriangle(triangles, 0, 10, 5);

		break;
	case 0b10100011: // 1+2+6+8
		//
		// 4-9-5
		// 2-4-9
		// 2-3-4
		// 2-10-9

		/// WRONG ///
		CreateTriangle(triangles, 4, 9, 5);
		CreateTriangle(triangles, 2, 4, 9);
		CreateTriangle(triangles, 2, 3, 4);
		CreateTriangle(triangles, 2, 10, 9);

		break;
	case 0b10100100: // 3+6+8
		//
		// 1-2-9
		// 2-10-9
		// 4-8-11

		CreateTriangle(triangles, 1, 2, 9);
		CreateTriangle(triangles, 2, 10, 9);
		CreateTriangle(triangles, 4, 8, 11);

		break;
	case 0b10100101: // 1+3+6+8
		//
		// 1-2-9
		// 2-10-9
		// 0-8-3
		// 3-8-11

		CreateTriangle(triangles, 1, 2, 9);
		CreateTriangle(triangles, 2, 10, 9);
		CreateTriangle(triangles, 0, 8, 3);
		CreateTriangle(triangles, 3, 8, 11);

		break;
	case 0b10100110: // 2+3+6+8
		//
		// 0-2-10
		// 5-10-9
		// 0-10-5
		// 4-8-11

		CreateTriangle(triangles, 0, 2, 10);
		CreateTriangle(triangles, 5, 10, 9);
		CreateTriangle(triangles, 0, 10, 5);
		CreateTriangle(triangles, 4, 8, 11);

		break;
	case 0b10100111: // 1+2+3+6+8, 4+5+7 C
		//
		// 5-8-9
		// 2-10-3
		// 3-10-11

		CreateTriangle(triangles, 5, 8, 9);
		CreateTriangle(triangles, 2, 10, 3);
		CreateTriangle(triangles, 3, 10, 11);

		break;
	case 0b10101000: // 4+6+8
		//
		// 2-10-8
		// 2-8-5
		// 1-2-5

		CreateTriangle(triangles, 2, 10, 8);
		CreateTriangle(triangles, 2, 8, 5);
		CreateTriangle(triangles, 1, 2, 5);

		break;
	case 0b10101001: // 1+4+6+8
		//
		// 2-10-8
		// 2-8-5
		// 1-2-5
		// 0-4-3

		CreateTriangle(triangles, 2, 10, 8);
		CreateTriangle(triangles, 2, 8, 5);
		CreateTriangle(triangles, 1, 2, 5);
		CreateTriangle(triangles, 0, 4, 3);

		break;
	case 0b10101010: // 2+4+6+8
		//
		// 0-2-8
		// 2-10-8

		CreateTriangle(triangles, 0, 2, 8);
		CreateTriangle(triangles, 2, 10, 8);

		break;
	case 0b10101011: // 1+2+4+6+8, 3+5+7 C
		//
		// 2-10-8
		// 2-8-4
		// 2-4-3

		CreateTriangle(triangles, 2, 10, 8);
		CreateTriangle(triangles, 2, 8, 4);
		CreateTriangle(triangles, 2, 4, 3);

		break;
	case 0b10101100: // 3+4+6+8
		// Case 14
		// 5-11-4
		// 1-2-5
		// 2-11-5
		// 2-10-11

		CreateTriangle(triangles, 5, 11, 4);
		CreateTriangle(triangles, 1, 2, 5);
		CreateTriangle(triangles, 2, 11, 5);
		CreateTriangle(triangles, 2, 10, 11);

		break;
	case 0b10101101: // 1+3+4+6+8, 2+5+7 C
		//
		// 2-11-3
		// 2-10-11
		// 0-5-1

		CreateTriangle(triangles, 2, 11, 3);
		CreateTriangle(triangles, 2, 10, 11);
		CreateTriangle(triangles, 0, 5, 1);

		break;
	case 0b10101110: // 2+3+4+6+8, 1+5+7 C
		//
		// 0-2-10
		// 0-11-4
		// 0-10-11

		CreateTriangle(triangles, 0, 2, 10);
		CreateTriangle(triangles, 0, 11, 4);
		CreateTriangle(triangles, 0, 10, 11);

		break;
	case 0b10101111: // 1+2+3+4+6+8, 5+7 C
		//
		// 2-11-3
		// 2-10-11

		CreateTriangle(triangles, 2, 11, 3);
		CreateTriangle(triangles, 2, 10, 11);

		break;
	case 0b10110000: // 5+6+8
		//
		// 1-3-9
		// 3-7-10
		// 3-10-9

		CreateTriangle(triangles, 1, 3, 9);
		CreateTriangle(triangles, 3, 7, 10);
		CreateTriangle(triangles, 3, 10, 9);

		break;
	case 0b10110001: // 1+5+6+8
		//
		// 0-4-7
		// 0-7-9
		// 0-9-1
		// 7-10-9

		CreateTriangle(triangles, 0, 4, 7);
		CreateTriangle(triangles, 0, 7, 9);
		CreateTriangle(triangles, 0, 9, 1);
		CreateTriangle(triangles, 7, 10, 9);

		break;
	case 0b10110010: // 2+5+6+8
		//
		// 0-3-7
		// 0-7-5
		// 5-7-10
		// 5-10-9

		CreateTriangle(triangles, 0, 3, 7);
		CreateTriangle(triangles, 0, 7, 5);
		CreateTriangle(triangles, 5, 7, 10);
		CreateTriangle(triangles, 5, 10, 9);

		break;
	case 0b10110011: // 1+2+5+6+8, 3+4+7 C
		//
		// 4-7-5
		// 5-7-9
		// 7-10-9

		CreateTriangle(triangles, 4, 7, 5);
		CreateTriangle(triangles, 5, 7, 9);
		CreateTriangle(triangles, 7, 10, 9);

		break;
	case 0b10110100: // 3+5+6+8
		//
		// 1-3-9
		// 3-7-9
		// 7-10-9
		// 4-8-11

		CreateTriangle(triangles, 1, 3, 9);
		CreateTriangle(triangles, 3, 7, 9);
		CreateTriangle(triangles, 7, 10, 9);
		CreateTriangle(triangles, 4, 8, 11);

		break;
	case 0b10110101: // 1+3+5+6+8, 2+4+7 C
		//
		// 0-1-8
		// 1-9-8
		// 7-10-11

		CreateTriangle(triangles, 0, 1, 8);
		CreateTriangle(triangles, 1, 9, 8);
		CreateTriangle(triangles, 7, 10, 11);

		break;
	case 0b10110110: // 2+3+5+6+8, 1+4+7 C
		//
		// 4-8-11
		// 0-3-7
		// 0-7-5
		// 5-7-10
		// 5-10-9

		CreateTriangle(triangles, 4, 8, 11);
		CreateTriangle(triangles, 0, 3, 7);
		CreateTriangle(triangles, 0, 7, 5);
		CreateTriangle(triangles, 5, 7, 10);
		CreateTriangle(triangles, 5, 10, 9);

		break;
	case 0b10110111: // 1+2+3+5+6+8, 4+7 C
		//
		// 5-8-11
		// 5-11-7
		// 5-7-10
		// 5-10-9

		CreateTriangle(triangles, 5, 8, 11);
		CreateTriangle(triangles, 5, 11, 7);
		CreateTriangle(triangles, 5, 7, 10);
		CreateTriangle(triangles, 5, 10, 9);

		break;
	case 0b10111000: // 4+5+6+8
		//
		// 3-11-8
		// 3-8-5
		// 2-3-5
		// 2-5-6

		CreateTriangle(triangles, 3, 11, 8);
		CreateTriangle(triangles, 3, 8, 5);
		CreateTriangle(triangles, 2, 3, 5);
		CreateTriangle(triangles, 2, 5, 6);

		break;
	case 0b10111001: // 1+4+5+6+8, 2+3+7 C
		// Case 6c
		// 1-0-4
		// 1-4-7
		// 1-7-10
		// 1-10-8
		// 1-8-5

		CreateTriangle(triangles, 1, 0, 4);
		CreateTriangle(triangles, 1, 4, 7);
		CreateTriangle(triangles, 1, 7, 10);
		CreateTriangle(triangles, 1, 10, 8);
		CreateTriangle(triangles, 1, 8, 5);

		break;
	case 0b10111010: // 2+4+5+6+8, 1+3+7 C
		//
		// 0-10-8
		// 0-3-10
		// 3-7-10

		CreateTriangle(triangles, 0, 10, 8);
		CreateTriangle(triangles, 0, 3, 10);
		CreateTriangle(triangles, 3, 7, 10);

		break;
	case 0b10111011: // 1+2+4+5+6+8, 3+7 C
		// 
		// 4-7-8
		// 8-7-10

		CreateTriangle(triangles, 4, 7, 8);
		CreateTriangle(triangles, 8, 7, 10);

		break;
	case 0b10111100: // 3+4+5+6+8, 1+2+7 C
		// Case 6c
		// 10-3-7
		// 10-1-3
		// 10-5-1
		// 10-4-5
		// 10-11-4

		CreateTriangle(triangles, 10, 3, 7);
		CreateTriangle(triangles, 10, 1, 3);
		CreateTriangle(triangles, 10, 5, 1);
		CreateTriangle(triangles, 10, 4, 5);
		CreateTriangle(triangles, 10, 11, 4);

		break;
	case 0b10111101: // 1+3+4+5+6+8, 2+7 C
		//
		// 0-1-5
		// 7-10-11

		CreateTriangle(triangles, 0, 1, 5);
		CreateTriangle(triangles, 7, 10, 11);

		break;
	case 0b10111110: // 2+3+4+5+6+8, 1+7 C
		// 
		// 0-11-4
		// 0-10-11
		// 0-3-7
		// 0-7-10

		CreateTriangle(triangles, 0, 11, 4);
		CreateTriangle(triangles, 0, 10, 11);
		CreateTriangle(triangles, 0, 3, 7);
		CreateTriangle(triangles, 0, 7, 10);

		break;
	case 0b10111111: // 1+2+3+4+5+6+8, 7 C
		//
		// 7-10-11

		CreateTriangle(triangles, 7, 10, 11);

		break;
	case 0b11000000: // 7+8
		//
		// 6-7-9
		// 7-11-9

		CreateTriangle(triangles, 6, 7, 9);
		CreateTriangle(triangles, 7, 11, 9);

		break;
	case 0b11000001: // 1+7+8
		//
		// 0-4-3
		// 6-7-9
		// 7-11-9

		CreateTriangle(triangles, 0, 4, 3);
		CreateTriangle(triangles, 6, 7, 9);
		CreateTriangle(triangles, 7, 11, 9);

		break;
	case 0b11000010: // 2+7+8
		//
		// 0-1-5
		// 6-7-9
		// 7-11-9

		CreateTriangle(triangles, 0, 1, 5);
		CreateTriangle(triangles, 6, 7, 9);
		CreateTriangle(triangles, 7, 11, 9);

		break;
	case 0b11000011: // 1+2+7+8
		//
		// 1-3-5
		// 3-4-5
		// 6-9-7
		// 7-9-11

		CreateTriangle(triangles, 1, 3, 5);
		CreateTriangle(triangles, 3, 4, 5);
		CreateTriangle(triangles, 6, 9, 7);
		CreateTriangle(triangles, 7, 9, 11);

		break;
	case 0b11000100: // 3+7+8
		//
		// 4-6-7
		// 4-8-6
		// 6-8-9

		CreateTriangle(triangles, 4, 6, 7);
		CreateTriangle(triangles, 4, 8, 6);
		CreateTriangle(triangles, 6, 8, 9);

		break;
	case 0b11000101: // 1+3+7+8
		//
		// 6-7-9
		// 7-8-9
		// 3-8-9
		// 0-8-3

		/// WRONG

		CreateTriangle(triangles, 6, 7, 9);
		CreateTriangle(triangles, 7, 8, 9);
		CreateTriangle(triangles, 3, 8, 9);
		CreateTriangle(triangles, 0, 8, 3);

		break;
	case 0b11000110: // 2+3+7+8
		//
		// 0-1-5
		// 4-6-7
		// 4-8-6
		// 6-8-9

		CreateTriangle(triangles, 0, 1, 5);
		CreateTriangle(triangles, 4, 6, 7);
		CreateTriangle(triangles, 4, 8, 6);
		CreateTriangle(triangles, 6, 8, 9);

		break;
	case 0b11000111: // 1+2+3+7+8, 4+5+6 C
		// Case 6c
		// 8-9-6
		// 8-6-7
		// 8-7-3
		// 8-3-1
		// 8-1-5

		CreateTriangle(triangles, 8, 9, 6);
		CreateTriangle(triangles, 8, 6, 7);
		CreateTriangle(triangles, 8, 7, 3);
		CreateTriangle(triangles, 8, 3, 1);
		CreateTriangle(triangles, 8, 1, 5);

		break;
	case 0b11001000: // 4+7+8
		//
		// 5-6-7
		// 5-7-8
		// 7-11-8

		CreateTriangle(triangles, 5, 6, 7);
		CreateTriangle(triangles, 5, 7, 8);
		CreateTriangle(triangles, 7, 11, 8);

		break;
	case 0b11001001: // 1+4+7+8
		//
		// 5-6-7
		// 5-7-8
		// 7-11-8
		// 0-3-4

		CreateTriangle(triangles, 5, 6, 7);
		CreateTriangle(triangles, 5, 7, 8);
		CreateTriangle(triangles, 7, 11, 8);
		CreateTriangle(triangles, 0, 3, 4);

		break;
	case 0b11001010: // 2+4+7+8
		//
		// 1-6-7
		// 0-1-7
		// 0-7-8
		// 7-11-8

		CreateTriangle(triangles, 1, 6, 7);
		CreateTriangle(triangles, 0, 1, 7);
		CreateTriangle(triangles, 0, 7, 8);
		CreateTriangle(triangles, 7, 11, 8);

		break;
	case 0b11001011: // 1+2+4+7+8, 3+5+6 C
		// Case 6c
		// 8-7-11
		// 8-6-7
		// 8-1-6
		// 8-3-1
		// 8-4-3

		CreateTriangle(triangles, 8, 7, 11);
		CreateTriangle(triangles, 8, 6, 7);
		CreateTriangle(triangles, 8, 1, 6);
		CreateTriangle(triangles, 8, 3, 1);
		CreateTriangle(triangles, 8, 4, 3);

		break;
	case 0b11001100: // 3+4+7+8
		//
		// 4-5-6
		// 4-6-7

		CreateTriangle(triangles, 4, 5, 6);
		CreateTriangle(triangles, 4, 6, 7);

		break;
	case 0b11001101: // 1+3+4+7+8, 2+5+6 C
		//
		// 5-6-7
		// 0-5-3
		// 3-5-7

		CreateTriangle(triangles, 5, 6, 7);
		CreateTriangle(triangles, 0, 5, 3);
		CreateTriangle(triangles, 3, 5, 7);

		break;
	case 0b11001110: // 2+3+4+7+8, 1+5+6 C
		//
		// 4-6-7
		// 0-1-6
		// 0-6-4

		CreateTriangle(triangles, 4, 6, 7);
		CreateTriangle(triangles, 0, 1, 6);
		CreateTriangle(triangles, 0, 6, 4);

		break;
	case 0b11001111: // 1+2+3+4+7+8, 5+6 C
		//
		// 1-7-3
		// 1-6-7

		CreateTriangle(triangles, 1, 7, 3);
		CreateTriangle(triangles, 1, 6, 7);

		break;
	case 0b11010000: // 5+7+8
		//
		// 3-11-9
		// 2-3-9
		// 2-9-6

		CreateTriangle(triangles, 3, 11, 9);
		CreateTriangle(triangles, 2, 3, 9);
		CreateTriangle(triangles, 2, 9, 6);

		break;
	case 0b11010001: // 1+5+7+8
		//
		// 2-9-6
		// 2-11-9
		// 2-4-11
		// 0-4-2

		CreateTriangle(triangles, 2, 9, 6);
		CreateTriangle(triangles, 2, 11, 9);
		CreateTriangle(triangles, 2, 4, 11);
		CreateTriangle(triangles, 0, 4, 2);

		break;
	case 0b11010010: // 2+5+7+8
		//
		// 0-5-1
		// 3-9-11
		// 2-9-3
		// 2-6-9

		CreateTriangle(triangles, 0, 5, 1);
		CreateTriangle(triangles, 3, 9, 11);
		CreateTriangle(triangles, 2, 9, 3);
		CreateTriangle(triangles, 2, 6, 9);

		break;
	case 0b11010011: // 1+2+5+7+8, 3+4+6 C
		// Case 6c
		// 2-9-6
		// 2-11-9
		// 2-4-11
		// 2-5-4
		// 2-1-5

		CreateTriangle(triangles, 2, 9, 6);
		CreateTriangle(triangles, 2, 11, 9);
		CreateTriangle(triangles, 2, 4, 11);
		CreateTriangle(triangles, 2, 5, 4);
		CreateTriangle(triangles, 2, 1, 5);

		break;
	case 0b11010100: // 3+5+7+8
		//
		// 2-3-6
		// 3-4-6
		// 4-9-6
		// 4-8-9

		CreateTriangle(triangles, 2, 3, 6);
		CreateTriangle(triangles, 3, 4, 6);
		CreateTriangle(triangles, 4, 9, 6);
		CreateTriangle(triangles, 4, 8, 9);

		break;
	case 0b11010101: // 1+3+5+7+8, 2+4+6 C
		//
		// 0-8-2
		// 2-8-9
		// 2-9-6

		CreateTriangle(triangles, 0, 8, 2);
		CreateTriangle(triangles, 2, 8, 9);
		CreateTriangle(triangles, 2, 9, 6);

		break;
	case 0b11010110: // 2+3+5+7+8, 1+4+6 C
		//
		// 0-1-5
		// 2-3-6
		// 3-4-6
		// 4-9-6
		// 4-8-9

		CreateTriangle(triangles, 0, 1, 5);
		CreateTriangle(triangles, 2, 3, 6);
		CreateTriangle(triangles, 3, 4, 6);
		CreateTriangle(triangles, 4, 9, 6);
		CreateTriangle(triangles, 4, 8, 9);

		break;
	case 0b11010111: // 1+2+3+5+7+8, 4+6 C
		//
		// 1-5-2
		// 2-5-8
		// 2-8-6
		// 6-8-9

		CreateTriangle(triangles, 1, 5, 2);
		CreateTriangle(triangles, 2, 5, 8);
		CreateTriangle(triangles, 2, 8, 6);
		CreateTriangle(triangles, 6, 8, 9);

		break;
	case 0b11011000: // 4+5+7+8
		//
	    // 5-11-8
		// 3-11-5
		// 2-3-5
		// 2-5-6

		CreateTriangle(triangles, 5, 11, 8);
		CreateTriangle(triangles, 3, 11, 5);
		CreateTriangle(triangles, 2, 3, 5);
		CreateTriangle(triangles, 2, 5, 6);

		break;
	case 0b11011001: // 1+4+5+7+8, 2+3+6 C
		// Case 6c
		// 11-0-4
		// 11-2-0
		// 11-6-2
		// 11-5-6
		// 11-8-5

		CreateTriangle(triangles, 11, 0, 4);
		CreateTriangle(triangles, 11, 2, 0);
		CreateTriangle(triangles, 11, 6, 2);
		CreateTriangle(triangles, 11, 5, 6);
		CreateTriangle(triangles, 11, 8, 5);
		
		break;
	case 0b11011010: // 2+4+5+7+8, 1+3+6 C
		// Case 6c
		// 6-2-3
		// 6-3-11
		// 6-11-8
		// 6-8-0
		// 6-0-1

		CreateTriangle(triangles, 6, 2, 3);
		CreateTriangle(triangles, 6, 3, 11);
		CreateTriangle(triangles, 6, 11, 8);
		CreateTriangle(triangles, 6, 8, 0);
		CreateTriangle(triangles, 6, 0, 1);

		break;
	case 0b11011011: // 1+2+4+5+7+8, 3+6 C
		//
		// 1-6-2
		// 4-11-8

		CreateTriangle(triangles, 1, 6, 2);
		CreateTriangle(triangles, 4, 11, 8);

		break;
	case 0b11011100: // 3+4+5+7+8, 1+2+6 C
		//
		// 4-5-6
		// 2-3-6
		// 3-4-6

		CreateTriangle(triangles, 4, 5, 6);
		CreateTriangle(triangles, 2, 3, 6);
		CreateTriangle(triangles, 3, 4, 6);

		break;
	case 0b11011101: // 1+3+4+5+7+8, 2+6 C
		//
		// 0-5-2
		// 2-5-6

		CreateTriangle(triangles, 0, 5, 2);
		CreateTriangle(triangles, 2, 5, 6);

		break;
	case 0b11011110: // 2+3+4+5+7+8, 1+6 C
		//
		// 0-1-6
		// 0-6-4
		// 2-3-6
		// 3-4-6

		CreateTriangle(triangles, 0, 1, 6);
		CreateTriangle(triangles, 0, 6, 4);
		CreateTriangle(triangles, 2, 3, 6);
		CreateTriangle(triangles, 3, 4, 6);

		break;
	case 0b11011111: // 1+2+3+4+5+7+8, 6 C
		//
		// 1-6-2

		CreateTriangle(triangles, 1, 6, 2);

		break;
	case 0b11100000: // 6+7+8
		//
		// 1-11-9
		// 1-7-11
		// 1-2-7

		CreateTriangle(triangles, 1, 11, 9);
		CreateTriangle(triangles, 1, 7, 11);
		CreateTriangle(triangles, 1, 2, 7);

		break;
	case 0b11100001: // 1+6+7+8
		//
		// 0-4-3
		// 1-11-9
		// 1-7-11
		// 1-2-7

		CreateTriangle(triangles, 0, 4, 3);
		CreateTriangle(triangles, 1, 11, 9);
		CreateTriangle(triangles, 1, 7, 11);
		CreateTriangle(triangles, 1, 2, 7);

		break;
	case 0b11100010: // 2+6+7+8
		//
		// 0-2-5
		// 2-9-5
		// 2-7-9
		// 7-11-9

		CreateTriangle(triangles, 0, 2, 5);
		CreateTriangle(triangles, 2, 9, 5);
		CreateTriangle(triangles, 2, 7, 9);
		CreateTriangle(triangles, 7, 11, 9);

		break;
	case 0b11100011: // 1+2+6+7+8, 3+4+5 C
		// Case 6c
		// 2-4-3
		// 2-5-4
		// 2-9-5
		// 2-11-9
		// 2-7-11

		CreateTriangle(triangles, 2, 4, 3);
		CreateTriangle(triangles, 2, 5, 4);
		CreateTriangle(triangles, 2, 9, 5);
		CreateTriangle(triangles, 2, 11, 9);
		CreateTriangle(triangles, 2, 7, 11);

		break;
	case 0b11100100: // 3+6+7+8
		//
		// 1-2-9
		// 2-7-9
		// 7-8-9
		// 4-8-7

		CreateTriangle(triangles, 1, 2, 9);
		CreateTriangle(triangles, 2, 7, 9);
		CreateTriangle(triangles, 7, 8, 9);
		CreateTriangle(triangles, 4, 8, 7);

		break;
	case 0b11100101: // 1+3+6+7+8, 2+4+5 C
		// Case 6c
		// 7-3-0
		// 7-0-8
		// 7-8-9
		// 7-9-1
		// 7-1-2

		CreateTriangle(triangles, 7, 3, 0);
		CreateTriangle(triangles, 7, 0, 8);
		CreateTriangle(triangles, 7, 8, 9);
		CreateTriangle(triangles, 7, 9, 1);
		CreateTriangle(triangles, 7, 1, 2);

		break;
	case 0b11100110: // 2+3+6+7+8, 1+4+5 C
		// Case 6c
		// 9-4-8
		// 9-7-4
		// 9-2-7
		// 9-0-2
		// 9-5-0

		CreateTriangle(triangles, 9, 4, 8);
		CreateTriangle(triangles, 9, 7, 4);
		CreateTriangle(triangles, 9, 2, 7);
		CreateTriangle(triangles, 9, 0, 2);
		CreateTriangle(triangles, 9, 5, 0);

		break;
	case 0b11100111: // 1+2+3+6+7+8, 4+5 C
		//
		// 2-7-3
		// 5-8-9

		CreateTriangle(triangles, 2, 7, 3);
		CreateTriangle(triangles, 5, 8, 9);

		break;
	case 0b11101000: // 4+6+7+8
		//
		// 7-11-8
		// 2-7-8
		// 2-8-5
		// 1-2-5

		CreateTriangle(triangles, 7, 11, 8);
		CreateTriangle(triangles, 2, 7, 8);
		CreateTriangle(triangles, 2, 8, 5);
		CreateTriangle(triangles, 1, 2, 5);

		break;
	case 0b11101001: // 1+4+6+7+8, 2+3+5 C
		//
		// 7-11-8
		// 2-7-8
		// 2-8-5
		// 1-2-5
		// 0-4-3

		CreateTriangle(triangles, 7, 11, 8);
		CreateTriangle(triangles, 2, 7, 8);
		CreateTriangle(triangles, 2, 8, 5);
		CreateTriangle(triangles, 1, 2, 5);
		CreateTriangle(triangles, 0, 4, 3);

		break;
	case 0b11101010: // 2+4+6+7+8, 1+3+5 C
		//
		// 0-2-8
		// 7-11-8
		// 2-7-8

		CreateTriangle(triangles, 0, 2, 8);
		CreateTriangle(triangles, 7, 11, 8);
		CreateTriangle(triangles, 2, 7, 8);

		break;
	case 0b11101011: // 1+2+4+6+7+8, 3+5 C
		//
		// 3-8-4
		// 3-2-8
		// 2-7-8
		// 7-11-8

		CreateTriangle(triangles, 3, 8, 4);
		CreateTriangle(triangles, 3, 2, 8);
		CreateTriangle(triangles, 2, 7, 8);
		CreateTriangle(triangles, 7, 11, 8);

		break;
	case 0b11101100: // 3+4+6+7+8, 1+2+5 C
		//
		// 4-5-7
		// 2-7-5
		// 1-2-5

		CreateTriangle(triangles, 4, 5, 7);
		CreateTriangle(triangles, 2, 7, 5);
		CreateTriangle(triangles, 1, 2, 5);

		break;
	case 0b11101101: // 1+3+4+6+7+8, 2+5 C
		//
		// 0-3-5
		// 3-5-7
		// 1-7-5
		// 1-2-7

		CreateTriangle(triangles, 0, 3, 5);
		CreateTriangle(triangles, 3, 5, 7);
		CreateTriangle(triangles, 1, 7, 5);
		CreateTriangle(triangles, 1, 2, 7);

		break;
	case 0b11101110: // 2+3+4+6+7+8, 1+5 C
		//
		// 0-2-7
		// 0-7-4

		CreateTriangle(triangles, 0, 2, 7);
		CreateTriangle(triangles, 0, 7, 4);

		break;
	case 0b11101111: // 1+2+3+4+6+7+8, 5 C
		//
		// 2-7-3

		CreateTriangle(triangles, 2, 7, 3);

		break;
	case 0b11110000: // 5+6+7+8
		//
		// 1-3-9
		// 3-11-9

		CreateTriangle(triangles, 1, 3, 9);
		CreateTriangle(triangles, 3, 11, 9);

		break;
	case 0b11110001: // 1+5+6+7+8, 2+3+4 C
		//
		// 1-11-9
		// 0-4-1
		// 1-4-11

		CreateTriangle(triangles, 1, 11, 9);
		CreateTriangle(triangles, 0, 4, 1);
		CreateTriangle(triangles, 1, 4, 11);

		break;
	case 0b11110010: // 2+5+6+7+8, 1+3+4 C
		//
		// 3-11-9
		// 0-3-9
		// 0-9-5

		CreateTriangle(triangles, 3, 11, 9);
		CreateTriangle(triangles, 0, 3, 9);
		CreateTriangle(triangles, 0, 9, 5);

		break;
	case 0b11110011: // 1+2+5+6+7+8, 3+4 C
		//
		// 4-9-5
		// 4-11-9

		CreateTriangle(triangles, 4, 9, 5);
		CreateTriangle(triangles, 4, 11, 9);

		break;
	case 0b11110100: // 3+5+6+7+8, 1+2+4 C
		//
		// 1-3-9
		// 3-4-9
		// 4-8-9

		CreateTriangle(triangles, 1, 3, 9);
		CreateTriangle(triangles, 3, 4, 9);
		CreateTriangle(triangles, 4, 8, 9);

		break;
	case 0b11110101: // 1+3+5+6+7+8, 2+4 C
		//
		// 0-8-1
		// 1-8-9

		CreateTriangle(triangles, 0, 8, 1);
		CreateTriangle(triangles, 1, 8, 9);

		break;
	case 0b11110110: // 2+3+5+6+7+8, 1+4 C
		//
		// 3-4-8
		// 3-8-9
		// 0-3-9
		// 0-9-5

		CreateTriangle(triangles, 3, 4, 8);
		CreateTriangle(triangles, 3, 8, 9);
		CreateTriangle(triangles, 0, 3, 9);
		CreateTriangle(triangles, 0, 9, 5);

		break;
	case 0b11110111: // 1+2+3+5+6+7+8, 4 C
		//
		// 5-8-9

		CreateTriangle(triangles, 5, 8, 9);

		break;
	case 0b11111000: // 4+5+6+7+8, 1+2+3 C
		//
		// 1-3-11
		// 1-11-8
		// 1-8-5

		CreateTriangle(triangles, 1, 3, 11);
		CreateTriangle(triangles, 1, 11, 8);
		CreateTriangle(triangles, 1, 8, 5);

		break;
	case 0b11111001: // 1+4+5+6+7+8, 2+3 C
		//
		// 0-4-1
		// 1-4-11
		// 1-11-8
		// 1-8-5

		CreateTriangle(triangles, 0, 4, 1);
		CreateTriangle(triangles, 1, 4, 11);
		CreateTriangle(triangles, 1, 11, 8);
		CreateTriangle(triangles, 1, 8, 5);

		break;
	case 0b11111010: // 2+4+5+6+7+8, 1+3 C
		//
		// 3-11-8
		// 3-8-0

		CreateTriangle(triangles, 3, 11, 8);
		CreateTriangle(triangles, 3, 8, 0);

		break;
	case 0b11111011: // 1+2+4+5+6+7+8, 3 C
		//
		// 4-11-8

		CreateTriangle(triangles, 4, 11, 8);

		break;
	case 0b11111100: // 3+4+5+6+7+8, 1+2 C
		//
		// 3-4-5
		// 1-3-5

		CreateTriangle(triangles, 3, 4, 5);
		CreateTriangle(triangles, 1, 3, 5);

		break;
	case 0b11111101: // 1+3+4+5+6+7+8, 2 C
		//
		// 0-5-1

		CreateTriangle(triangles, 0, 5, 1);

		break;
	case 0b11111110: // 2+3+4+5+6+7+8, 1 C
		//
		// 0-3-4
		
		CreateTriangle(triangles, 0, 3, 4);

		break;
	case 0b11111111: // 1+2+3+4+5+6+7+8
		// There are no vertices to create.
		break;
	default:
		// Unrecognized option spotted
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Create vertices using edges table. Set texture coordinates and set normals.
/// ----------------------------------------------------------------------
void CMarchingCube::CreateTriangle(vector< array<float, 8> > &triangle, int vrtx1, int vrtx2, int vrtx3)
{
	// 1. Calculate vertex position and save it in "triangles" variable.
	// 2. Calculate texture coordinates (or just type random ones...)
	// 3. Calculate normals using cross product of 2 vertices.

	//	0,	1,	2,	3,	4,	5,	6,	7
	//	x,	y,	z,	tu,	tv,	nx,	ny,	nz
	array<float, 8> vertex;
	float normal[3];

	/// 1 /// ---------------------------------------------
	// POSITION -------------------------------------------
	vertex[0] = edges[vrtx1][0]; // x
	vertex[1] = edges[vrtx1][1]; // y
	vertex[2] = edges[vrtx1][2]; // z
	
	// TEXTURE --------------------------------------------
	vertex[3] = 0; // tu
	vertex[4] = 0.5; // tv
	
	// NORMAL ---------------------------------------------
	CreateNormal(vrtx1, vrtx3, vrtx2, normal);

	vertex[5] = /*edges[vrtx1][0] + */normal[0]; // nx
	vertex[6] = /*edges[vrtx1][1] + */normal[1]; // ny
	vertex[7] = /*edges[vrtx1][2] + */normal[2]; // nz

	// Push the vertex onto triangle vector.
	triangle.push_back(vertex);

	/// 2 /// ---------------------------------------------
	// POSITION -------------------------------------------
	vertex[0] = edges[vrtx2][0]; // x
	vertex[1] = edges[vrtx2][1]; // y
	vertex[2] = edges[vrtx2][2]; // z

	// TEXTURE --------------------------------------------
	vertex[3] = 1; // tu
	vertex[4] = 0; // tv

	// NORMAL ---------------------------------------------
	CreateNormal(vrtx2, vrtx1, vrtx3, normal);

	vertex[5] = /*edges[vrtx2][0] + */normal[0]; // nx
	vertex[6] = /*edges[vrtx2][1] + */normal[1]; // ny
	vertex[7] = /*edges[vrtx2][2] + */normal[2]; // nz
											 
	// Push the vertex onto triangle vector.
	triangle.push_back(vertex);

	/// 3 /// ---------------------------------------------
	// POSITION -------------------------------------------
	vertex[0] = edges[vrtx3][0]; // x
	vertex[1] = edges[vrtx3][1]; // y
	vertex[2] = edges[vrtx3][2]; // z

	// TEXTURE --------------------------------------------
	vertex[3] = 1; // tu
	vertex[4] = 1; // tv

	// NORMAL ---------------------------------------------
	CreateNormal(vrtx3, vrtx2, vrtx1, normal);

	vertex[5] = /*edges[vrtx2][0] + */normal[0]; // nx
	vertex[6] = /*edges[vrtx2][1] + */normal[1]; // ny
	vertex[7] = /*edges[vrtx2][2] + */normal[2]; // nz

	// Push the vertex onto triangle vector.
	triangle.push_back(vertex);

	return;
}

/// ----------------------------------------------------------------------
/// vrtx1 is the vertex for which normal should be evaluated.
/// v1 = vrtx2 - vrtx1
/// v2 = vrtx3 - vrtx1
/// v1 x v2 -> normal!
/// ----------------------------------------------------------------------
void CMarchingCube::CreateNormal(int vrtx1, int vrtx2, int vrtx3, float (&normal)[3])
{
	float v1[3], v2[3];
	float length;

	// Evaluating normals using cross product
	v1[0] = edges[vrtx2][0] - edges[vrtx1][0];
	v1[1] = edges[vrtx2][1] - edges[vrtx1][1];
	v1[2] = edges[vrtx2][2] - edges[vrtx1][2];

	v2[0] = edges[vrtx3][0] - edges[vrtx1][0];
	v2[1] = edges[vrtx3][1] - edges[vrtx1][1];
	v2[2] = edges[vrtx3][2] - edges[vrtx1][2];

	// U = v1, V = v2
	// Nx = UyVz - UzVy
	// Ny = UzVx - UxVz
	// Nz = UxVy - UyVx
	normal[0] = v1[1] * v2[2] - v1[2] * v2[1];
	normal[1] = v1[2] * v2[0] - v1[0] * v2[2];
	normal[2] = v1[0] * v2[1] - v1[1] * v2[0];

	// normalize vectors
	// 1. evaluate length
	// 2. divide components by length
	length = sqrt(normal[0] * normal[0] +
		normal[1] * normal[1] + normal[2] * normal[2]);

	normal[0] /= length;
	normal[1] /= length;
	normal[2] /= length;

	/// temp ///
	//normal[0] = -1;
	//normal[1] = 0;
	//normal[2] = 0;

	return;
}