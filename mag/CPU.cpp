#include "CPU.h"

CCPU::CCPU()
{
	m_queryHandle = NULL;
	m_counterHandle = NULL;
}
CCPU::~CCPU(){}

/// ----------------------------------------------------------------------
/// Sets up the handle for querying the cpu on its usage. 
/// ----------------------------------------------------------------------
void CCPU::Initialize()
{
	PDH_STATUS status;


	// Initialize the flag indicating whether this object can
	// read the system cpu usage or not.
	m_canReadCPU = true;

	// Create a query object to poll CPU usage.
	status = PdhOpenQuery(NULL, 0, &m_queryHandle);
	if (status != ERROR_SUCCESS)
	{
		m_canReadCPU = false;
	}

	// Set query object to poll all CPUs in the system.
	status = PdhAddEnglishCounter(m_queryHandle, 
		TEXT("\\Processor(_Total)\\% processor time"), 0, &m_counterHandle);
	if (status != ERROR_SUCCESS)
	{
		m_canReadCPU = false;
	}

	m_lastSampleTime = GetTickCount();

	m_cpuUsage = 0;

	return;
}

/// ----------------------------------------------------------------------
/// Releases the handle used to query the cpu usage.
/// ----------------------------------------------------------------------
void CCPU::Shutdown()
{
	if (m_canReadCPU)
	{
		PdhCloseQuery(m_queryHandle);
	}

	return;
}

/// ----------------------------------------------------------------------
/// Query for CPU usage in given frame.
/// ----------------------------------------------------------------------
void CCPU::Frame()
{
	PDH_FMT_COUNTERVALUE value;


	if (m_canReadCPU)
	{
		if ((m_lastSampleTime + 1000) < GetTickCount())
		{
			m_lastSampleTime = GetTickCount();

			PdhCollectQueryData(m_queryHandle);

			PdhGetFormattedCounterValue(m_counterHandle, PDH_FMT_LONG, 
				NULL, &value);

			m_cpuUsage = value.longValue;
		}
	}

	return;
}

int CCPU::GetCPUPercentage()
{
	int usage;


	if (m_canReadCPU)
	{
		usage = static_cast<int>(m_cpuUsage);
	}
	else
	{
		usage = 0;
	}

	return usage;
}