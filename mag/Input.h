#pragma once

#define DIRECTINPUT_VERSION 0x0800

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

#include <dinput.h>

enum DIR { UP, DOWN, LEFT, RIGHT, FORWARD, BACKWARD, NONE }; // Camera controls.

/// ----------------------------------------------------------------------
/// The input object will store the state of each key in a keyboard array. 
/// When queried it will tell the calling functions if a certain key is pressed.
/// ----------------------------------------------------------------------
class CInput 
{
public:
	CInput();
	CInput(const CInput &);
	~CInput();

	bool Initialize(HINSTANCE, HWND, int, int);
	void Shutdown();
	bool Frame();

	bool IsEscapePressed();
	bool IsWPressed();
	bool IsAPressed();
	bool IsSPressed();
	bool IsDPressed();
	bool IsZPressed();
	bool IsSpacePressed();

	void GetMouseLocation(int &, int &);
	void GetMouseMovement(int &, int &);
	DIR GetDirection();

	void SetDirection(DIR);

private:
	bool ReadKeyboard();
	bool ReadMouse();
	void ProcessInput();

private:
	IDirectInput8 *m_directInput;
	IDirectInputDevice8 *m_keyboard, *m_mouse;
	
	unsigned char m_keyboardState[256];
	DIMOUSESTATE m_mouseState;

	int m_screenWidth, m_screenHeight;
	int m_mouseX, m_mouseY;

	DIR m_direction;
};