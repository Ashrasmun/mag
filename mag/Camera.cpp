#include "Camera.h"

CCamera::CCamera()
{
	m_positionX = 0.0f;
	m_positionY = 0.0f;
	m_positionZ = 0.0f;

	m_rotationX = 0.0f;
	m_rotationY = 0.0f;
	m_rotationZ = 0.0f;
}
CCamera::CCamera(const CCamera& other) {}
CCamera::~CCamera() {}

void CCamera::SetPosition(float x, float y, float z)
{
	m_positionX = x;
	m_positionY = y;
	m_positionZ = z;
	return;
}

void CCamera::SetRotation(float x, float y, float z)
{
	m_rotationX = x;
	m_rotationY = y;
	m_rotationZ = z;
	return;
}

XMFLOAT3 CCamera::GetPosition()
{
	return XMFLOAT3(m_positionX, m_positionY, m_positionZ);
}

XMFLOAT3 CCamera::GetRotation()
{
	return XMFLOAT3(m_rotationX, m_rotationY, m_rotationZ);
}

/// ----------------------------------------------------------------------
/// Use the position and rotation of the camera to build 
/// and update the view matrix.
/// ----------------------------------------------------------------------
void CCamera::Render()
{
	XMFLOAT3 up, position, lookAt;
	XMVECTOR upVector, positionVector, lookAtVector;
	float yaw, pitch, roll;
	XMMATRIX rotationMatrix;


	// Setup the vector that points upwards.
	up.x = 0.0f;
	up.y = 1.0f;
	up.z = 0.0f;

	// Load it into a XMVECTOR structure.
	upVector = XMLoadFloat3(&up);

	// Setup the position of the camera in the world.
	position.x = m_positionX;
	position.y = m_positionY;
	position.z = m_positionZ + 5.0f;

	// Load it into a XMVECTOR structure.
	positionVector = XMLoadFloat3(&position);

	// Setup where the camera is looking by default.
	lookAt.x = 0.0f;
	lookAt.y = 0.0f;
	lookAt.z = 1.0f;

	// Load it into a XMVECTOR structure.
	lookAtVector = XMLoadFloat3(&lookAt);

	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
	pitch = m_rotationX * 0.0174532925f;
	yaw = m_rotationY * 0.0174532925f;
	roll = m_rotationZ * 0.0174532925f;

	// Create the rotation matrix from the yaw, pitch, and roll values.
	rotationMatrix = XMMatrixRotationRollPitchYaw(pitch, yaw, roll);

	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	lookAtVector = XMVector3TransformCoord(lookAtVector, rotationMatrix);
	upVector = XMVector3TransformCoord(upVector, rotationMatrix);

	// Translate the rotated camera position to the location of the viewer.
	lookAtVector = XMVectorAdd(positionVector, lookAtVector);

	// Finally create the view matrix from the three updated vectors.
	m_viewMatrix = XMMatrixLookAtLH(positionVector, lookAtVector, upVector);

	return;
}

/// ----------------------------------------------------------------------
/// Update the camera using the inputs from keyboard and mouse.
/// ----------------------------------------------------------------------
void CCamera::Update(int mousemovementX, int mousemovementY, int dir)
{

	// TODO: Make it independent from FPS

	float px, py, pz, rx, ry, rz;
	float ms = MOVEMENT_SPEED * 0.01f;
	float rs = ROTATION_SPEED * 0.1f;

	// Check camera position and rotation.
	px = GetPosition().x;
	py = GetPosition().y;
	pz = GetPosition().z;

	rx = GetRotation().x;
	ry = GetRotation().y;
	rz = GetRotation().z;

	rx += mousemovementY * rs;
	ry += mousemovementX * rs;

	// Cap camera vertical angle at 60 degrees.
	if (rx < -60) { rx = -60; }
	if (rx > 60) { rx = 60; }
	if (ry < -360) { ry = 0; }
	if (ry > 360) { ry = 0; }

	SetRotation(rx, ry, rz);

	// Change rotation to radians.
	ry *= 0.0174532925f;

	/// DIR { UP, DOWN, LEFT, RIGHT, FORWARD, BACKWARD, NONE }
	switch (dir)
	{
	case 0:
		SetPosition(px, py + ms, pz);
		break;

	case 1:
		SetPosition(px, py - ms, pz);
		break;

	case 2:
		SetPosition(px - cosf(ry) * ms, py, pz + sinf(ry) * ms);
		break;

	case 3:
		SetPosition(px + cosf(ry) * ms, py, pz - sinf(ry) * ms);
		break;

	case 4:
		SetPosition(px + sinf(ry) * ms, py, pz + cosf(ry) * ms);
		break;

	case 5:
		SetPosition(px - sinf(ry) * ms, py, pz - cosf(ry) * ms);
		break;

	default:
		break;
	}

	return;
}

void CCamera::GetViewMatrix(XMMATRIX &viewMatrix)
{
	viewMatrix = m_viewMatrix;
	return;
}