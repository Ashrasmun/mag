#pragma once

#include <d3d11.h>
#include <directxmath.h>
#include <fstream>
#include <string>
#include <cmath>
#include "Config.h"
#include "Texture.h"
#include "FileParser.h"
#include "FunctionParser.h"
#include "MarchingCube.h"
#include "Timer.h"

using namespace std;
using namespace DirectX;
using namespace Config::Grid;
using namespace Config::Model;

/// ----------------------------------------------------------------------
/// Encapsulating the geometry for 3D models
/// ----------------------------------------------------------------------
class CModel
{
private:
	enum EXPR { ADD, SUB, MUL, DIV, POW, NONE };

	struct SVertex
	{
		XMFLOAT3 position;
		XMFLOAT2 texture;
		XMFLOAT3 normal;
	};

	struct SModel
	{
		float x, y, z;		// position
		float tu, tv;		// texture
		float nx, ny, nz;	// normal
	};

public:
	CModel();
	CModel(const CModel &);
	~CModel();

	bool Initialize(ID3D11Device *, ID3D11DeviceContext *, int &, float&);
	void Shutdown();
	void Render(ID3D11DeviceContext *); 

	int GetIndexCount();

	ID3D11ShaderResourceView *GetTexture();

private:
	bool InitializeBuffers(ID3D11Device *);
	void ShutdownBuffers();
	void RenderBuffers(ID3D11DeviceContext *);

	bool LoadTexture(ID3D11Device *, ID3D11DeviceContext *, string);
	void ReleaseTexture();

	bool LoadModel(string, int);
	//void ReleaseModel();

	void PrepareModelData();

	bool InitializeFunctionParser();
	void ReleaseFunctionParser();

	void CreatePolygons();

	bool InitializeFileParser();
	void ReleaseFileParser();

	bool InitializeMarchingCube();
	void ReleaseMarchingCube();


private:
	int m_vertexCount, m_indexCount, m_partialVertexCount;
	ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;

	vector<bool> m_row;
	vector<float> m_isorow;
	vector< vector<bool> > m_slice;
	vector< vector<float> > m_isoslice;
	vector< vector< vector<bool> > > m_data;
	vector< vector< vector<float> > > m_isodata;

	SModel m_point;
	vector<SModel> m_classicModel;
	vector<SModel> m_isoModel;
	CTexture *m_Texture;
	CFileParser *m_FileParser;
	CFunctionParser *m_FunctionParser;
	CMarchingCube *m_MarchingCube;
	CTimer		*m_Timer;
};