#include "FontShader.h"

CFontShader::CFontShader()
{
	m_vertexShader = nullptr;
	m_pixelShader = nullptr;
	m_layout = nullptr;
	m_constantBuffer = nullptr;
	m_sampleState = nullptr;
	m_pixelBuffer = nullptr;
}
CFontShader::CFontShader(const CFontShader &other) {}
CFontShader::~CFontShader() {}

/// ----------------------------------------------------------------------
/// Initializes shader files.
/// ----------------------------------------------------------------------
bool CFontShader::Initialize(ID3D11Device *device, HWND hwnd)
{
	bool result;


	// Initialize the vertex and pixel shaders.
	result = InitializeShader(device, hwnd, L"../mag/font.vs", L"../mag/font.ps");
	if (!result)
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Shutdowns the shader.
/// ----------------------------------------------------------------------
void CFontShader::Shutdown()
{
	// Shutdown the vertex and pixel shaders as well as the related objects.
	ShutdownShader();

	return;
}

/// ----------------------------------------------------------------------
/// Sets shader parameters and rendering.
/// ----------------------------------------------------------------------
bool CFontShader::Render(ID3D11DeviceContext *deviceContext, int indexCount,
	XMMATRIX worldMatrix, XMMATRIX viewMatrix, XMMATRIX projectionMatrix,
	ID3D11ShaderResourceView *texture,
	XMFLOAT4 pixelColor)
{
	bool result;


	// Set the shader parameters that it will use for rendering.
	result = SetShaderParameters(deviceContext, 
		worldMatrix, viewMatrix, projectionMatrix, 
		texture,
		pixelColor);
	if (!result)
	{
		return false;
	}

	// Now render the prepared buffers with the shader.
	RenderShader(deviceContext, indexCount);

	return true;
}

/// ----------------------------------------------------------------------
/// Initializes shader.
/// ----------------------------------------------------------------------
bool CFontShader::InitializeShader(ID3D11Device *device, HWND hwnd, 
	WCHAR *vsFilename, WCHAR *psFilename)
{
	HRESULT hr;
	ID3D10Blob* errorMessage;
	ID3D10Blob* vertexShaderBuffer;
	ID3D10Blob* pixelShaderBuffer;
	D3D11_INPUT_ELEMENT_DESC polygonLayout[2];
	unsigned int numElements;
	D3D11_BUFFER_DESC constantBufferDesc;
	D3D11_BUFFER_DESC pixelBufferDesc;
	D3D11_SAMPLER_DESC samplerDesc;


	// Initialize the pointers this function will use to null.
	errorMessage = nullptr;
	vertexShaderBuffer = nullptr;
	pixelShaderBuffer = nullptr;

	// Compile the vertex shader code.
	hr = D3DCompileFromFile(vsFilename, nullptr, nullptr, "FontVertexShader",
		"vs_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, &vertexShaderBuffer, &errorMessage);
	if (FAILED(hr))
	{
		// If the shader failed to compile it should have writen something to the error message.
		if (errorMessage)
		{
			OutputShaderErrorMessage(errorMessage, hwnd, vsFilename);
		}
		// If there was  nothing in the error message then it simply could not find the shader file itself.
		else
		{
			MessageBox(hwnd, vsFilename, L"Missing Vertex Shader File", MB_OK);
		}

		return false;
	}

	// Compile the pixel shader code.
	hr = D3DCompileFromFile(psFilename, nullptr, nullptr, "FontPixelShader", 
		"ps_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, &pixelShaderBuffer, &errorMessage);
	if (FAILED(hr))
	{
		// If the shader failed to compile it should have writen something to the error message.
		if (errorMessage)
		{
			OutputShaderErrorMessage(errorMessage, hwnd, psFilename);
		}
		// If there was nothing in the error message then it simply could not find the file itself.
		else
		{
			MessageBox(hwnd, psFilename, L"Missing Pixel Shader File", MB_OK);
		}

		return false;
	}

	// Create the vertex shader from the buffer.
	hr = device->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(),
		vertexShaderBuffer->GetBufferSize(), nullptr, &m_vertexShader);
	if (FAILED(hr))
	{
		return false;
	}

	// Create the pixel shader from the buffer.
	hr = device->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(),
		pixelShaderBuffer->GetBufferSize(), nullptr, &m_pixelShader);
	if (FAILED(hr))
	{
		return false;
	}

	// Create the vertex input layout description.
	// This setup needs to match the SVertex stucture in the CFont 
	// and in the shader.
	polygonLayout[0].SemanticName			= "POSITION";
	polygonLayout[0].SemanticIndex			= 0;
	polygonLayout[0].Format					= DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot				= 0;
	polygonLayout[0].AlignedByteOffset		= 0;
	polygonLayout[0].InputSlotClass			= D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate	= 0;

	polygonLayout[1].SemanticName			= "TEXCOORD";
	polygonLayout[1].SemanticIndex			= 0;
	polygonLayout[1].Format					= DXGI_FORMAT_R32G32_FLOAT;
	polygonLayout[1].InputSlot				= 0;
	polygonLayout[1].AlignedByteOffset		= D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass			= D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate	= 0;

	// Get a count of the elements in the layout.
	numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	// Create the vertex input layout.
	hr = device->CreateInputLayout(polygonLayout, numElements, 
		vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(),
		&m_layout);
	if (FAILED(hr))
	{
		return false;
	}

	// Release the vertex shader buffer and pixel shader buffer 
	// since they are no longer needed.
	vertexShaderBuffer->Release();
	vertexShaderBuffer = nullptr;

	pixelShaderBuffer->Release();
	pixelShaderBuffer = nullptr;

	// Setup the description of the dynamic constant buffer 
	// that is in the vertex shader.
	constantBufferDesc.Usage				= D3D11_USAGE_DYNAMIC;
	constantBufferDesc.ByteWidth			= sizeof(SConstantBuffer);
	constantBufferDesc.BindFlags			= D3D11_BIND_CONSTANT_BUFFER;
	constantBufferDesc.CPUAccessFlags		= D3D11_CPU_ACCESS_WRITE;
	constantBufferDesc.MiscFlags			= 0;
	constantBufferDesc.StructureByteStride	= 0;

	// Create the constant buffer pointer so we can access 
	// the vertex shader constant buffer from within this class.
	hr = device->CreateBuffer(&constantBufferDesc, nullptr, &m_constantBuffer);
	if (FAILED(hr))
	{
		return false;
	}

	// Create a texture sampler state description.
	samplerDesc.Filter			= D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU		= D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV		= D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW		= D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias		= 0.0f;
	samplerDesc.MaxAnisotropy	= 1;
	samplerDesc.ComparisonFunc  = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0]  = 0;
	samplerDesc.BorderColor[1]  = 0;
	samplerDesc.BorderColor[2]  = 0;
	samplerDesc.BorderColor[3]  = 0;
	samplerDesc.MinLOD			= 0;
	samplerDesc.MaxLOD			= D3D11_FLOAT32_MAX;

	// Create the texture sampler state.
	hr = device->CreateSamplerState(&samplerDesc, &m_sampleState);
	if (FAILED(hr))
	{
		return false;
	}

	// Setup the pixel color constant buffer that will allow 
	// this class to set the pixel color in the pixel shader.
	pixelBufferDesc.Usage				= D3D11_USAGE_DYNAMIC;
	pixelBufferDesc.ByteWidth			= sizeof(SPixelBuffer);
	pixelBufferDesc.BindFlags			= D3D11_BIND_CONSTANT_BUFFER;
	pixelBufferDesc.CPUAccessFlags		= D3D11_CPU_ACCESS_WRITE;
	pixelBufferDesc.MiscFlags			= 0;
	pixelBufferDesc.StructureByteStride = 0;

	// Create the pixel constant buffer pointer so we can access 
	// the pixel shader constant buffer from withitn this class.
	hr = device->CreateBuffer(&pixelBufferDesc, nullptr, &m_pixelBuffer);
	if(FAILED(hr))
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Releases the four interfaces that were setup in the InitializeShader 
/// function.
/// ----------------------------------------------------------------------
void CFontShader::ShutdownShader()
{
	// Release the pixel constant buffer.
	if (m_pixelBuffer)
	{
		m_pixelBuffer->Release();
		m_pixelBuffer = nullptr;
	}

	// Release the sampler state.
	if (m_sampleState)
	{
		m_sampleState->Release();
		m_sampleState = nullptr;
	}

	// Release the matrix constant buffer.
	if (m_constantBuffer)
	{
		m_constantBuffer->Release();
		m_constantBuffer = nullptr;
	}

	// Release the layout.
	if (m_layout)
	{
		m_layout->Release();
		m_layout = nullptr;
	}

	// Release the pixel shader.
	if (m_pixelShader)
	{
		m_pixelShader->Release();
		m_pixelShader = nullptr;
	}

	// Release the vertex shader.
	if (m_vertexShader)
	{
		m_vertexShader->Release();
		m_vertexShader = nullptr;
	}

	return;
}

/// ----------------------------------------------------------------------
/// Write out error messages that are generating when compiling either 
/// vertex shaders or pixel shaders.
/// ----------------------------------------------------------------------
void CFontShader::OutputShaderErrorMessage(ID3D10Blob *errorMessage,
	HWND hwnd, WCHAR *shaderFilename)
{
	char *compileErrors;
	unsigned long long bufferSize, i;
	ofstream fout;


	// Get a pointer to the error message text buffer.
	compileErrors = (char*)(errorMessage->GetBufferPointer());

	// Get the length of the message.
	bufferSize = errorMessage->GetBufferSize();

	// Open a file to write the error message to.
	fout.open("shader-error.txt");

	// Write out the error message.
	for (i = 0; i < bufferSize; i++)
	{
		fout << compileErrors[i];
	}

	// Close the file.
	fout.close();

	// Release the error message.
	errorMessage->Release();
	errorMessage = nullptr;

	// Pop a message up on the screen to notify the user 
	// to check the text file for compile errors.
	MessageBox(hwnd, L"Error compiling shader. "
		"Check shader-error.txt for message.", shaderFilename, MB_OK);

	return;
}

/// ----------------------------------------------------------------------
/// This exists to make setting the global variables in the shader easier.
/// ----------------------------------------------------------------------
bool CFontShader::SetShaderParameters(ID3D11DeviceContext *deviceContext,
	XMMATRIX worldMatrix, XMMATRIX viewMatrix, XMMATRIX projectionMatrix,
	ID3D11ShaderResourceView *texture,
	XMFLOAT4 pixelColor)
{
	HRESULT hr;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	SConstantBuffer *dataPtr;
	SPixelBuffer *dataPtr2;
	unsigned int bufferNumber;

	// Transpose the matrices to prepare them for the shader.
	worldMatrix = XMMatrixTranspose(worldMatrix);
	viewMatrix = XMMatrixTranspose(viewMatrix);
	projectionMatrix = XMMatrixTranspose(projectionMatrix);

	// Lock the constant buffer so it can be written to.
	hr = deviceContext->Map(m_constantBuffer, 0, D3D11_MAP_WRITE_DISCARD,
		0, &mappedResource);
	if (FAILED(hr))
	{
		return false;
	}

	// Get a pointer to the data in the constant buffer.
	dataPtr = (SConstantBuffer*)mappedResource.pData;

	// Copy the matrices into the constant buffer.
	dataPtr->world = worldMatrix;
	dataPtr->view = viewMatrix;
	dataPtr->projection = projectionMatrix;

	// Unlock the constant buffer.
	deviceContext->Unmap(m_constantBuffer, 0);

	// Set the position of the constant buffer in the vertex shader.
	bufferNumber = 0;

	// Finally set the constant buffer in the vertex shader with 
	// the updated values.
	deviceContext->VSSetConstantBuffers(bufferNumber, 1, &m_constantBuffer);

	// Set shader texture resource in the pixel shader.
	deviceContext->PSSetShaderResources(0, 1, &texture);

	// Lock the pixel constant buffer so it can be written to.
	hr = deviceContext->Map(m_pixelBuffer, 0, D3D11_MAP_WRITE_DISCARD,
		0, &mappedResource);
	if (FAILED(hr))
	{
		return false;
	}

	// Get a pointer to the data in the pixel constant buffer;
	dataPtr2 = (SPixelBuffer*)mappedResource.pData;

	// Copy the pixel color into the pixel constant buffer.
	dataPtr2->pixelColor = pixelColor;

	// Unlock the pixel constant buffer.
	deviceContext->Unmap(m_pixelBuffer, 0);

	// Set the position of the pixel constant buffer in the pixel shader.
	bufferNumber = 0;

	// Set the pixel constant buffer in the pixel shader with 
	// the updated value.
	deviceContext->PSSetConstantBuffers(bufferNumber, 1, &m_pixelBuffer);

	return true;
}

/// ----------------------------------------------------------------------
/// Rendering what's written in shader.
/// ----------------------------------------------------------------------
void CFontShader::RenderShader(ID3D11DeviceContext* deviceContext,
	int indexCount)
{
	// Set the vertex input layout.
	deviceContext->IASetInputLayout(m_layout);

	// Set the vertex and pixel shaders that will be used 
	// to render this triangle.
	deviceContext->VSSetShader(m_vertexShader, nullptr, 0);
	deviceContext->PSSetShader(m_pixelShader, nullptr, 0);

	// Set the sampler state in the pixel shader.
	deviceContext->PSSetSamplers(0, 1, &m_sampleState);

	// Render the model.
	deviceContext->DrawIndexed(indexCount, 0, 0);

	return;
}