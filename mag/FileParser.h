#pragma once

#include <directxmath.h>
#include <fstream>
#include <vector>
#include <array>
#include <string>

using namespace std;

/// ----------------------------------------------------------------------
/// Parsing file in case of stumbling upon a file with wrong extension
/// than the one used in this program
/// ----------------------------------------------------------------------
class CFileParser
{
public:
	CFileParser();
	CFileParser(const CFileParser &);
	~CFileParser();

	bool CheckFilenameExtension(string &);
	bool CreateParsedFile(string &);
	bool ParseOBJFileData(string);

	void StoreVertexData(ifstream &, char);
	void SetFinalVertexData(ifstream &, char, array<float, 8> &);

private:
	char *m_newFilename;
	int m_finalVertexCount;
	vector<array<float, 3>> m_geometricVertices;	// x, y, z
	vector<array<float, 2>> m_textureVertices;		//			tu, tv
	vector<array<float, 3>> m_normalVertices;		//					nx, ny, nz
	vector<array<float, 8>> m_finalVertices;		// x, y, z, tu, tv, nx, ny, nz
	array<array<float, 8>, 3> m_triangle;	
};