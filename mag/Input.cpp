#include "Input.h"


CInput::CInput() 
{
	m_directInput = nullptr;
	m_keyboard = nullptr;
	m_mouse = nullptr;
}
CInput::CInput(const CInput& other) {}
CInput::~CInput() {}

/// ----------------------------------------------------------------------
/// Initialize mouse and keyboard
/// ----------------------------------------------------------------------
bool CInput::Initialize(HINSTANCE hinstance, HWND hwnd,
	int screenWidth, int screenHeight) 
{
	HRESULT hr;


	// Store the screen size which will be used for 
	// positioning the mouse cursor
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;

	// Initialize the location of the mouse on the screen
	m_mouseX = 0;
	m_mouseY = 0;

	// Initialize the main direct input interface.
	hr = DirectInput8Create(hinstance, DIRECTINPUT_VERSION, IID_IDirectInput8,
		(void**)&m_directInput, NULL);
	if (FAILED(hr))
	{
		return false;
	}

	/// //////// ///
	/// KEYBOARD ///
	/// //////// ///

	// Initialize the direct input interface for the keyboard.
	hr = m_directInput->CreateDevice(GUID_SysKeyboard, &m_keyboard, NULL);
	if (FAILED(hr))
	{
		return false;
	}

	// Set the data format. In the case, since it is a keyboard we can use 
	// the predefined data format.
	hr = m_keyboard->SetDataFormat(&c_dfDIKeyboard);
	if (FAILED(hr))
	{
		return false;
	}

	// Set the cooperative level of the keyboard to not share with other programs.
	hr = m_keyboard->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_EXCLUSIVE);
	if (FAILED(hr))
	{
		return false;
	}

	// Acquire the keyboard.
	hr = m_keyboard->Acquire();
	if (FAILED(hr))
	{
		return false;
	}

	/// ///// ///
	/// MOUSE ///
	/// ///// ///

	// Initialize the direct input interface for the mouse.
	hr = m_directInput->CreateDevice(GUID_SysMouse, &m_mouse, NULL);
	if (FAILED(hr))
	{
		return false;
	}

	// Set the data format for the mouse using the pre-defined mouse data format.
	hr = m_mouse->SetDataFormat(&c_dfDIMouse);
	if (FAILED(hr))
	{
		return false;
	}

	// Set the cooperative level of the mouse to share with other programs.
	hr = m_mouse->SetCooperativeLevel(hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	if (FAILED(hr))
	{
		return false;
	}

	// Acquire the mouse.
	hr = m_mouse->Acquire();
	if (FAILED(hr))
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Releases the two devices and the interface to Direct Input.
/// ----------------------------------------------------------------------
void CInput::Shutdown()
{
	// Release the mouse.
	if (m_mouse)
	{
		m_mouse->Unacquire();
		m_mouse->Release();
		m_mouse = nullptr;
	}

	// Release the keyboard.
	if (m_keyboard)
	{
		m_keyboard->Unacquire();
		m_keyboard->Release();
		m_keyboard = nullptr;
	}

	// Release the main interface to direct input.
	if (m_directInput)
	{
		m_directInput->Release();
		m_directInput = nullptr;
	}

	return;
}

/// ----------------------------------------------------------------------
/// Reads the current state of the devices into state buffers. 
/// ----------------------------------------------------------------------
bool CInput::Frame()
{
	bool result;


	// Read the current state of the keyboard.
	result = ReadKeyboard();
	if (!result)
	{
		return false;
	}

	// Read the current state of the mouse.
	result = ReadMouse();
	if (!result)
	{
		return false;
	}

	// Process the changes in the mouse and keyboard.
	ProcessInput();

	return true;
}

/// ----------------------------------------------------------------------
/// Reads the state of the keyboard and shows any keys that are currently 
/// pressed or not.
/// ----------------------------------------------------------------------
bool CInput::ReadKeyboard()
{
	HRESULT hr;


	// Read the keyboard device.
	hr = m_keyboard->GetDeviceState(sizeof(m_keyboardState), 
		(LPVOID)&m_keyboardState);
	if (FAILED(hr))
	{
		// If the keyboard lost focus or was not acquired then try to 
		// get control back.
		if ((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED))
		{
			m_keyboard->Acquire();
		}
		else
		{
			return false;
		}
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Reads the state of the mouse and shows any keys that are currently 
/// pressed or not.
/// ----------------------------------------------------------------------
bool CInput::ReadMouse()
{
	HRESULT hr;


	// Read the mouse device.
	hr = m_mouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&m_mouseState);
	if (FAILED(hr))
	{
		// If the mouse lost focus or was not acquired 
		// then try to get control back.
		if ((hr == DIERR_INPUTLOST) || (hr == DIERR_NOTACQUIRED))
		{
			m_mouse->Acquire();
		}
		else
		{
			return false;
		}
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Deals with the changes that have happened in the input devices 
/// since the last frame.
/// ----------------------------------------------------------------------
void CInput::ProcessInput()
{
	// Update the location of the mouse cursor based on the change of
	// the mouse location during the frame.
	m_mouseX += m_mouseState.lX;
	m_mouseY += m_mouseState.lY;

	// Ensure the mouse location doesn't exceed the screen width or height.
	if (m_mouseX < 0) { m_mouseX = 0; }
	if (m_mouseY < 0) { m_mouseY = 0; }

	if (m_mouseX > m_screenWidth) { m_mouseX = m_screenWidth; }
	if (m_mouseY > m_screenHeight) { m_mouseY = m_screenHeight; }

	return;
}

bool CInput::IsEscapePressed()
{
	// Do a bitwise and on keyboard state to check if the escape key is 
	// currently being pressed.
	if (m_keyboardState[DIK_ESCAPE] & 0x80)
	{
		return true;
	}

	return false;
}

bool CInput::IsWPressed()
{
	if (m_keyboardState[DIK_W] & 0x80)
	{
		return true;
	}

	return false;
}

bool CInput::IsAPressed()
{
	if (m_keyboardState[DIK_A] & 0x80)
	{
		return true;
	}

	return false;
}

bool CInput::IsSPressed()
{
	if (m_keyboardState[DIK_S] & 0x80)
	{
		return true;
	}

	return false;
}

bool CInput::IsDPressed()
{
	if (m_keyboardState[DIK_D] & 0x80)
	{
		return true;
	}

	return false;
}

bool CInput::IsZPressed()
{
	if (m_keyboardState[DIK_Z] & 0x80)
	{
		return true;
	}

	return false;
}

bool CInput::IsSpacePressed()
{
	if (m_keyboardState[DIK_SPACE] & 0x80)
	{
		return true;
	}

	return false;
}

void CInput::GetMouseLocation(int &mouseX, int &mouseY)
{
	mouseX = m_mouseX;
	mouseY = m_mouseY;

	return;
}

void CInput::GetMouseMovement(int &moveX, int &moveY)
{
	moveX = m_mouseState.lX;
	moveY = m_mouseState.lY;

	return;
}

void CInput::SetDirection(DIR dir)
{
	m_direction = dir;
	return;
}

DIR CInput::GetDirection()
{
	return m_direction;
}