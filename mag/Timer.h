#pragma once

#include <windows.h>

/// ----------------------------------------------------------------------
/// High precision timer that measures the exact time between 
/// frames of execution. It is used to properly measure FPS and CPU usage.
/// ----------------------------------------------------------------------
class CTimer
{
public:
	CTimer();
	~CTimer();

	bool Initialize();
	void Frame();

	float GetTime();
	bool IsRead();

private:
	bool isRead; // check whether the timer has been read
	INT64 m_frequency, m_startTime;
	float m_ticksPerMs, m_frameTime, m_elapsedTime;
};

