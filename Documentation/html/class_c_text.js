var class_c_text =
[
    [ "CText", "class_c_text.html#af837cea14f98ba12e37139cb702eb57e", null ],
    [ "CText", "class_c_text.html#aa1b1665b2f7cc6cfc6686f9dc1f87e1c", null ],
    [ "~CText", "class_c_text.html#acf85c241b5e80ef93883d2b25e330b49", null ],
    [ "Initialize", "class_c_text.html#a8ba03c3e62ed42a14342af2ef1d19226", null ],
    [ "Render", "class_c_text.html#ab0f66c2319f75c8dca1c3a7105242357", null ],
    [ "SetCPU", "class_c_text.html#a9078ae45bd8394be3aa972ed207cb641", null ],
    [ "SetFPS", "class_c_text.html#aeddbf723502ed2d107c5307c1647f5ea", null ],
    [ "SetMousePosition", "class_c_text.html#a8589938367ede05ba288a46dc0c4564f", null ],
    [ "SetNumberOfTriangles", "class_c_text.html#a90a163a202a37f5ea7285a3be78b0690", null ],
    [ "SetPosRot", "class_c_text.html#a0f8bc4e61e48e600ba2cb043ab369f71", null ],
    [ "SetTimer", "class_c_text.html#a63e33c8d1991a5aac32acf9bdd6957d3", null ],
    [ "SetVideoCard", "class_c_text.html#a2ac1334d34a34b5dbd2782fbaee68e6d", null ],
    [ "Shutdown", "class_c_text.html#af016943aeec8d3f99deb3033110a53e1", null ]
];