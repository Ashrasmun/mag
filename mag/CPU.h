#pragma once

#pragma comment(lib, "pdh.lib")

#include <pdh.h>

/// ----------------------------------------------------------------------
/// CPU usage measuring.
/// ----------------------------------------------------------------------
class CCPU
{
public:
	CCPU();
	~CCPU();

	void Initialize();
	void Shutdown();
	void Frame();
	
	int GetCPUPercentage();

private:
	bool m_canReadCPU;
	HQUERY m_queryHandle;
	HCOUNTER m_counterHandle;
	unsigned long m_lastSampleTime;
	long m_cpuUsage;
};