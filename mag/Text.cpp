#include "Text.h"


CText::CText() 
{
	m_Font = nullptr;
	m_FontShader = nullptr;

	m_videoCard = nullptr;
	m_videoMemory = nullptr;

	m_fps = nullptr;
	m_cpu = nullptr;
	m_timer = nullptr;
	m_numOfTriangles = nullptr;

	m_mouseX = nullptr;
	m_mouseY = nullptr;

	m_posX = nullptr;
	m_posY = nullptr;
	m_posZ = nullptr;

	m_rotX = nullptr;
	m_rotY = nullptr;
	m_rotZ = nullptr;
}
CText::CText(const CText &) {}
CText::~CText() {}

/// ----------------------------------------------------------------------
/// Create sentences.
/// ----------------------------------------------------------------------
bool CText::Initialize(ID3D11Device *device, ID3D11DeviceContext *deviceContext,
	HWND hwnd, int screenWidth, int screenHeight, XMMATRIX baseViewMatrix)
{
	bool result;


	// Store the screen width and height.
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;

	// Store the base view matrix.
	m_baseViewMatrix = baseViewMatrix;

	// Create the font object.
	m_Font = new CFont;
	if (!m_Font)
	{
		return false;
	}

	// Initialize the font object.
	result = m_Font->Initialize(device, deviceContext, 
		"../mag/data/fontdata.txt", "../mag/data/font.tga");
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize the font object.", 
			L"Error", MB_OK);
		return false;
	}

	// Create the font shader object.
	m_FontShader = new CFontShader;
	if (!m_FontShader)
	{
		return false;
	}

	// Initialize the font shader object.
	result = m_FontShader->Initialize(device, hwnd);
	if (!result)
	{
		MessageBox(hwnd, L"Could not initialize the font shader object.",
			L"Error", MB_OK);
		return false;
	}

	// Initialize the video card name sentence.
	result = InitializeSentence(&m_videoCard, 144, device);
	if (!result) { return false; }

	// Initialize the video card memory capacity sentence.
	result = InitializeSentence(&m_videoMemory, 40, device);
	if (!result) { return false; }

	// Initialize the FPS counter sentence.
	result = InitializeSentence(&m_fps, 16, device);
	if (!result) { return false; }

	// Initialize the CPU usage sentence
	result = InitializeSentence(&m_cpu, 16, device);
	if (!result) { return false; }

	// Initialize the timer sentence
	result = InitializeSentence(&m_timer, 16, device);
	if (!result) { return false; }

	// Initialize the number of triangles sentence
	result = InitializeSentence(&m_numOfTriangles, 32, device);
	if (!result) { return false; }

	// Initialize the mouse X position sentence
	result = InitializeSentence(&m_mouseX, 16, device);
	if (!result) { return false; }

	// Initialize the mouse Y position sentence
	result = InitializeSentence(&m_mouseY, 16, device);
	if (!result) { return false; }

	/// CAMERA ///
	// Initialize the camera X position sentence.
	result = InitializeSentence(&m_posX, 16, device);
	if (!result) { return false; }

	// Initialize the camera Y position sentence.
	result = InitializeSentence(&m_posY, 16, device);
	if (!result) { return false; }

	// Initialize the camera Z position sentence.
	result = InitializeSentence(&m_posZ, 16, device);
	if (!result) { return false; }


	// Initialize the camera X rotation sentence.
	result = InitializeSentence(&m_rotX, 16, device);
	if (!result) { return false; }

	// Initialize the camera Y rotation sentence.
	result = InitializeSentence(&m_rotY, 16, device);
	if (!result) { return false; }

	// Initialize the camera Z rotation sentence.
	result = InitializeSentence(&m_rotZ, 16, device);
	if (!result) { return false; }


	return true;
}

/// ----------------------------------------------------------------------
/// Release the two sentences, the font object, and the font shader object.
/// ----------------------------------------------------------------------
void CText::Shutdown()
{
	ReleaseSentence(&m_rotZ);
	ReleaseSentence(&m_rotY);
	ReleaseSentence(&m_rotX);

	ReleaseSentence(&m_posZ);
	ReleaseSentence(&m_posY);
	ReleaseSentence(&m_posX);
	// Release the mouse Y position sentence.
	ReleaseSentence(&m_mouseY);

	// Release the mouse X position sentence.
	ReleaseSentence(&m_mouseX);
	
	// Release the number of triangles sentence.
	ReleaseSentence(&m_numOfTriangles);

	// Release the timer sentence.
	ReleaseSentence(&m_timer);

	// Release the CPU usage sentence.
	ReleaseSentence(&m_cpu);

	// Release the FPS counter sentence.
	ReleaseSentence(&m_fps);

	ReleaseSentence(&m_videoMemory);
	ReleaseSentence(&m_videoCard);


	// Release the font shader object.
	if (m_FontShader)
	{
		m_FontShader->Shutdown();
		delete m_FontShader;
		m_FontShader = nullptr;
	}

	// Release the font object.
	if (m_Font)
	{
		m_Font->Shutdown();
		delete m_Font;
		m_Font = nullptr;
	}

	return;
}

/// ----------------------------------------------------------------------
/// Draw sentences to the screen.
/// ----------------------------------------------------------------------
bool CText::Render(ID3D11DeviceContext *deviceContext,
	XMMATRIX worldMatrix, XMMATRIX orthoMatrix)
{
	bool result;


	// Draw the video card name sentence.
	result = RenderSentence(deviceContext, m_videoCard, worldMatrix, orthoMatrix);
	if (!result) { return false; }

	// Draw the video card memory capacity sentence.
	result = RenderSentence(deviceContext, m_videoMemory, worldMatrix, orthoMatrix);
	if (!result) { return false; }

	// Draw the FPS counter sentence.
	result = RenderSentence(deviceContext, m_fps, worldMatrix, orthoMatrix);
	if (!result) { return false; }

	// Draw the CPU usage sentence.
	result = RenderSentence(deviceContext, m_cpu, worldMatrix, orthoMatrix);
	if (!result) { return false; }

	// Draw the timer sentence.
	result = RenderSentence(deviceContext, m_timer, worldMatrix, orthoMatrix);
	if (!result) { return false; }

	// Draw the number of triangles sentence.
	result = RenderSentence(deviceContext, m_numOfTriangles, worldMatrix, orthoMatrix);
	if (!result) { return false; }

	// Draw the mouse X position sentence.
	result = RenderSentence(deviceContext, m_mouseX, worldMatrix, orthoMatrix);
	if (!result) { return false; }

	// Draw the mouse Y position sentence.
	result = RenderSentence(deviceContext, m_mouseY, worldMatrix, orthoMatrix);
	if (!result) { return false; }

	/// CAMERA ///
	// Draw the camera X position sentence.
	result = RenderSentence(deviceContext, m_posX, worldMatrix, orthoMatrix);
	if (!result) { return false; }

	// Draw the camera Y position sentence.
	result = RenderSentence(deviceContext, m_posY, worldMatrix, orthoMatrix);
	if (!result) { return false; }

	// Draw the camera Z position sentence.
	result = RenderSentence(deviceContext, m_posZ, worldMatrix, orthoMatrix);
	if (!result) { return false; }


	// Draw the camera X rotation sentence.
	result = RenderSentence(deviceContext, m_rotX, worldMatrix, orthoMatrix);
	if (!result) { return false; }

	// Draw the camera Y rotation sentence.
	result = RenderSentence(deviceContext, m_rotY, worldMatrix, orthoMatrix);
	if (!result) { return false; }

	// Draw the camera Z rotation sentence.
	result = RenderSentence(deviceContext, m_rotZ, worldMatrix, orthoMatrix);
	if (!result) { return false; }


	return true;
}

bool CText::SetVideoCard(char *videoCardName, int videoCardMemory, 
	ID3D11DeviceContext *deviceContext)
{
	char videoString[144];
	char memoryString[32];	
	char tempString[16];
	bool result;

	// Setup the video card strings.
	strcpy_s(videoString, "Video Card: ");
	strcat_s(videoString, videoCardName);

	_itoa_s(videoCardMemory, tempString, 10);

	strcpy_s(memoryString, "Video Memory: ");
	strcat_s(memoryString, tempString);
	strcat_s(memoryString, " MB");
	
	// Initialize the video text strings.
	result = UpdateSentence(m_videoCard, videoString, 20, 20,
		0.0f, 1.0f, 0.0f, deviceContext);
	if (!result)
	{
		return false;
	}

	result = UpdateSentence(m_videoMemory, memoryString, 20, 40,
		0.0f, 1.0f, 0.0f, deviceContext);
	if (!result)
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Converts the FPS counter int a string and then updates 
/// the sentences so that the counter can be rendered to the screen.
/// ----------------------------------------------------------------------
bool CText::SetFPS(int fps, ID3D11DeviceContext *deviceContext)
{
	char tempString[16];
	char fpsString[16];
	float red, green, blue;
	bool result;


	// Truncate the FPS to below 10k.
	if (fps > 9999)
	{
		fps = 9999;
	}

	// Convert the fps integer to string format.
	_itoa_s(fps, tempString, 10);

	// Setup the fps string.
	strcpy_s(fpsString, "FPS: ");
	strcat_s(fpsString, tempString);

	// If FPS is 60 or above set the FPS color to green.
	if (fps >= 60)
	{
		red = 0.0f;
		green = 1.0f;
		blue = 0.0f;
	}

	// If FPS is below 60, set the FPS color to yellow.
	if (fps < 60)
	{ 
		red = 1.0f;
		green = 1.0f;
		blue = 1.0f;
	}

	// If FPS is below 30, set the FPS color to red.
	if (fps < 30)
	{
		red = 1.0f;
		green = 0.0f;
		blue = 0.0f;
	}

	// Update the sentence vertex buffer with the new string information.
	result = UpdateSentence(m_fps, fpsString, 20, 70,
		red, green, blue, deviceContext);
	if (!result)
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Converts the CPU usage value to a string and then updates 
/// the sentences so that the counter can be rendered to the screen.
/// ----------------------------------------------------------------------
bool CText::SetCPU(int cpu, ID3D11DeviceContext *deviceContext)
{
	char tempString[16];
	char cpuString[16];
	bool result;


	// Convert the cpu integer to string format.
	_itoa_s(cpu, tempString, 10);

	// Setup the CPU usage string.
	strcpy_s(cpuString, "CPU: ");
	strcat_s(cpuString, tempString);
	strcat_s(cpuString, "%");

	// Update the sentence vertex buffer with the new string information.
	result = UpdateSentence(m_cpu, cpuString, 20, 90,
		0.0f, 1.0f, 0.0f, deviceContext);
	if (!result)
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Converts the time elapsed until the model is rendered to a string
/// and then updates the sentences so that the counter can be rendered 
/// to the screen.
/// ----------------------------------------------------------------------
bool CText::SetTimer(float timer, ID3D11DeviceContext *deviceContext)
{
	char tempString[16];
	char timerString[16];
	bool result;


	// Convert the cpu integer to string format.
	_itoa_s(static_cast<int>(timer), tempString, 10);

	// Setup the CPU usage string.
	strcpy_s(timerString, "Timer: ");
	strcat_s(timerString, tempString);

	// Update the sentence vertex buffer with the new string information.
	result = UpdateSentence(m_timer, timerString, 20, 110,
		0.0f, 1.0f, 0.0f, deviceContext);
	if (!result)
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Converts the time elapsed until the model is rendered to a string
/// and then updates the sentences so that the counter can be rendered 
/// to the screen.
/// ----------------------------------------------------------------------
bool CText::SetNumberOfTriangles(int num, ID3D11DeviceContext *deviceContext)
{
	char tempString[16];
	char trianglesString[16];
	bool result;


	// Convert the cpu integer to string format.
	_itoa_s(num, tempString, 10);

	// Setup the CPU usage string.
	strcpy_s(trianglesString, "T: ");
	strcat_s(trianglesString, tempString);

	// Update the sentence vertex buffer with the new string information.
	result = UpdateSentence(m_numOfTriangles, trianglesString, 20, 130,
		0.0f, 1.0f, 0.0f, deviceContext);
	if (!result)
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Converts the mouse X and Y position into two strings and then updates 
/// the two sentences so that the position of the mouse can be 
/// rendered to the screen.
/// ----------------------------------------------------------------------
bool CText::SetMousePosition(int mouseX, int mouseY,
	ID3D11DeviceContext *deviceContext)
{
	char tempString[16];
	char mouseString[16];
	bool result;


	// Convert the mouseX integer to string format.
	_itoa_s(mouseX, tempString, 10);

	// Setup the mouseX string.
	strcpy_s(mouseString, "Mouse X: ");
	strcat_s(mouseString, tempString);

	// Update the sentence vertex buffer with new string information.
	result = UpdateSentence(m_mouseX, mouseString, 
		20, 160, 1.0f, 1.0f, 1.0f, deviceContext);
	if (!result)
	{
		return false;
	}

	// Convert the mouseY integer to string format.
	_itoa_s(mouseY, tempString, 10);

	// Setup the mouseY string.
	strcpy_s(mouseString, "Mouse Y: ");
	strcat_s(mouseString, tempString);

	// Update the sentence vertex buffer with new string information.
	result = UpdateSentence(m_mouseY, mouseString,
		20, 180, 1.0f, 1.0f, 1.0f, deviceContext);
	if (!result)
	{
		return false;
	}

	return true;
}

bool CText::SetPosRot(int posX, int posY, int posZ,
	int rotX, int rotY, int rotZ, ID3D11DeviceContext *deviceContext)
{
	char tempString[16];
	char onScreenString[16];
	bool result;


	// Convert the posX integer to string format.
	_itoa_s(posX, tempString, 10);

	// Setup the posX string.
	strcpy_s(onScreenString, "Pos X: ");
	strcat_s(onScreenString, tempString);

	// Update the sentence vertex buffer with new string information.
	result = UpdateSentence(m_posX, onScreenString,
		20, 210, 1.0f, 1.0f, 1.0f, deviceContext);
	if (!result)
	{
		return false;
	}

	// Convert the posY integer to string format.
	_itoa_s(posY, tempString, 10);

	// Setup the posY string.
	strcpy_s(onScreenString, "Pos Y: ");
	strcat_s(onScreenString, tempString);

	// Update the sentence vertex buffer with new string information.
	result = UpdateSentence(m_posY, onScreenString,
		20, 230, 1.0f, 1.0f, 1.0f, deviceContext);
	if (!result)
	{
		return false;
	}

	// Convert the posY integer to string format.
	_itoa_s(posZ, tempString, 10);

	// Setup the posY string.
	strcpy_s(onScreenString, "Pos Z: ");
	strcat_s(onScreenString, tempString);

	// Update the sentence vertex buffer with new string information.
	result = UpdateSentence(m_posZ, onScreenString,
		20, 250, 1.0f, 1.0f, 1.0f, deviceContext);
	if (!result)
	{
		return false;
	}

	// Convert the posY integer to string format.
	_itoa_s(rotX, tempString, 10);

	// Setup the posY string.
	strcpy_s(onScreenString, "Rot X: ");
	strcat_s(onScreenString, tempString);

	// Update the sentence vertex buffer with new string information.
	result = UpdateSentence(m_rotX, onScreenString,
		20, 270, 1.0f, 1.0f, 1.0f, deviceContext);
	if (!result)
	{
		return false;
	}

	// Convert the posY integer to string format.
	_itoa_s(rotY, tempString, 10);

	// Setup the posY string.
	strcpy_s(onScreenString, "Rot Y: ");
	strcat_s(onScreenString, tempString);

	// Update the sentence vertex buffer with new string information.
	result = UpdateSentence(m_rotY, onScreenString,
		20, 290, 1.0f, 1.0f, 1.0f, deviceContext);
	if (!result)
	{
		return false;
	}

	// Convert the posY integer to string format.
	_itoa_s(rotZ, tempString, 10);

	// Setup the posY string.
	strcpy_s(onScreenString, "Rot Z: ");
	strcat_s(onScreenString, tempString);

	// Update the sentence vertex buffer with new string information.
	result = UpdateSentence(m_rotZ, onScreenString,
		20, 310, 1.0f, 1.0f, 1.0f, deviceContext);
	if (!result)
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Initialize vertex and index buffers for sentences.
/// ----------------------------------------------------------------------
bool CText::InitializeSentence(SSentence **sentence, int maxLength, 
	ID3D11Device *device)
{
	SVertex *vertices;
	unsigned long *indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT hr;


	// Create a new sentence object.
	*sentence = new SSentence;
	if (!*sentence)
	{
		return false;
	}

	// Initialize the sentence buffers to null.
	(*sentence)->vertexBuffer = nullptr;
	(*sentence)->indexBuffer = nullptr;

	// Set the maximum length of the sentence.
	(*sentence)->maxLength = maxLength;

	// Set the number of vertices in the vertex array.
	(*sentence)->vertexCount = 6 * maxLength;

	// Set the number of indexes in the index array.
	(*sentence)->indexCount = (*sentence)->vertexCount;

	// Create the vertex array.
	vertices = new SVertex[(*sentence)->vertexCount];
	if (!vertices)
	{
		return false;
	}

	// Create the index array.
	indices = new unsigned long[(*sentence)->indexCount];
	if (!indices)
	{
		return false;
	}

	// Initialize vertex arrat to zeros at first.
	memset(vertices, 0, sizeof(SVertex) * (*sentence)->vertexCount);

	// Initialize the index array.
	for (int i = 0; i < (*sentence)->indexCount; i++)
	{
		indices[i] = i;
	}

	// Set up the description of the dynamic vertex buffer.
	// Usage is dynamic, because sentence contents might be changed at any time.
	vertexBufferDesc.Usage					= D3D11_USAGE_DYNAMIC;
	vertexBufferDesc.ByteWidth				= sizeof(SVertex) * (*sentence)->vertexCount;
	vertexBufferDesc.BindFlags				= D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags			= D3D11_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags				= 0;
	vertexBufferDesc.StructureByteStride	= 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem			= vertices;
	vertexData.SysMemPitch		= 0;
	vertexData.SysMemSlicePitch = 0;

	// Create the vertex buffer.
	hr = device->CreateBuffer(&vertexBufferDesc, &vertexData, &(*sentence)->vertexBuffer);
	if (FAILED(hr))
	{
		return false;
	}

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage					= D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth				= sizeof(unsigned long) * (*sentence)->indexCount;
	indexBufferDesc.BindFlags				= D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags			= 0;
	indexBufferDesc.MiscFlags				= 0;
	indexBufferDesc.StructureByteStride		= 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem			= indices;
	indexData.SysMemPitch		= 0;
	indexData.SysMemSlicePitch	= 0;

	// Create the index buffer.
	hr = device->CreateBuffer(&indexBufferDesc, &indexData, &(*sentence)->indexBuffer);
	if (FAILED(hr))
	{
		return false;
	}

	// Release the index array as it is no longer needed.
	delete[] indices;
	indices = nullptr;

	// Release the vertex arrat as it is no longer needed.
	delete[] vertices;
	vertices = nullptr;

	return true;
}

/// ----------------------------------------------------------------------
/// Changes the contents of the vertex buffer for the input sentence.
/// ----------------------------------------------------------------------
bool CText::UpdateSentence(SSentence *sentence, char *text,
	int positionX, int positionY, float red, float green, float blue,
	ID3D11DeviceContext *deviceContext)
{
	int numLetters;
	SVertex *vertices, *verticesPtr;
	float drawX, drawY;
	HRESULT hr;
	D3D11_MAPPED_SUBRESOURCE mappedResource;


	// Store the color of the sentence.
	sentence->red = red;
	sentence->green = green;
	sentence->blue = blue;

	// Get the number of letters in the sentence.
	numLetters = static_cast<int>(strlen(text));

	// Check for possible buffer overflow.
	if (numLetters > sentence->maxLength)
	{
		return false;
	}

	// Create the vertex array.
	vertices = new SVertex[sentence->vertexCount];
	if (!vertices)
	{
		return false;
	}

	// Initialize vertex array to zeros at first.
	memset(vertices, 0, (sizeof(SVertex) * sentence->vertexCount));

	// Calculate the X and U pixel position on the screen to start drawing to.
	drawX = (float)(((m_screenWidth / 2) * -1) + positionX);
	drawY = (float)((m_screenHeight / 2) - positionY);

	// Use the font class to build the vertex arrat from the sentence text
	// and sentence draw location.
	m_Font->BuildVertexArray((void*)vertices, text, drawX, drawY);

	// Lock the vertex buffer so it can be written to.
	hr = deviceContext->Map(sentence->vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD,
		0, &mappedResource);
	if (FAILED(hr))
	{
		return false;
	}

	// Get a pointer to the data in the vertex buffer.
	verticesPtr = (SVertex*)mappedResource.pData;

	// Copy the data into the vertex buffer.
	memcpy(verticesPtr, (void*)vertices, sizeof(SVertex) * sentence->vertexCount);

	// Unlock the vertex buffer.
	deviceContext->Unmap(sentence->vertexBuffer, 0);

	// Release the vertex array as it is no longer needed.
	delete[] vertices;
	vertices = nullptr;

	return true;
}

/// ----------------------------------------------------------------------
/// Release the sentence vertex and index buffer as well as the sentence itself.
/// ----------------------------------------------------------------------
void CText::ReleaseSentence(SSentence **sentence)
{
	if (*sentence)
	{
		// Release the sentence vertex buffer.
		if ((*sentence)->vertexBuffer)
		{
			(*sentence)->vertexBuffer->Release();
			(*sentence)->vertexBuffer = nullptr;
		}

		// Release the sentence index buffer.
		if ((*sentence)->indexBuffer)
		{
			(*sentence)->indexBuffer->Release();
			(*sentence)->indexBuffer = nullptr;
		}

		// Release the sentence.
		delete *sentence;
		*sentence = nullptr;
	}

	return;
}

/// ----------------------------------------------------------------------
/// Puts the sentence vertex and index buffer on the input assembler 
/// and then calls the CFontShader object to draw the sentence 
/// that was given as input to this function. 
/// ----------------------------------------------------------------------
bool CText::RenderSentence(ID3D11DeviceContext *deviceContext, SSentence *sentence,
	XMMATRIX worldMatrix, XMMATRIX orthoMatrix)
{
	unsigned int stride, offset;
	XMFLOAT4 pixelColor;
	bool result;


	// Set vertex buffer stride and offset.
	stride = sizeof(SVertex);
	offset = 0;

	// Set the vertex buffer to active in the input assembler 
	// so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &sentence->vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler 
	// so it can be rendered.
	deviceContext->IASetIndexBuffer(sentence->indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer,
	// in this case triangles.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Create a pixel color vector with the input sentence color.
	pixelColor = XMFLOAT4(sentence->red, sentence->green, sentence->blue, 1.0f);

	// Render the text using the font shader.
	result = m_FontShader->Render(deviceContext, sentence->indexCount,
		worldMatrix, m_baseViewMatrix, orthoMatrix, m_Font->GetTexture(), pixelColor);
	if (!result) { return false; }

	return true;
}