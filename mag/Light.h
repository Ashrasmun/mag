#pragma once

#include <directxmath.h>

using namespace DirectX;

/// ----------------------------------------------------------------------
/// Maintain the direction and color of lights.
/// ----------------------------------------------------------------------
class CLight
{
public:
	CLight();
	CLight(const CLight &);
	~CLight();

	void SetAmbientColor(float, float, float, float);
	void SetDiffuseColor(float, float, float, float);
	void SetDirection(float, float, float);

	XMFLOAT4A GetAmbientColor();
	XMFLOAT4A GetDiffuseColor();
	XMFLOAT3A GetDirection();

private:
	XMFLOAT4A m_ambientColor;
	XMFLOAT4A m_diffuseColor;
	XMFLOAT3A m_direction;
};