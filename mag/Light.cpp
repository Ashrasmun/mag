#include "Light.h"

CLight::CLight() {}
CLight::CLight(const CLight &) {}
CLight::~CLight() {}

// RGBA
void CLight::SetAmbientColor(float red, float green, float blue, float alpha)
{
	m_ambientColor = XMFLOAT4A(red, green, blue, alpha);
	return;
}

// RGBA
void CLight::SetDiffuseColor(float red, float green, float blue, float alpha)
{
	m_diffuseColor = XMFLOAT4A(red, green, blue, alpha);
	return;
}

void CLight::SetDirection(float x, float y, float z)
{
	m_direction = XMFLOAT3A(x, y, z);
	return;
}

XMFLOAT4A CLight::GetAmbientColor()
{
	return m_ambientColor;
}

XMFLOAT4A CLight::GetDiffuseColor()
{
	return m_diffuseColor;
}

XMFLOAT3A CLight::GetDirection()
{
	return m_direction;
}