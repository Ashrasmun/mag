#include "Config.h"

namespace Config
{
	namespace Graphics
	{
		bool FULLSCREEN;
		bool VSYNC_ENABLED;
		float SCREEN_DEPTH;
		float SCREEN_NEAR;
	}

	namespace Grid
	{
		bool SHOW;
		int RESOLUTION;
		float SCALE;
	}

	namespace Camera
	{
		float MOVEMENT_SPEED;
		float ROTATION_SPEED;
		array<float, 3> STARTING_POSITION;
	}

	namespace Model
	{
		vector<string> FILENAME;
		string TEXTURE;
		string RENDERING_MODE;
		vector<array<float, 3>> ORIGIN;
		vector<array<float, 3>> POINT;
		vector<string> FUNCTION;
	}

	namespace Parameters
	{
		int MODEL_NUM;
		float PARAM_A;
		float BLINN_PARAM_R;
		float BLINN_PARAM_B;
		float PARAM_S, PARAM_D, PARAM_B;
		float THRESHOLD;
	}
}

CConfig::CConfig() {}
CConfig::CConfig(const CConfig &other) {}
CConfig::~CConfig() {}

/// ----------------------------------------------------------------------
/// Read the config file and fill Data structure with proper data.
/// ----------------------------------------------------------------------
bool CConfig::Initialize(string filename)
{
	ifstream fin;

	// Open the config file.
	fin.open(filename);

	// If it could not open the file, then exit.
	if (fin.fail()) { return false; }

	// Load data from file.
	ParseFile(fin);
	
	// Close the config file.
	fin.close();

	return true;
}

/// ----------------------------------------------------------------------
/// Parses file and reads in all the relevant data.
/// ----------------------------------------------------------------------
void CConfig::ParseFile(ifstream &fin)
{
	string line;
	int modelIndex = 1;
	int pointIndex = 1;
	int originIndex = 1;
	array< float, 3 > temparray;


	while (getline(fin, line))
	{
		istringstream sin(line.substr(line.find("= ") + 1));
		if (line[0] != '#') // Comment
		{

			if (line.find("graphics_") != -1)
			{
				if (line.find("fullscreen") != -1)
				{
					if (line.find("false") != -1)
					{
						Config::Graphics::FULLSCREEN = false;
						continue;
					}
					if (line.find("true") != -1)
					{
						Config::Graphics::FULLSCREEN = true;
						continue;
					}
				}
				if (line.find("vsync_enabled") != -1)
				{
					if (line.find("false") != -1)
					{
						Config::Graphics::VSYNC_ENABLED = false;
						continue;
					}
					if (line.find("true") != -1)
					{
						Config::Graphics::VSYNC_ENABLED = true;
						continue;
					}
				}
				if (line.find("screen_depth") != -1)
				{
					sin >> Config::Graphics::SCREEN_DEPTH;
					continue;
				}
				if (line.find("screen_near") != -1)
				{
					sin >> Config::Graphics::SCREEN_NEAR;
					continue;
				}
			}
			if (line.find("grid_") != -1)
			{
				if (line.find("show") != -1)
				{
					if (line.find("false") != -1)
					{
						Config::Grid::SHOW = false;
						continue;
					}
					if (line.find("true") != -1)
					{
						Config::Grid::SHOW = true;
						continue;
					}
				}
				if (line.find("resolution") != -1)
				{
					sin >> Config::Grid::RESOLUTION;
					Config::Grid::SCALE = 1;
					continue;
				}
				/// DEPRECATED
				//if (line.find("scale") != -1)
				//{
				//	sin >> Config::Grid::SCALE;
				//	continue;
				//}
			}
			if (line.find("camera_") != -1)
			{
				if (line.find("movement_speed") != -1)
				{
					sin >> Config::Camera::MOVEMENT_SPEED;
					continue;
				}
				if (line.find("rotation_speed") != -1)
				{
					sin >> Config::Camera::ROTATION_SPEED;
					continue;
				}
				if (line.find("starting_position") != -1)
				{
					string in, out;
					int pos = static_cast<int>(sin.tellg());
					while (pos != -1)
					{
						sin >> in;
						out += in;
						pos = static_cast<int>(sin.tellg());
					}
					ParsePoint(out, temparray);
					Config::Camera::STARTING_POSITION[0] = temparray[0]; // x
					Config::Camera::STARTING_POSITION[1] = temparray[1]; // y
					Config::Camera::STARTING_POSITION[2] = temparray[2]; // z
					continue;
				}
			}
			if (line.find("model_") != -1)
			{
				if (line.find("renderingmode") != -1)
				{
					sin >> Config::Model::RENDERING_MODE;
					continue;
				}
				if (line.find("file") != -1)
				{
					// Proper naming convention has to be obeyed.
					// First model is named model_1, second model_2 etc.
					// Any discrepancies are catched and signalised.
					stringstream ss(line.substr(line.find("i") + 4)); // model_fIle_X - points on x
					string s;
					int i;

					ss >> s;
					i = stoi(s);

					if (pointIndex == i)
					{
						string in, out;
						int pos = static_cast<int>(sin.tellg());
						while (pos != -1)
						{
							sin >> in;
							out += in;
							pos = static_cast<int>(sin.tellg());
						}
						Config::Model::FILENAME.push_back(out);
						pointIndex++;
						continue;
					}
				}
				if (line.find("origin_") != -1)
				{
					// Proper naming convention has to be obeyed.
					// First model is named model_1, second model_2 etc.
					// Any discrepancies are catched and signalised.
					stringstream ss(line.substr(line.find("n") + 2)); // model_origiN_X - points on x
					string s;
					int i;

					ss >> s;
					i = stoi(s);

					if (originIndex == i)
					{
						string in, out;
						int pos = static_cast<int>(sin.tellg());
						while (pos != -1)
						{
							sin >> in;
							out += in;
							pos = static_cast<int>(sin.tellg());
						}
						ParsePoint(out, temparray);
						Config::Model::ORIGIN.push_back(temparray);
						originIndex++;
						continue;
					}
				}
				if (line.find("texture") != -1)
				{
					sin >> Config::Model::TEXTURE;
					continue;
				}
				if (line.find("point_") != -1)
				{
					// Proper naming convention has to be obeyed.
					// First model is named model_1, second model_2 etc.
					// Any discrepancies are catched and signalised.
					stringstream ss(line.substr(line.find("t") + 2)); // model_poinT_X - points on x
					string s;
					int i;

					ss >> s;
					i = stoi(s);

					if (pointIndex == i)
					{
						string in, out;
						int pos = static_cast<int>(sin.tellg());
						while (pos != -1)
						{
							sin >> in;
							out += in;
							pos = static_cast<int>(sin.tellg());
						}
						ParsePoint(out, temparray);
						Config::Model::POINT.push_back(temparray);
						pointIndex++;
						continue;
					}
				}

				if (line.find("num") != -1)
				{
					sin >> Config::Parameters::MODEL_NUM;
					continue;
				}

				if (line.find("param_") != -1)
				{
					if (line.find("gauss_a") != -1)
					{
						sin >> Config::Parameters::PARAM_A;
						continue;
					}
					if (line.find("blinn_b") != -1)
					{
						sin >> Config::Parameters::BLINN_PARAM_B;
						continue;
					}
					if (line.find("blinn_r") != -1)
					{
						sin >> Config::Parameters::BLINN_PARAM_R;
						continue;
					}
					if (line.find("cauchy_s") != -1)
					{
						sin >> Config::Parameters::PARAM_S;
						continue;
					}
					if (line.find("meta_d") != -1)
					{
						sin >> Config::Parameters::PARAM_D;
						continue;
					}
					if (line.find("meta_b") != -1)
					{
						sin >> Config::Parameters::PARAM_B;
						continue;
					}
					if (line.find("threshold") != -1)
					{
						sin >> Config::Parameters::THRESHOLD;
						continue;
					}

				}

				if (line.find("function") != -1)
				{
					// Proper naming convention has to be obeyed.
					// First model is named model_1, second model_2 etc.
					// Any discrepancies are catched and signalised.
					stringstream ss(line.substr(line.find("_") + 10)); // model_function_X
					string s;
					int i;

					ss >> s;
					i = stoi(s);

					if (modelIndex == i)
					{
						string in, out;
						int pos = static_cast<int>(sin.tellg());
						while (pos != -1)
						{
							sin >> in;
							out += in;
							pos = static_cast<int>(sin.tellg());
						}
						Config::Model::FUNCTION.push_back(out);
						modelIndex++;
						continue;
					}
				}
			}
		}
	}

	return;
}

/// ----------------------------------------------------------------------
/// Parses point string into 3 float values.
/// ----------------------------------------------------------------------
bool CConfig::ParsePoint(string s, array<float, 3> &pos)
{
	int i = 1, argnum = 0;
	stringstream ss(s.substr(s.find("(") + 1));
	string temp;

	// Valid format should be:
	// model_point_1 = (0, 1, 3.4)

	// Check whether the string starts properly
	if (s[0] == '(')
	{
		while (i < s.length() && argnum < 3)
		{
			// If there's a number to parse.
			if (s[i] != ',' && s[i] != ')')
			{
				// Append one character from string s.
				temp.append(s, i, 1);
			}
			else
			{
				// Save the argument.
				pos[argnum] = stof(temp);

				// Flush the string.
				temp.clear();

				// Remember the fact, that argument has been found
				argnum++;
			}
			i++;
		}

		return true;
	}
	else
	{
		return false;
	}
}