#include "Texture.h"

CTexture::CTexture()
{
	m_targaData		= nullptr;
	m_texture		= nullptr;
	m_textureView	= nullptr;
}
CTexture::CTexture(const CTexture &other) {}
CTexture::~CTexture() {}

/// ----------------------------------------------------------------------
/// Load targa data, create a texture and fill it with targa data and
/// create a resource view of the texture for the shader to use.
/// ----------------------------------------------------------------------
bool CTexture::Initialize(ID3D11Device *device, ID3D11DeviceContext *deviceContext, string filename)
{
	bool result;
	int height, width;
	D3D11_TEXTURE2D_DESC textureDesc;
	HRESULT hr;
	unsigned int rowPitch;
	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;

	// Load the targa image data into memory.
	result = LoadTarga(filename, height, width);
	if (!result)
	{
		return false;
	}

	// Setup the description of the texture.
	textureDesc.Height				= height;
	textureDesc.Width				= width;
	textureDesc.MipLevels			= 0;
	textureDesc.ArraySize			= 1;
	textureDesc.Format				= DXGI_FORMAT_R8G8B8A8_UNORM;
	textureDesc.SampleDesc.Count	= 1;
	textureDesc.SampleDesc.Quality	= 0;
	textureDesc.Usage				= D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags			= D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
	textureDesc.CPUAccessFlags		= 0;
	textureDesc.MiscFlags			= D3D11_RESOURCE_MISC_GENERATE_MIPS;

	// Create the empty texture.
	hr = device->CreateTexture2D(&textureDesc, nullptr, &m_texture);
	if (FAILED(hr))
	{
		return false;
	}

	// Set the row pitch of the targa image data.
	rowPitch = (width * 4) * sizeof(unsigned char);

	// Copy the targa image data into the texture.
	deviceContext->UpdateSubresource(m_texture, 0, nullptr, m_targaData, rowPitch, 0);

	// Setup the shader resource view description.
	srvDesc.Format						= textureDesc.Format;
	srvDesc.ViewDimension				= D3D11_SRV_DIMENSION_TEXTURE2D;
	srvDesc.Texture2D.MostDetailedMip	= 0;
	srvDesc.Texture2D.MipLevels			= -1;

	// Create the shader resource view for the texture.
	hr = device->CreateShaderResourceView(m_texture, &srvDesc, &m_textureView);
	if (FAILED(hr))
	{
		return false;
	}

	// Generate mipmaps for this texture.
	deviceContext->GenerateMips(m_textureView);

	// Release the targa image data now that the image data has been loaded into the texture.
	delete[] m_targaData;
	m_targaData = nullptr;

	return true;
}

/// ----------------------------------------------------------------------
/// Releases the texture data.
/// ----------------------------------------------------------------------
void CTexture::Shutdown()
{
	// Release the texture view resource.
	if (m_textureView)
	{
		m_textureView->Release();
		m_textureView = nullptr;
	}

	// Release the texture.
	if (m_texture)
	{
		m_texture->Release();
		m_texture = nullptr;
	}

	// Release the targa data.
	if (m_targaData)
	{
		delete[] m_targaData;
		m_targaData = nullptr;
	}

	return;
}

ID3D11ShaderResourceView *CTexture::GetTexture()
{
	return m_textureView;
}

/// ----------------------------------------------------------------------
/// TGA image loading function.
/// ----------------------------------------------------------------------
bool CTexture::LoadTarga(string filename, int &height, int &width)
{
	int error, bpp, imageSize, index, i, j, k;
	FILE *filePtr;
	unsigned int count;
	TargaHeader targaFileHeader;
	unsigned char *targaImage;
	enum BPP { _24, _32 };
	BPP _bpp;
	/// WORKAROUND
	const char *cstr = filename.c_str();

	// Open the targa file for reading in binary.
	error = fopen_s(&filePtr, cstr, "rb");
	if (error != 0)
	{
		return false;
	}

	// Read in the file header.
	count = (unsigned int)fread(&targaFileHeader, sizeof(TargaHeader), 1, filePtr);
	if (count != 1)
	{
		return false;
	}

	// Get the important information from the header.
	height	= static_cast<int>(targaFileHeader.height);
	width	= static_cast<int>(targaFileHeader.width);
	bpp		= static_cast<int>(targaFileHeader.bpp);

	// Determine the format of TGA file.
	switch (bpp)
	{
	case 24:
		_bpp = _24;
		break;
	case 32:
		_bpp = _32;
		break;
	default:
		return false;
	}

	// Channels per pixel, 3 (BGR) or 4 (BGRA).
	int cpp = ((_bpp == _24) ? 3 : 4); 

	// Calculate the size of the 24/32bit image data.
	imageSize = width * height * cpp;

	// Allocate memory for the targa image data.
	targaImage = new unsigned char[imageSize];
	if (!targaImage)
	{
		return false;
	}

	// Read in the targa image data.
	count = (unsigned int)fread(targaImage, 1, imageSize, filePtr);
	if (count != imageSize)
	{
		return false;
	}

	// Close the file.
	error = fclose(filePtr);
	if (error != 0)
	{
		return false;
	}

	// Allocate memory for the targa destination data.
	m_targaData = new unsigned char[imageSize * 4 / cpp];
	if (!m_targaData)
	{
		return false;
	}

	// Initialize the index into the targa destination data array.
	index = 0;

	// Initialize the index into the targa image data.
	k = (width * height * cpp) - (width * cpp);

	// Now copy the targa image data into the targa destination array in the correct order
	// since the targa format is stored upside down.
	for (j = 0; j < height; j++)
	{
		for (i = 0; i < width; i++)
		{
			m_targaData[index + 0] = targaImage[k + 2]; // Red.
			m_targaData[index + 1] = targaImage[k + 1]; // Green.
			m_targaData[index + 2] = targaImage[k + 0]; // Blue.
			m_targaData[index + 3] = (cpp == 4) ? targaImage[k + 3] : 1; // Alpha.

			// Increment the indexes into the targa data.
			k += cpp;
			index += 4;
		}

		// Set the targa image data index back to the preceding row at the beginning of the column
		// since its reading it in upside down manner.
		k -= (width * 2 * cpp);
	}

	// Release the targa image data now that it was copierd into the destination array.
	delete[] targaImage;
	targaImage = nullptr;

	return true;
}