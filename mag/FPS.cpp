#include "FPS.h"

CFPS::CFPS() {}
CFPS::CFPS(const CFPS &other) {}
CFPS::~CFPS() {}

/// ----------------------------------------------------------------------
/// Sets all the counters to zero and starts the timer.
/// ----------------------------------------------------------------------
void CFPS::Initialize()
{
	m_fps = 0;
	m_count = 0;
	m_startTime = timeGetTime();
	return;
}

/// ----------------------------------------------------------------------
/// Counts number of frames and stores it every second.
/// ----------------------------------------------------------------------
void CFPS::Frame()
{
	m_count++;

	if (timeGetTime() >= (m_startTime + 1000))
	{
		m_fps = m_count;
		m_count = 0;
		m_startTime = timeGetTime();
	}

	return;
}

int CFPS::GetFPS()
{
	return m_fps;
}