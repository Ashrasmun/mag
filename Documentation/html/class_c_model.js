var class_c_model =
[
    [ "CModel", "class_c_model.html#a1ea1e9d8dab5ad230fb3698ae3c7d742", null ],
    [ "CModel", "class_c_model.html#aba8efb972c7d87d3a8b92760b8e925d0", null ],
    [ "~CModel", "class_c_model.html#a10d40f6d5be633108b1f12e01878491f", null ],
    [ "GetIndexCount", "class_c_model.html#a1afed8a225705539735c961442317f5d", null ],
    [ "GetTexture", "class_c_model.html#aa0a3eac9a117ee1124ef6b1ee689a9af", null ],
    [ "Initialize", "class_c_model.html#a22c0cbfc7edc9af9b134816adee1c69d", null ],
    [ "Render", "class_c_model.html#a9dc5928dd6900bd4d2a1d3c51ceb7fda", null ],
    [ "Shutdown", "class_c_model.html#a311a55b00b8ea92c2d69eb483a5544e3", null ]
];