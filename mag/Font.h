#pragma once

#include <d3d11.h>
#include <directxmath.h>
#include <fstream>
#include "Texture.h"

using namespace std;
using namespace DirectX;

/// ----------------------------------------------------------------------
/// Handle the texture for the font, the font data from 
/// the text file, and the function used to build
/// vertex buffers with the font data
/// DevNote: CFont is a reserved class from afxwin.h!
/// ----------------------------------------------------------------------
class CFont
{
private:
	struct SFont
	{
		float left, right;	// tu, texture coordinates
		int size;			// width of character in pixels
	};

	struct SVertex
	{
		XMFLOAT3 position;
		XMFLOAT2 texture;
	};

public:
	CFont();
	CFont(const CFont &);
	~CFont();

	bool Initialize(ID3D11Device *, ID3D11DeviceContext *, char *, char*);
	void Shutdown();

	ID3D11ShaderResourceView * GetTexture();

	void BuildVertexArray(void *, char *, float, float);

private:
	bool LoadFontData(char *);
	void ReleaseFontData();
	bool LoadTexture(ID3D11Device *, ID3D11DeviceContext *, char *);
	void ReleaseTexture();

private:
	SFont *m_Font;
	CTexture *m_Texture;
};