#pragma once

#include <vector>
#include "Config.h"

/// ----------------------------------------------------------------------
/// Parsing string from _config.txt to mathematical equation.
/// ----------------------------------------------------------------------
class CFunctionParser
{
private:
	enum EXPR { ADD, SUB, MUL, DIV, POW, NONE };

public:
	CFunctionParser();
	CFunctionParser(const CFunctionParser &);
	~CFunctionParser();	
	
	float Calculate(float, float, float, string);

	string StripRight(string);
	string StripLeft(string);
	string StripFromAbsolute(string);

private:
	void GetArgumentsAndExpressions(vector<string> &, vector<EXPR> &, string);
};

