#pragma once

#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <fstream>

using namespace DirectX;
using namespace std;

/// ----------------------------------------------------------------------
/// Class used to invoke HLSL shaders for drawing 
/// the 3D models that are on the GPU.
/// ----------------------------------------------------------------------
class CLightShader
{
private:
	struct SMatrixBuffer
	{
		XMMATRIX world;
		XMMATRIX view;
		XMMATRIX projection;
	};

	struct SLightBuffer
	{
		XMFLOAT4A ambientColor;
		XMFLOAT4A diffuseColor;
		XMFLOAT3A lightDirection;
	};

public:
	CLightShader();
	CLightShader(const CLightShader &);
	~CLightShader();

	bool Initialize(ID3D11Device *, HWND);
	void Shutdown();
	bool Render(ID3D11DeviceContext *, int, 
		XMMATRIX, XMMATRIX, XMMATRIX, 
		ID3D11ShaderResourceView *, 
		XMFLOAT3A, XMFLOAT4A, XMFLOAT4A);

private:
	bool InitializeShader(ID3D11Device *, HWND, WCHAR *, WCHAR *);
	void ShutdownShader();
	void OutputShaderErrorMessage(ID3D10Blob *, HWND, WCHAR *);

	bool SetShaderParameters(ID3D11DeviceContext *, 
		XMMATRIX, XMMATRIX, XMMATRIX, 
		ID3D11ShaderResourceView *, 
		XMFLOAT3A, XMFLOAT4A, XMFLOAT4A);
	void RenderShader(ID3D11DeviceContext *, int);

private:
	ID3D11VertexShader	*m_vertexShader;
	ID3D11PixelShader	*m_pixelShader;
	ID3D11InputLayout	*m_layout;
	ID3D11Buffer		*m_matrixBuffer;
	ID3D11SamplerState	*m_sampleState;
	ID3D11Buffer		*m_lightBuffer;
};