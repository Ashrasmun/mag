#pragma once

#include "Config.h"
#include <directxmath.h>

using namespace DirectX;
using namespace Config::Camera;

/// ----------------------------------------------------------------------
/// Keep track of where the camera is and its current rotation
/// ----------------------------------------------------------------------
class CCamera
{
public:
	CCamera();
	CCamera(const CCamera &);
	~CCamera();

	void SetPosition(float, float, float);
	void SetRotation(float, float, float);

	XMFLOAT3 GetPosition();
	XMFLOAT3 GetRotation();

	void Render();
	void Update(int, int, int);
	void GetViewMatrix(XMMATRIX &);

private:
	float m_positionX, m_positionY, m_positionZ;
	float m_rotationX, m_rotationY, m_rotationZ;
	XMMATRIX m_viewMatrix;
};