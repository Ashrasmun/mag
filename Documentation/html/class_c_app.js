var class_c_app =
[
    [ "CApp", "class_c_app.html#af14ddd76e4c5d54998308e299f2c0af5", null ],
    [ "CApp", "class_c_app.html#a722a79f37968f0d698b52050cd7cf9c1", null ],
    [ "~CApp", "class_c_app.html#ac83c58f4d11a0b2e73ca14ecea5eda46", null ],
    [ "Initialize", "class_c_app.html#a256b689a081b7b1312993ef5efdedb43", null ],
    [ "MessageHandler", "class_c_app.html#a7a42f971a1f66a3ce3d2e8e846adc3c2", null ],
    [ "Run", "class_c_app.html#ad659de8741a77816abbf8e7cc33dd186", null ],
    [ "Shutdown", "class_c_app.html#a1b34145338ee57271fe747d761a640a6", null ]
];