#include "App.h"

CApp::CApp() 
{
	//elapsedTime = 0.0f;
	m_Config = nullptr;
	m_Input		= nullptr;
	m_Graphics	= nullptr;
	// This is important because if the initialization of these objects fail 
	// then the Shutdown function further on will attempt to clean up those objects
	m_FPS = nullptr;
	m_CPU = nullptr;
	//m_Timer = nullptr;
}
CApp::CApp(const CApp& other) {}
CApp::~CApp() 
{
	// Cleaning up is done in Shutdown method
}

/// ----------------------------------------------------------------------
/// Initialize window, input and graphics
/// ----------------------------------------------------------------------
bool CApp::Initialize() 
{
	int screenWidth, screenHeight;
	bool result;


	// Create config object.
	m_Config = new CConfig;
	if (!m_Config) { return false; }

	// Initialize configuration from _config.txt
	result = m_Config->Initialize("../mag/data/_config.txt");
	if (!result) { return false; }

	// Initialize the width and height of the screen to zero before sending 
	// the variables into the function.
	screenWidth = 0;
	screenHeight = 0;

	// Initialize the windows API.
	InitializeWindows(screenWidth, screenHeight);

	// Create the input object.  This object will be used to handle reading 
	// the keyboard input from the user.
	m_Input = new CInput;
	if (!m_Input) { return false; }

	// Initialize the input object.
	result = m_Input->Initialize(m_hinstance, m_hwnd, screenWidth, screenHeight);
	if (!result)
	{
		MessageBox(m_hwnd, L"Could not initialize the input object.", 
			L"Error", MB_OK);
		return false;
	}

	// Create the FPS counter object.
	m_FPS = new CFPS;
	if (!m_FPS) { return false; }

	// Initialize the FPS counter object.
	m_FPS->Initialize();

	// Create the CPU counter object.
	m_CPU = new CCPU;
	if (!m_CPU) { return false; }

	// Initialize the CPU counter object.
	m_CPU->Initialize();

	// Create the timer object.
	//m_Timer = new CTimer;
	//if (!m_Timer) { return false; }

	// Initialize the timer object.
	//result = m_Timer->Initialize();
	//if (!result)
	//{
	//	MessageBox(m_hwnd, L"Could not initialize the Timer object.",
	//		L"Error", MB_OK);
	//	return false;
	//}

	// Create the graphics object.  This object will handle rendering 
	// all the graphics for this application.
	// This HAS TO BE executed after the initialization of timer,
	// so that the data about rendering time can be gathered.
	m_Graphics = new CGraphics;
	if (!m_Graphics) { return false; }

	// Initialize the graphics object.
	result = m_Graphics->Initialize(screenWidth, screenHeight, m_hwnd);
	if (!result) { return false; }

	return true;
}

/// ----------------------------------------------------------------------
/// Cleaning up and releasing everything
/// ----------------------------------------------------------------------
void CApp::Shutdown() 
{
	// Release the timer object.
	//if (m_Timer)
	//{
	//	delete m_Timer;
	//	m_Timer = nullptr;
	//}

	// Release the CPU counter object.
	if (m_CPU)
	{
		m_CPU->Shutdown();
		delete m_CPU;
		m_CPU = nullptr;
	}

	// Release the FPS counter object.
	if (m_FPS)
	{
		delete m_FPS;
		m_FPS = nullptr;
	}

	// Release the graphics object.
	if (m_Graphics)
	{
		m_Graphics->Shutdown();
		delete m_Graphics;
		m_Graphics = nullptr;
	}

	// Release the input object.
	if (m_Input)
	{
		m_Input->Shutdown();
		delete m_Input;
		m_Input = nullptr;
	}

	// Shutdown the window.
	ShutdownWindows();

	// Release the config object.
	if (m_Config)
	{
		delete m_Config;
		m_Config = nullptr;
	}

	return;
}

/// ----------------------------------------------------------------------
/// Application processing until user decides to quit
/// ----------------------------------------------------------------------
void CApp::Run() 
{
	MSG msg;
	bool done, result;

	// Initialize the message structure.
	ZeroMemory(&msg, sizeof(MSG));

	// Loop until there is a quit message from the window or the user.
	done = false;
	while (!done)
	{
		// Handle the windows messages.
		// PeekMessage has to be used instead of GetMessage so that
		// app is not stopping when not getting a message.
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		// If windows signals to end the application then exit out.
		if (msg.message == WM_QUIT)
		{
			done = true;
		}
		else
		{
			// Otherwise do the frame processing.
			result = Frame();
			if (!result)
			{
				MessageBox(m_hwnd, L"Frame processing failed.", L"Error", MB_OK);
				done = true;
			}
		}

		// Check if the user pressed escape and wants to quit.
		if (m_Input->IsEscapePressed() == true)
		{
			done = true;
		}
	}

	return;
}

/// ----------------------------------------------------------------------
/// Processing what has to be done in given frame. First it processes
/// the input, then the frame of graphics.
/// ----------------------------------------------------------------------
bool CApp::Frame() 
{
	bool result;
	int mouseX, mouseY,
		mousemoveX, mousemoveY;

	// Update the system stats.
	//m_Timer->Frame();
	m_FPS->Frame();
	m_CPU->Frame();

	// Do the input frame processing.
	result = m_Input->Frame();
	if (!result)
	{
		return false;
	}

	// Get the location and movement of the mouse from the input object.
	m_Input->GetMouseLocation(mouseX, mouseY);
	m_Input->GetMouseMovement(mousemoveX, mousemoveY);

	// Get the information about pressed keys.
	m_Input->SetDirection(NONE);
	if (m_Input->IsWPressed()) { m_Input->SetDirection(FORWARD); }
	if (m_Input->IsAPressed()) { m_Input->SetDirection(LEFT); }
	if (m_Input->IsSPressed()) { m_Input->SetDirection(BACKWARD); }
	if (m_Input->IsDPressed()) { m_Input->SetDirection(RIGHT); }
	if (m_Input->IsZPressed()) { m_Input->SetDirection(DOWN); }
	if (m_Input->IsSpacePressed()) { m_Input->SetDirection(UP); }

	//if (!m_Timer->IsRead())
	//	elapsedTime = m_Timer->GetTime();
		

	// Do the frame processing for the graphics object.
	result = m_Graphics->Frame(mouseX, mouseY, 
		static_cast<int>(m_Input->GetDirection()), mousemoveX, mousemoveY,
		m_FPS->GetFPS(), m_CPU->GetCPUPercentage()/*, elapsedTime*/);
	if (!result) {
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Windows system messages are directed here. This way program can listen
/// for certain information it's interested in.
/// ----------------------------------------------------------------------
LRESULT CALLBACK CApp::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, 
	LPARAM lparam) 
{
	return DefWindowProc(hwnd, umsg, wparam, lparam);
}

/// ----------------------------------------------------------------------
/// Build a window used to render to.
/// ----------------------------------------------------------------------
void CApp::InitializeWindows(int& screenWidth, int& screenHeight) 
{
	WNDCLASSEX wc;
	DEVMODE dmScreenSettings;
	int posX, posY;


	// Get an external pointer to this object.	
	ApplicationHandle = this;

	// Get the instance of this application.
	m_hinstance = GetModuleHandle(NULL);

	// Give the application a name.
	m_applicationName = L"Praca magisterska";

	// Setup the windows class with default settings.
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = m_hinstance;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm = wc.hIcon;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = m_applicationName;
	wc.cbSize = sizeof(WNDCLASSEX);

	// Register the window class.
	RegisterClassEx(&wc);

	// Determine the resolution of the clients desktop screen.
	screenWidth = GetSystemMetrics(SM_CXSCREEN);
	screenHeight = GetSystemMetrics(SM_CYSCREEN);

	// Setup the screen settings depending on whether it is running in full screen or in windowed mode.
	if (Config::Graphics::FULLSCREEN)
	{
		// If full screen set the screen to maximum size of the users desktop and 32bit.
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth = (unsigned long)screenWidth;
		dmScreenSettings.dmPelsHeight = (unsigned long)screenHeight;
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

		// Set the position of the window to the top left corner.
		posX = posY = 0;
	}
	else 
	{
		// If windowed then set it to 800x600 resolution.
		screenWidth = 800;
		screenHeight = 600;

		// Place the window in the middle of the screen.
		posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth) / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
	}

	// Create the window with the screen settings and get the handle to it.
	m_hwnd = CreateWindowEx(WS_EX_APPWINDOW, m_applicationName, m_applicationName,
		WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,
		posX, posY, screenWidth, screenHeight, NULL, NULL, m_hinstance, NULL);

	// Bring the window up on the screen and set it as main focus.
	ShowWindow(m_hwnd, SW_SHOW);
	SetForegroundWindow(m_hwnd);
	SetFocus(m_hwnd);

	// Hide the mouse cursor.
	ShowCursor(false);

	return;
}

/// ----------------------------------------------------------------------
/// Shuts windows down
/// ----------------------------------------------------------------------
void CApp::ShutdownWindows(/*bool fullscreen*/) 
{
	// Show the mouse cursor.
	ShowCursor(true);

	// Fix the display settings if leaving full screen mode.
	if (Config::Graphics::FULLSCREEN)
	{
		ChangeDisplaySettings(NULL, 0);
	}

	// Remove the window.
	DestroyWindow(m_hwnd);
	m_hwnd = NULL;

	// Remove the application instance.
	UnregisterClass(m_applicationName, m_hinstance);
	m_hinstance = NULL;

	// Release the pointer to this class.
	ApplicationHandle = nullptr;

	return;
}

/// ----------------------------------------------------------------------
/// WndProc function is where windows sends its messages to
/// ----------------------------------------------------------------------
LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, 
	LPARAM lparam) 
{
	switch (umessage) 
	{
		// Check if the window is being destroyed.
		case WM_DESTROY: 
		{
			PostQuitMessage(0);
			return 0;
		}

		// Check if the window is being closed.
		case WM_CLOSE: 
		{
			PostQuitMessage(0);
			return 0;
		}

		// All other messages pass to the message handler in the system class.
		default: 
		{
			return ApplicationHandle->MessageHandler(hwnd, umessage, wparam, lparam);
		}
	}
}