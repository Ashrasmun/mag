#pragma once

#include <array>
#include <vector>
#include "Config.h"

using namespace std;
using namespace Config::Grid;

/// ----------------------------------------------------------------------
/// This class contains all data necessary for marching cubes algorithm 
/// ----------------------------------------------------------------------
class CMarchingCube
{
private:
	// Array of unscaled parameters for every edge.
	float ue[12][3] = 
	{
		//   X,    Y,    Z 
		/// ////// ///
		/// BOTTOM ///
		/// ////// ///
		{ 0.5f, 0.0f, 0.0f},
		{ 1.0f, 0.0f, 0.5f},
		{ 0.5f, 0.0f, 1.0f},
		{ 0.0f, 0.0f, 0.5f},

		/// ///// ///
		/// SIDES ///
		/// ///// ///
		{ 0.0f, 0.5f, 0.0f},
		{ 1.0f, 0.5f, 0.0f},
		{ 1.0f, 0.5f, 1.0f},
		{ 0.0f, 0.5f, 1.0f},

		/// /// ///
		/// TOP ///
		/// /// ///
		{ 0.5f, 1.0f, 0.0f},
		{ 1.0f, 1.0f, 0.5f},
		{ 0.5f, 1.0f, 1.0f},
		{ 0.0f, 1.0f, 0.5f}
	};

	float edges[12][3];

	void CreateTriangle(vector< array<float, 8> >  &, int, int, int);
	void CreateNormal(int, int, int, float(&)[3]);
public:
	CMarchingCube();
	~CMarchingCube();

	bool Check(int, int, int, int/*vector< vector< vector<bool> > >*/, vector< array<float, 8> > &);
};

