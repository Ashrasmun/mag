#include "FileParser.h"

CFileParser::CFileParser() 
{
	m_newFilename	= nullptr;
}
CFileParser::CFileParser(const CFileParser &other) {}
CFileParser::~CFileParser() {}

/// ----------------------------------------------------------------------
/// This function checks for extension only. 
/// It doesn't check validity of whole file.
/// ----------------------------------------------------------------------
bool CFileParser::CheckFilenameExtension(string &filename)
{
	bool result;
	// Skip first 2 dots if they are present.
	int i = (filename[0] == '.' && filename[1] == '.') ? i = 2 : i = 0;


	// Determine the extension of the file.
	// Go to file extension.
	while (filename[i] != '.')
	{
		i++;
	}

	// If the file is a Wavefront .obj type file.
	if (filename[i + 1] == 'o' &&
		filename[i + 2] == 'b' &&
		filename[i + 3] == 'j')
	{
		// Gather information from .obj file and store it
		result = ParseOBJFileData(filename);
		if (!result)
		{
			return false;
		}

		// Create new, properly formated file and fill it with data
		result = CreateParsedFile(filename);
		if (!result)
		{
			return false;
		}
	}
	
	if (filename[i + 1] != 't' ||
		filename[i + 2] != 'x' ||
		filename[i + 3] != 't')
	{
		// If the type is unrecognisable.
		return false;
	}
	
	return true;
}

/// ----------------------------------------------------------------------
/// Creates a .txt file with properly placed data, so that CModel::LoadModel
/// can handle the data without problems.
/// ----------------------------------------------------------------------
bool CFileParser::CreateParsedFile(string &filename)
{
	ofstream fout;

	// Create filename.
	filename.erase(filename.end() - 3, filename.end());
	filename.append("txt");

	// Create new file and open it.
	fout.open(filename,
		fstream::binary |
		fstream::in |
		fstream::out |
		fstream::trunc);
	if (!fout)
	{
		return false;
	}

	// Fill it with data stored within this class.
	fout << "Vertex Count: " << m_finalVertexCount << '\n'
		<< '\n'
		<< "Data:" << '\n';
	
	for each(array<float, 8> ar in m_finalVertices)
	{
		fout << ar[0] << ' ' 	// x
			<< ar[1] << ' ' 	// y
			<< ar[2] << ' ' 	// z
			<< ar[3] << ' ' 	// tu
			<< ar[4] << ' ' 	// tv
			<< ar[5] << ' ' 	// nx
			<< ar[6] << ' ' 	// ny
			<< ar[7] << ' ';	// nz
			//<< '\n';
	}
		
	// Flush the data.
	m_geometricVertices.clear();
	m_textureVertices.clear();
	m_normalVertices.clear();
	m_finalVertices.clear();
	m_finalVertexCount = 0;

	// Close the file.
	fout.close();

	return true;
}

/// ----------------------------------------------------------------------
/// Primitive, quick parser that handles .obj files created in Blender v2.77
/// with Write Normals, Include UVs and Triangulate Faces options enabled.
/// Given that Blender exports .obj files with right-hand coordinate
/// system, adjustment to data is made here to accomodate to this.
/// ----------------------------------------------------------------------
bool CFileParser::ParseOBJFileData(string filename)
{
	ifstream fin;
	char input = 0;


	// Open the model file.
	fin.open(filename);

	// If it could not open the file, then exit.
	if (fin.fail())
	{
		return false;
	}

	// Parse the file.
	while (!fin.eof())
	{
		// Store vertex data
		if (input == 'v')
		{
			StoreVertexData(fin, input);

			// Get to the end of the line.
			while (input != '\n') { fin.get(input); }
		}
			

		// Assemble triangle data
		if (input == 'f')
		{
			// There are 3 vertices per each face
			// Drawing order has to be changed in order
			// to draw triangles correctly (counter cw -> cw).
			SetFinalVertexData(fin, input, m_triangle[2]);
			SetFinalVertexData(fin, input, m_triangle[1]);
			SetFinalVertexData(fin, input, m_triangle[0]);

			// Place data into the final vertex vector in correct order.
			m_finalVertices.push_back(m_triangle[0]);
			m_finalVertices.push_back(m_triangle[1]);
			m_finalVertices.push_back(m_triangle[2]);
			
			// Get to the end of the line.
			while (input != '\n') { fin.get(input); }
		}
			
		// Skip the unimportant data.
		if(input != 'v' && input != 'f' && input != '\n')
		{
			// Get to the end of the line.
			while (input != '\n') { fin.get(input); }
		}

		// Start reading new line and determine whether we check for 
		// vertex, triangle face or EOF.
		fin.get(input);
	}

	// Set the vertex count.
	m_finalVertexCount = static_cast<int>(m_finalVertices.size());

	// Close the model file.
	fin.close();

	return true;
}

/// ----------------------------------------------------------------------
/// Decide the type of vertex and store the data.
/// ----------------------------------------------------------------------
void CFileParser::StoreVertexData(ifstream &fin, char input)
{
	// Temporary arrays for positions, textures and normals
	array<float, 3> tmp3f;
	array<float, 2> tmp2f;


	// Check what kind of vertex are we coping with right now.
	fin.get(input);

	// Read geometric vertices and vertex count.
	if (input == ' ')
	{
		// Read position to temporary variables.
		fin >> tmp3f[0]		// x
			>> tmp3f[1]		// y
			>> tmp3f[2];	// z

		// Invert the Z coordinate.
		get<2>(tmp3f) *= -1;

		// Store the position.
		m_geometricVertices.push_back(tmp3f);
	}

	if (input == 't')
	{
		// Read texture vertex value to temporary variables.
		fin >> tmp2f[0]		// tu
			>> tmp2f[1];	// tv

		// Adjust the TV coordinate. 
		get<1>(tmp2f) = 1.0f - tmp2f[1];

		// Store texture vertex values.
		m_textureVertices.push_back(tmp2f);
	}

	if (input == 'n')
	{
		// Read normal to temporary variables.
		fin >> tmp3f[0]		// nx
			>> tmp3f[1]		// ny
			>> tmp3f[2];	// nz

		// Invert the NZ coordinate. 
		get<2>(tmp3f) *= -1;

		// Store normal vertex values.
		m_normalVertices.push_back(tmp3f);
	}
}

/// ----------------------------------------------------------------------
/// After collecting all the data about the model, place it into an array 
/// that constitutes how final vertex is described.
/// ----------------------------------------------------------------------
void CFileParser::SetFinalVertexData(ifstream &fin, char input, array<float, 8> &ar)
{
	int vIndex;


	// Read the vertex position's index.
	fin >> vIndex;

	// Place data into array[0..2]
	ar[0] = get<0>(m_geometricVertices[vIndex - 1]);
	ar[1] = get<1>(m_geometricVertices[vIndex - 1]);
	ar[2] = get<2>(m_geometricVertices[vIndex - 1]);

	// Skip the '/'.
	fin.get(input);

	// Read the texture coordinate's index.
	fin >> vIndex;

	// Place data into array[3..4]
	ar[3] = get<0>(m_textureVertices[vIndex - 1]);
	ar[4] = get<1>(m_textureVertices[vIndex - 1]);

	// Skip the '/'.
	fin.get(input);

	// Read the normal vector's index.
	fin >> vIndex;

	// Place data into array[5..7]
	ar[5] = get<0>(m_normalVertices[vIndex - 1]);
	ar[6] = get<1>(m_normalVertices[vIndex - 1]);
	ar[7] = get<2>(m_normalVertices[vIndex - 1]);
}