#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <array>

using namespace std;

namespace Config
{
	namespace Graphics
	{
		extern bool FULLSCREEN;
		extern bool VSYNC_ENABLED;
		extern float SCREEN_DEPTH;
		extern float SCREEN_NEAR;
	}

	namespace Grid
	{
		extern bool SHOW;
		extern int RESOLUTION;
		extern float SCALE;
	}

	namespace Camera
	{
		extern float MOVEMENT_SPEED;
		extern float ROTATION_SPEED;
		extern array<float, 3> STARTING_POSITION;
	}

	namespace Model
	{
		extern string TEXTURE;
		extern string RENDERING_MODE;
		extern vector<string> FILENAME;
		extern vector<array<float, 3>> ORIGIN;
		extern vector<array<float, 3>> POINT;
		extern vector<string> FUNCTION;
	}

	namespace Parameters
	{
		extern int MODEL_NUM;
		extern float PARAM_A;
		extern float BLINN_PARAM_R;
		extern float BLINN_PARAM_B;
		extern float PARAM_S, PARAM_D, PARAM_B;
		extern float THRESHOLD;
	}
}

/// ----------------------------------------------------------------------
/// Collect and distribute configuration data from _config.txt file
/// ----------------------------------------------------------------------
class CConfig
{
public:
	CConfig();
	CConfig(const CConfig &other);
	~CConfig();

	bool Initialize(string);

private:
	void ParseFile(ifstream &);
	bool ParsePoint(string, array<float, 3> &);
};

