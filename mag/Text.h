#pragma once

#include "Font.h"
#include "FontShader.h"

/// ----------------------------------------------------------------------
/// Handles all the 2D text drawing that the application will need.
/// ----------------------------------------------------------------------
class CText
{
private:
	struct SSentence
	{
		ID3D11Buffer *vertexBuffer, *indexBuffer;
		int vertexCount, indexCount, maxLength;
		float red, green, blue;
	};

	struct SVertex
	{
		XMFLOAT3 position;
		XMFLOAT2 texture;
	};

public:
	CText();
	CText(const CText &);
	~CText();

	bool Initialize(ID3D11Device *, ID3D11DeviceContext *, 
		HWND, int, int, XMMATRIX);
	void Shutdown();
	bool Render(ID3D11DeviceContext *, 
		XMMATRIX, XMMATRIX);

	bool SetVideoCard(char *, int, ID3D11DeviceContext *);
	bool SetFPS(int, ID3D11DeviceContext *);
	bool SetCPU(int, ID3D11DeviceContext *);
	bool SetTimer(float, ID3D11DeviceContext *);
	bool SetNumberOfTriangles(int, ID3D11DeviceContext *);
	bool SetMousePosition(int, int, ID3D11DeviceContext *);
	bool SetPosRot(int, int, int, int, int, int, ID3D11DeviceContext *);

private:
	bool InitializeSentence(SSentence **, int, 
		ID3D11Device *);
	bool UpdateSentence(SSentence *, char *,
		int, int, float, float, float, ID3D11DeviceContext *);
	void ReleaseSentence(SSentence **);
	bool RenderSentence(ID3D11DeviceContext *, SSentence *, 
		XMMATRIX, XMMATRIX);

private:
	CFont *m_Font;
	CFontShader *m_FontShader;
	int m_screenWidth, m_screenHeight;
	XMMATRIX m_baseViewMatrix;

	SSentence *m_videoCard, *m_videoMemory;
	SSentence *m_fps;
	SSentence *m_cpu;
	SSentence *m_timer;
	SSentence *m_numOfTriangles;
	SSentence *m_mouseX, *m_mouseY;
	/// CAMERA ///
	SSentence *m_posX, *m_posY, *m_posZ, *m_rotX, *m_rotY, *m_rotZ;
};