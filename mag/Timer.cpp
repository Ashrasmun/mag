#include "Timer.h"

CTimer::CTimer()
{
	isRead = false;
}
CTimer::~CTimer() {}

/// ----------------------------------------------------------------------
/// Queries the system to see if it supports high frequency timers.
/// If it returns a frequency then we use that value to determine 
/// how many counter ticks will occur each millisecond.
/// ----------------------------------------------------------------------
bool CTimer::Initialize()
{
	// Check to see if this system supports high performance timers.
	QueryPerformanceFrequency((LARGE_INTEGER*)&m_frequency);
	if (m_frequency == 0)
	{
		return false;
	}

	// Find out how mamny times the frequency counter ticks every milisecond
	m_ticksPerMs = (float)(m_frequency / 1000);

	// Query for current time.
	QueryPerformanceCounter((LARGE_INTEGER*)&m_startTime);

	m_elapsedTime = 0;
	return true;
}

/// ----------------------------------------------------------------------
/// Calculates the difference of time between loops and determine the time 
/// it took to execute this frame. 
/// ----------------------------------------------------------------------
void CTimer::Frame()
{
	INT64 currentTime;
	float timeDifference;


	QueryPerformanceCounter((LARGE_INTEGER*)&currentTime);

	timeDifference = (float)(currentTime - m_startTime);

	// How long did this frame last.
	m_frameTime = timeDifference / m_ticksPerMs;

	// Calculate elapsed time from the beginning of the scene rendering.
	m_elapsedTime += m_frameTime;

	// Start of the next frame.
	m_startTime = currentTime;

	return;
}

float CTimer::GetTime()
{
	return m_elapsedTime;
}

/// ----------------------------------------------------------------------
/// Check whether the timer is already read from. If it is, make sure that
/// this was the only time when it happened.
/// ----------------------------------------------------------------------
bool CTimer::IsRead()
{
	if (isRead)
	{
		return true;
	}
	else
	{
		isRead = true;
		return false;
	}
}