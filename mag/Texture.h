#pragma once

#include <d3d11.h>
#include <stdio.h>
#include <string>

using namespace std;

/// ----------------------------------------------------------------------
/// This class encapsulates the loading, unloading, 
/// and accessing of a single texture resource. 
/// For each texture needed, an object of this class must be instantiated.
/// ----------------------------------------------------------------------
class CTexture
{
private:
	// To make reading .tga files easier
	struct TargaHeader
	{
		unsigned char data1[12];
		unsigned short width;
		unsigned short height;
		unsigned char bpp;
		unsigned char data2;
	};

public:
	CTexture();
	CTexture(const CTexture &);
	~CTexture();

	bool Initialize(ID3D11Device *, ID3D11DeviceContext *, string);
	void Shutdown();

	ID3D11ShaderResourceView *GetTexture();

private:
	// To support other formats, put more methods HERE.
	bool LoadTarga(string, int &, int &);

private:
	unsigned char *m_targaData;
	ID3D11Texture2D *m_texture;
	ID3D11ShaderResourceView *m_textureView;
};