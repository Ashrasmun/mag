#pragma once

#include <d3d11.h>
#include <d3dcompiler.h>
#include <directxmath.h>
#include <fstream>

using namespace std;
using namespace DirectX;

/// ----------------------------------------------------------------------
/// Class used to invoke HLSL shaders for drawing the font textures.
/// ----------------------------------------------------------------------
class CFontShader
{
private:
	struct SConstantBuffer
	{
		XMMATRIX world;
		XMMATRIX view;
		XMMATRIX projection;
	};

	struct SPixelBuffer
	{
		XMFLOAT4 pixelColor;
	};

public:
	CFontShader();
	CFontShader(const CFontShader &);
	~CFontShader();

	bool Initialize(ID3D11Device *, HWND);
	void Shutdown();
	bool Render(ID3D11DeviceContext *, int,
		XMMATRIX, XMMATRIX, XMMATRIX,
		ID3D11ShaderResourceView *,
		XMFLOAT4);

private:
	bool InitializeShader(ID3D11Device *, HWND, WCHAR *, WCHAR *);
	void ShutdownShader();
	void OutputShaderErrorMessage(ID3D10Blob *, HWND, WCHAR *);

	bool SetShaderParameters(ID3D11DeviceContext *, 
		XMMATRIX, XMMATRIX, XMMATRIX, 
		ID3D11ShaderResourceView *,
		XMFLOAT4);
	void RenderShader(ID3D11DeviceContext *, int);

private:
	ID3D11VertexShader	*m_vertexShader;
	ID3D11PixelShader	*m_pixelShader;
	ID3D11InputLayout	*m_layout;
	ID3D11Buffer		*m_constantBuffer;
	ID3D11SamplerState	*m_sampleState;
	ID3D11Buffer		*m_pixelBuffer;
};