#pragma once

#pragma comment(lib, "winmm.lib")

#include <windows.h>
#include <mmsystem.h>

/// ----------------------------------------------------------------------
/// Handles recording the frames per second that the application is running at.
/// It counts how many frames occur in a one second period and 
/// constantly updates that count.
/// ----------------------------------------------------------------------
class CFPS
{
public:
	CFPS();
	CFPS(const CFPS &);
	~CFPS();

	void Initialize();
	void Frame();
	int GetFPS();

private:
	int m_fps, m_count;
	unsigned long m_startTime;
};