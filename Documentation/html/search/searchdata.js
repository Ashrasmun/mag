var indexSectionsWithContent =
{
  0: "abcdefgilmnoprstuvw~",
  1: "c",
  2: "c",
  3: "acdfgilmt",
  4: "bcefgimprstuw~",
  5: "bfmoprstv",
  6: "d",
  7: "bdflnru",
  8: "dw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

