var class_c_light =
[
    [ "CLight", "class_c_light.html#a6428960cf198a7a9644e3146cf59f82e", null ],
    [ "CLight", "class_c_light.html#aead5987188b3d3adb3c4d1a74b60bdfb", null ],
    [ "~CLight", "class_c_light.html#aef6c25b7608cf6776c3a627c03f70ff2", null ],
    [ "GetAmbientColor", "class_c_light.html#af3b931ea5b772a7714354e0f632f11f2", null ],
    [ "GetDiffuseColor", "class_c_light.html#aa9669fa03f0e9ff21d01870151d8d6be", null ],
    [ "GetDirection", "class_c_light.html#a419762a0c4a7e364dd3ac8636aa2b66e", null ],
    [ "SetAmbientColor", "class_c_light.html#a09fa8198e8df80ccf3ec14245db7a3bd", null ],
    [ "SetDiffuseColor", "class_c_light.html#aaa14ae5df24dee6f1764542c21c72ee0", null ],
    [ "SetDirection", "class_c_light.html#aec02bebbef890e95d9c0a98494c7229a", null ]
];