var class_c_direct3_d =
[
    [ "CDirect3D", "class_c_direct3_d.html#abe4b16e2e7b4d9e28715e93072fb4079", null ],
    [ "CDirect3D", "class_c_direct3_d.html#a87183305caadb2051628681771a8236e", null ],
    [ "~CDirect3D", "class_c_direct3_d.html#a963418bec9b30ce1af7d93bc289f8e55", null ],
    [ "BeginScene", "class_c_direct3_d.html#a5c977980479a3d658827ab2176b92a19", null ],
    [ "EndScene", "class_c_direct3_d.html#adec450bc34008996fae171cdeaab13b6", null ],
    [ "GetDevice", "class_c_direct3_d.html#a9fb8b2c1ae643d82096702a30d11cc15", null ],
    [ "GetDeviceContext", "class_c_direct3_d.html#a940d0d0fb4bdd31672f43113bca3ca87", null ],
    [ "GetOrthoMatrix", "class_c_direct3_d.html#a799d408b31f420a0bf7f3f79525b5c0d", null ],
    [ "GetProjectionMatrix", "class_c_direct3_d.html#adbf7ecadd197cc6c1e625e9af7ea4e82", null ],
    [ "GetVideoCardInfo", "class_c_direct3_d.html#a1238bcb008c42595266f72a965e01cdc", null ],
    [ "GetWorldMatrix", "class_c_direct3_d.html#a136eb6e92ad55d23dbca0da8c07fe19d", null ],
    [ "Initialize", "class_c_direct3_d.html#ad371b5c01e6e25dc836ee14d5e64bd36", null ],
    [ "Shutdown", "class_c_direct3_d.html#af75afcea0606c94d18c22fa46ebcdbcb", null ],
    [ "TurnAlphaBlendingOff", "class_c_direct3_d.html#afb75be4962661f19625816be5adcfe64", null ],
    [ "TurnAlphaBlendingOn", "class_c_direct3_d.html#a1561d5f934b9a0e078824c5018d247c3", null ],
    [ "TurnZBufferOff", "class_c_direct3_d.html#a05ab2b7c786d366f4ab3853edcd260fd", null ],
    [ "TurnZBufferOn", "class_c_direct3_d.html#a085bb97c89d6d0db8c6c2862a3e431c2", null ]
];