#include "Model.h"

CModel::CModel()
{
	m_vertexCount = 0;
	m_vertexBuffer = nullptr;
	m_indexBuffer = nullptr;
	//m_classicModel = nullptr;
	m_Texture = nullptr;
	m_FileParser = nullptr;
	m_FunctionParser = nullptr;
	m_MarchingCube = nullptr;
}
CModel::CModel(const CModel &other) {}
CModel::~CModel() {}

/// ----------------------------------------------------------------------
/// Initialization functions for the vertex and index buffers.
/// This function loads models and prepare them to be rendered by using
/// the Marching cubes algorithm.
/// ----------------------------------------------------------------------
bool CModel::Initialize(ID3D11Device *device, ID3D11DeviceContext *deviceContext,
	int &triangles, float &calculationTime)
{
	bool result;
	float startTime = 0.0f;

	// Create the timer object.
	m_Timer = new CTimer;
	if (!m_Timer) { return false; }

	// Initialize the timer object.
	result = m_Timer->Initialize();
	if (!result) { return false; }


	if (RENDERING_MODE == "CLASSIC")
	{
		// Initialize the file parser.
		result = InitializeFileParser();
		if (!result)
		{
			return false;
		}

		int i = 0;
		for (string filename : FILENAME)
		{
			// Check the filename. If it's .obj, parse it to your own .txt format
			result = m_FileParser->CheckFilenameExtension(filename);
			if (!result)
			{
				return false;
			}

			// Load in the model data.
			result = LoadModel(filename, i++);
			if (!result)
			{
				return false;
			}
		}

		// Initialize the vertex and index buffers.
		result = InitializeBuffers(device);
		if (!result)
		{
			return false;
		}

		// Load the texture for this model.
		result = LoadTexture(device, deviceContext, TEXTURE);
		if (!result)
		{
			return false;
		}

		triangles = m_vertexCount / 3;

		return true;
	}

	if (RENDERING_MODE == "CONVOLUTION_SURFACE")
	{
		// Initialize the file parser.
		result = InitializeFileParser();
		if (!result)
		{
			return false;
		}

		// If there's a model to load.
		int i = 0;
		for (string filename : FILENAME)
		{
			// Check the filename. If it's .obj, parse it to your own .txt format
			result = m_FileParser->CheckFilenameExtension(filename);
			if (!result)
			{
				return false;
			}

			// Load in the model data.
			result = LoadModel(filename, i++);
			if (!result)
			{
				return false;
			}
		}

		/// MARCHING CUBES 

		// Start measuring the time
		m_Timer->Frame();
		startTime = m_Timer->GetTime();

		// Load the data from the file.
		PrepareModelData();

		// Measure the time after calculations.
		m_Timer->Frame();
		calculationTime = m_Timer->GetTime() - startTime;

		// Initialize marching cube.
		result = InitializeMarchingCube();
		if (!result)
		{
			return false;
		}
		// Use this data to create the model.
		CreatePolygons();
		/// END OF MARCHING CUBES

		// Initialize the vertex and index buffers.
		result = InitializeBuffers(device);
		if (!result)
		{
			return false;
		}

		// Load the texture for this model.
		result = LoadTexture(device, deviceContext, TEXTURE);
		if (!result)
		{
			return false;
		}

		triangles = m_vertexCount / 3;

		return true;
	}

	// Unrecognised rendering mode.
	return false;
}

/// ----------------------------------------------------------------------
/// Shutdown functions for the vertex and index buffers.
/// ----------------------------------------------------------------------
void CModel::Shutdown()
{
	// Release the model texture.
	ReleaseTexture();

	// Shutdown the vertex and index buffers.
	ShutdownBuffers();

	// Release the model data.
	//ReleaseModel();

	// Release marching cube object.
	ReleaseMarchingCube();

	// Release the function parser object.
	ReleaseFunctionParser();

	// Release the parser object.
	ReleaseFileParser();

	// Release the timer object.
	if (m_Timer)
	{
		delete m_Timer;
		m_Timer = nullptr;
	}

	return;
}

/// ----------------------------------------------------------------------
/// Puts the vertex and index buffers on the graphics pipeline 
/// so the color shader will be able to render them.
/// ----------------------------------------------------------------------
void CModel::Render(ID3D11DeviceContext *deviceContext)
{
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(deviceContext);

	return;
}

int CModel::GetIndexCount()
{
	return m_indexCount;
}

ID3D11ShaderResourceView * CModel::GetTexture()
{
	return m_Texture->GetTexture();
}

/// ----------------------------------------------------------------------
/// Handles the process of creating the vertex and index buffers.
/// ----------------------------------------------------------------------
bool CModel::InitializeBuffers(ID3D11Device *device)
{
	HRESULT hr;
	SVertex *vertices;
	unsigned long *indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;

	//m_vertexCount = 3;

	// Create the vertex array.
	vertices = new SVertex[m_vertexCount];
	if (!vertices)
	{
		return false;
	}

	// There are as many indices as there are vertices.
	m_indexCount = m_vertexCount;

	// Create the index array.
	indices = new unsigned long[m_indexCount];
	if (!indices)
	{
		return false;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// Load the vertex array and index array with data.
	if(RENDERING_MODE == "CLASSIC")
		for (int i = 0; i < m_vertexCount; i++)
		{
			vertices[i].position = XMFLOAT3(m_classicModel[i].x, m_classicModel[i].y, m_classicModel[i].z);
			vertices[i].texture = XMFLOAT2(m_classicModel[i].tu, m_classicModel[i].tv);
			vertices[i].normal = XMFLOAT3(m_classicModel[i].nx, m_classicModel[i].ny, m_classicModel[i].nz);

			indices[i] = i;
		}
	if(RENDERING_MODE == "CONVOLUTION_SURFACE")
		for (int i = 0; i < m_vertexCount; i++)
		{
			vertices[i].position = XMFLOAT3(m_isoModel[i].x, m_isoModel[i].y, m_isoModel[i].z);
			vertices[i].texture = XMFLOAT2(m_isoModel[i].tu, m_isoModel[i].tv);
			vertices[i].normal = XMFLOAT3(m_isoModel[i].nx, m_isoModel[i].ny, m_isoModel[i].nz);

			indices[i] = i;
		}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	// Set up the description of the static vertex buffer.
	vertexBufferDesc.Usage					= D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth				= sizeof(SVertex) * m_vertexCount;
	vertexBufferDesc.BindFlags				= D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags			= 0;
	vertexBufferDesc.MiscFlags				= 0;
	vertexBufferDesc.StructureByteStride	= 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem			= vertices;
	vertexData.SysMemPitch		= 0;
	vertexData.SysMemSlicePitch	= 0;

	// Now create the vertex buffer.
	hr = device->CreateBuffer(&vertexBufferDesc, &vertexData, &m_vertexBuffer);
	if (FAILED(hr))
	{
		return false;
	}

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage				= D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth			= sizeof(unsigned long) * m_indexCount;
	indexBufferDesc.BindFlags			= D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags		= 0;
	indexBufferDesc.MiscFlags			= 0;
	indexBufferDesc.StructureByteStride	= 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem			= indices;
	indexData.SysMemPitch		= 0;
	indexData.SysMemSlicePitch	= 0;

	// Create the index buffer.
	hr = device->CreateBuffer(&indexBufferDesc, &indexData, &m_indexBuffer);
	if (FAILED(hr))
	{
		return false;
	}

	// Release the arrays now that the vertex and index buffers 
	// have been created and loaded.
	delete[] vertices;
	vertices = nullptr;

	delete[] indices;
	indices = nullptr;

	return true;
}

/// ----------------------------------------------------------------------
/// Releases the vertex buffer and index buffer.
/// ----------------------------------------------------------------------
void CModel::ShutdownBuffers()
{
	// Release the index buffer.
	if (m_indexBuffer)
	{
		m_indexBuffer->Release();
		m_indexBuffer = nullptr;
	}

	// Release the vertex buffer.
	if (m_vertexBuffer)
	{
		m_vertexBuffer->Release();
		m_vertexBuffer = nullptr;
	}

	return;
}

/// ----------------------------------------------------------------------
/// Sets the vertex buffer and index buffer as active on the 
/// input assembler in the GPU.
/// ----------------------------------------------------------------------
void CModel::RenderBuffers(ID3D11DeviceContext *deviceContext)
{
	unsigned int stride;
	unsigned int offset;


	// Set vertex buffer stride and offset.
	stride = sizeof(SVertex);
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &m_vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(m_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from 
	// this vertex buffer, in this case triangles.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return;
}

/// ----------------------------------------------------------------------
/// Creates the texture object and then initialize it with the input file 
/// name provided. Called during initialization.
/// ----------------------------------------------------------------------
bool CModel::LoadTexture(ID3D11Device *device, ID3D11DeviceContext *deviceContext, 
	string filename)
{
	bool result;


	// Create the texture object.
	m_Texture = new CTexture;
	if (!m_Texture)
	{
		return false;
	}

	// Initialize the texture object.
	result = m_Texture->Initialize(device, deviceContext, filename);
	if (!result)
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Releases the texture object that was created and loaded 
/// during the LoadTexture function.
/// ----------------------------------------------------------------------
void CModel::ReleaseTexture()
{
	// Release the texture object.
	if (m_Texture)
	{
		m_Texture->Shutdown();
		delete m_Texture;
		m_Texture = nullptr;
	}

	return;
}

/// ----------------------------------------------------------------------
/// Loads the model data from the file into the m_model array variable.
/// ----------------------------------------------------------------------
bool CModel::LoadModel(string filename, int modelNumber)
{
	ifstream fin;
	char input;

	// Open the model file.
	fin.open(filename);

	// If it could not open the file, then exit.
	if (fin.fail())
	{
		return false;
	}
	
	// Read up to the value of vertex count.
	fin.get(input);
	while (input != ':')
	{
		fin.get(input);
	}

	// Read in the vertex count.
	fin >> m_partialVertexCount;

	// Set the number of indices to be the same as the vertex count.
	m_indexCount = m_partialVertexCount + m_vertexCount;

	// Create the model using the vertex count that was read in.
	//m_classicModel = new SModel[m_vertexCount];
	//if (!m_classicModel)
	//{
	//	return false;
	//}

	// Read up to the beginning of the data.
	fin.get(input);
	while (input != ':')
	{
		fin.get(input);
	}
	fin.get(input); // carriages
	fin.get(input);

	// Read in the vertex data.
	for (int i = m_vertexCount; i < m_partialVertexCount + m_vertexCount; i++)
	{
		fin >> m_point.x >> m_point.y >> m_point.z;
		fin >> m_point.tu >> m_point.tv;
		fin >> m_point.nx >> m_point.ny >> m_point.nz;
		m_classicModel.push_back(m_point);
	}

	// Close the model file.
	fin.close();

	// Move the model to the origin point.
	for (int i = m_vertexCount; i < m_partialVertexCount + m_vertexCount; i++)
	{
		m_classicModel[i].x += Config::Model::ORIGIN[modelNumber][0];
		m_classicModel[i].y += Config::Model::ORIGIN[modelNumber][1];
		m_classicModel[i].z += Config::Model::ORIGIN[modelNumber][2];
	}

	// Update the number of vertices from previous models.
	m_vertexCount += m_partialVertexCount;

	return true;
}

/// ----------------------------------------------------------------------
/// Fills m_data structure with results of calculating function.
/// m_data is filled with "true" if a point satisfies the function and
/// "false" otherwise.
/// ----------------------------------------------------------------------
void CModel::PrepareModelData()
{
	const float e = 2.71828182846f;
	float result = 0.0f, fres;
	int ti, tj, tk;
	vector< array< float, 3 > > data; // x, y, z, r
	array< float, 3 > point;

	// 0: no kernel function
	// 1: Gauss
	// 2: Cauchy
	// 3: Inverse function
	// 4: Inverse squared function
	// 5: Metaballs
	// 6: Soft objects
	// 7: Quartic polynomial
	// 8: Blinn - blobby
	int model = Config::Parameters::MODEL_NUM;;

	// Parameters
	float R = Config::Parameters::BLINN_PARAM_R,
		B = Config::Parameters::BLINN_PARAM_B,	// Blinn
		a = Config::Parameters::PARAM_A,
		s = Config::Parameters::PARAM_S, // Cauchy
		b = Config::Parameters::PARAM_B, 
		d = Config::Parameters::PARAM_D, // metaballs, soft objects and quartic polynomial
		r; // distance from examined point to point in cloud
	// Threshold
	float T = Config::Parameters::THRESHOLD;

	// Initialize m_data structure.
	for (int i = 0; i <= RESOLUTION; i++)
	{
		m_row.push_back(false);
		m_isorow.push_back(0.0f);
	}

	for (int i = 0; i <= RESOLUTION; i++)
	{
		m_slice.push_back(m_row);
		m_isoslice.push_back(m_isorow);
	}
	
	for (int i = 0; i <= RESOLUTION; i++)
	{
		m_data.push_back(m_slice);
		m_isodata.push_back(m_isoslice);
		
	}

	float init = -RESOLUTION / 2 * SCALE;
	float cond = RESOLUTION / 2 * SCALE;

	// Collect dataset in one place.
	// Convert data from m_classicModel to point cloud
	for (int i = 0; i < m_vertexCount; i++)
	{
		point[0] = m_classicModel[i].x;
		point[1] = m_classicModel[i].y;
		point[2] = m_classicModel[i].z;
		data.push_back(point);
	}
	// Take isolated points into consideration
	for (array<float, 3> p : Config::Model::POINT)
	{
		point[0] = p[0];
		point[1] = p[1];
		point[2] = p[2];
		data.push_back(point);
	}
	// Collect points that satisfy the equation given by functions
	for (string f : FUNCTION)
	{
		for (float z = init; z <= cond; z += SCALE)
		{
			for (float y = init; y <= cond; y += SCALE)
			{
				for (float x = init; x <= cond; x += SCALE)
				{
					// If the equation is satisfied, save the point.
					if (m_FunctionParser->Calculate(x, y, z, f) <= 0)
					{
						point[0] = x;
						point[1] = y;
						point[2] = z;
						data.push_back(point);
					}
				}
			}
		}
	}

	// Calculate the isosurface.
	for (float z = init; z <= cond; z += SCALE)
	{
		for (float y = init; y <= cond; y += SCALE)
		{
			for (float x = init; x <= cond; x += SCALE)
			{
				// Choose the kernel function.
				switch (model)
				{
				// 0: NO KERNEL FUNCTION - raw function
				case 0:
				{
					//result = 0;
					//
					//for (string f : FUNCTION)
					//{
					//	fres = m_FunctionParser->Calculate(x, y, z, f);
					//	if (fres <= 0)
					//	{
					//		result += 2 * T;
					//	}
					//	else
					//	{
					//		result += 0;
					//	}
					//}

					result = 0;

					for (array<float, 3> point : data)
					{
						if (point[0] == x && point[1] == y && point[2] == z)
						{
							result = T * 2;
						}
						else
						{
							result += 0;
						}
					}
				}
				break;
				// 1: Gauss
				case 1:
				{
					result = 0;

					// Calculate the isosurface value for each point of the mesh
					for (array< float, 3 > point : data)
					{
						// point[0] = point.x
						// point[1] = point.y
						// point[2] = point.z
						r = pow((x - point[0]), 2) + pow((y - point[1]), 2) + pow((z - point[2]), 2);
						
						// distance r is already squared!
						result += pow(e, (-pow(a, 2) * r));
					}
				}
					break;

				// 2: Cauchy
				case 2:
				{
					s = 0.001f * Config::Parameters::PARAM_S; 
					result = 0;

					// Calculate the isosurface value for each point of the mesh
					for (array< float, 3 > point : data)
					{
						// point[0] = point.x
						// point[1] = point.y
						// point[2] = point.z
						r = pow((x - point[0]), 2) + pow((y - point[1]), 2) + pow((z - point[2]), 2);

						// r > 0
						if (r == 0.0f)
							r = 0.0001f;

						// r is already squared!
						result += 1 / pow(1 + pow(s, 2) * r, 2);
					}
				}
					break;	

				// 3: Inverse function
				case 3:
				{
					s = 10.0f * Config::Parameters::PARAM_S;
					result = 0;

					// Calculate the isosurface value for each point of the mesh
					for (array< float, 3 > point : data)
					{
						// point[0] = point.x
						// point[1] = point.y
						// point[2] = point.z
						r = sqrt(pow((x - point[0]), 2) + pow((y - point[1]), 2) + pow((z - point[2]), 2));

						// r > 0
						if (r == 0.0f)
							r = 0.0001f;

						result += s / r;
					}
				}
					break;	

				// 4: Inverse squared function
				case 4:
				{
					s = 1000.0f * Config::Parameters::PARAM_S;
					result = 0;

					// Calculate the isosurface value for each point of the mesh
					for (array< float, 3 > point : data)
					{
						// point[0] = point.x
						// point[1] = point.y
						// point[2] = point.z
						r = pow((x - point[0]), 2) + pow((y - point[1]), 2) + pow((z - point[2]), 2);

						// r > 0
						if (r == 0.0f)
							r = 0.0001f;

						// r is already squared!
						result += s / r;
					}
				}
					break;	

				// 5: Metaballs
				case 5:
				{
					b = Config::Parameters::PARAM_B;
					d = RESOLUTION * Config::Parameters::PARAM_D;
					result = 0;

					// Calculate the isosurface value for each point of the mesh
					for (array< float, 3 > point : data)
					{
						// point[0] = point.x
						// point[1] = point.y
						// point[2] = point.z
						r = sqrt(pow((x - point[0]), 2) + pow((y - point[1]), 2) + pow((z - point[2]), 2));

						if (r > d)
							result += 0;
						else
							if (r > (d / 3))
								result += 1.5f * b * pow(1 - r / d, 2);
							else
								//if (r >= 0)
								result += b * (1 - 3 * pow(r, 2) / pow(d, 2));
					}
				}
					break;	

				// 6: Soft objects
				case 6:
				{
					b = Config::Parameters::PARAM_B;
					d = RESOLUTION * Config::Parameters::PARAM_D;
					result = 0;

					// Calculate the isosurface value for each point of the mesh
					for (array< float, 3 > point : data)
					{
						// point[0] = point.x
						// point[1] = point.y
						// point[2] = point.z
						r = pow((x - point[0]), 2) + pow((y - point[1]), 2) + pow((z - point[2]), 2);
						
						// r is already squared!
						if (sqrt(r) > d)
							result += 0;
						else
							result += b * (1 -
								(4 * pow(r, 3)) / (9 * pow(d, 6)) +
								(17 * pow(r, 2)) / (9 * pow(d, 4)) -
								(22 * r) / (9 * pow(d, 2)));
					}
				}
					break;

				// 7: Quartic polynomial
				case 7:
				{					
					b = Config::Parameters::PARAM_B;
					d = RESOLUTION * Config::Parameters::PARAM_D;
					result = 0;

					// Calculate the isosurface value for each point of the mesh
					for (array< float, 3 > point : data)
					{
						// point[0] = point.x
						// point[1] = point.y
						// point[2] = point.z
						r = pow((x - point[0]), 2) + pow((y - point[1]), 2) + pow((z - point[2]), 2);

						// r is already squared!
						if (sqrt(r) > d)
							result += 0;
						else
							result += b * pow(1 - r / pow(d, 2), 2);
					}
				}
					break;
				// 8: Blinn - blobby
				case 8:
				{
					result = 0;

					// Calculate the isosurface value for each point of the mesh
					for (array< float, 3 > point : data)
					{
						// point[0] = point.x
						// point[1] = point.y
						// point[2] = point.z
						r = pow((x - point[0]), 2) + pow((y - point[1]), 2) + pow((z - point[2]), 2);

						// distance r is already squared!
						result += T * pow(e, (B / pow(R, 2) * r - B));
					}
				}
				break;
				default:
					result = 0;
					break;
				}

				// Scale indexes from coordinates to natural numbers.
				ti = static_cast<int>((x + cond) / SCALE);
				tj = static_cast<int>((y + cond) / SCALE);
				tk = static_cast<int>((z + cond) / SCALE);

				m_isodata[ti][tj][tk] = result;

				// Check solution and if there was already a positive 
				// solution in the cell.
				if (m_isodata[ti][tj][tk] > T && !m_data[ti][tj][tk])
				{
					m_data[ti][tj][tk] = true;
				}
			}
		}
	}


	return;
}

/// ----------------------------------------------------------------------
/// Create polygons from prepared data.
/// ----------------------------------------------------------------------
void CModel::CreatePolygons()
{
	bool result = false;
	vector< array<float, 8> > triangles;
	SModel vertexData;
	int optionIndex;

	// There are no vertices yet.
	m_vertexCount = 0;

	for (int k = 0; k < RESOLUTION; k++)
	{
		for (int j = 0; j < RESOLUTION; j++)
		{
			for (int i = 0; i < RESOLUTION; i++)
			{
				optionIndex = 0;

				// Determine the option.
				// ZYX
				if (m_data[i][j][k])
				{
					optionIndex += 1;
				}

				if (m_data[i + 1][j][k])
				{
					optionIndex += 2;
				}

				if (m_data[i][j + 1][k])
				{
					optionIndex += 4;
				}

				if (m_data[i + 1][j + 1][k])
				{
					optionIndex += 8;
				}

				if (m_data[i][j][k + 1])
				{
					optionIndex += 16;
				}

				if (m_data[i + 1][j][k + 1])
				{
					optionIndex += 32;
				}

				if (m_data[i][j + 1][k + 1])
				{
					optionIndex += 64;
				}

				if (m_data[i + 1][j + 1][k + 1])
				{
					optionIndex += 128;
				}

				// Create triangles for given cube if possible.
				// triangles vector will contain 3*number of triangles records or
				// simply put, vertices, texture coords and normals.
				result = m_MarchingCube->Check(i, j, k, optionIndex /*m_data*/, triangles);
				
				if (!result)
				{
					// Unrecognized solution.
					break;
				}

				// Fill model with data from previous function.
				for (array<float, 8> data : triangles)
				{
					vertexData.x = data[0];
					vertexData.y = data[1];
					vertexData.z = data[2];

					vertexData.tu = data[3];
					vertexData.tv = data[4];

					vertexData.nx = data[5];
					vertexData.ny = data[6];
					vertexData.nz = data[7];

					m_vertexCount++;

					// Save vertex data from this cube.
					m_isoModel.push_back(vertexData);
				}
				
				// Clear triangles vector.
				triangles.clear();
			}
		}
	}

	return;
}

/// ----------------------------------------------------------------------
/// Creates the file parser object. Called during initialization.
/// ----------------------------------------------------------------------
bool CModel::InitializeFileParser()
{
	// Create the parser object.
	m_FileParser = new CFileParser;
	if (!m_FileParser)
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Releases the file parser object.
/// ----------------------------------------------------------------------
void CModel::ReleaseFileParser()
{
	if (m_FileParser)
	{
		delete m_FileParser;
		m_FileParser = nullptr;
	}

	return;
}

/// ----------------------------------------------------------------------
/// Creates the function parser object. Called during initialization.
/// ----------------------------------------------------------------------
bool CModel::InitializeFunctionParser()
{
	// Create the parser object.
	m_FunctionParser = new CFunctionParser;
	if (!m_FunctionParser)
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Releases the functionparser object.
/// ----------------------------------------------------------------------
void CModel::ReleaseFunctionParser()
{
	if (m_FunctionParser)
	{
		delete m_FunctionParser;
		m_FunctionParser = nullptr;
	}

	return;
}

/// ----------------------------------------------------------------------
/// Creates the marching cube object. Called during initialization.
/// ----------------------------------------------------------------------
bool CModel::InitializeMarchingCube()
{
	// Create the parser object.
	m_MarchingCube = new CMarchingCube;
	if (!m_MarchingCube)
	{
		return false;
	}

	return true;
}

/// ----------------------------------------------------------------------
/// Releases the functionparser object.
/// ----------------------------------------------------------------------
void CModel::ReleaseMarchingCube()
{
	if (m_MarchingCube)
	{
		delete m_MarchingCube;
		m_MarchingCube = nullptr;
	}

	return;
}