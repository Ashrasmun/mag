#pragma once

// Contains all the Direct3D functionality for setting up 
// and drawing 3D graphics in DirectX 11.
#pragma comment(lib, "d3d11.lib")

// Contains tools to interface with the hardware on the computer 
// to obtain information about the refresh rate of the monitor, 
// the video card being used, and so forth.
#pragma comment(lib, "dxgi.lib")

// Contains functionality for compiling shaders.
#pragma comment(lib, "d3dcompiler.lib")

#include <d3d11.h>
#include <directxmath.h>

using namespace DirectX;

/// ----------------------------------------------------------------------
/// Setting up Direct3D device, swap chain, depth stencil and rasterizer
/// ----------------------------------------------------------------------
class CDirect3D 
{
public:
	CDirect3D();
	CDirect3D(const CDirect3D&);
	~CDirect3D();

	bool Initialize(int, int, bool, HWND, bool, float, float);
	void Shutdown();

	void BeginScene(float, float, float, float);
	void EndScene();

	ID3D11Device *GetDevice();
	ID3D11DeviceContext *GetDeviceContext();

	void GetProjectionMatrix(XMMATRIX &);
	void GetWorldMatrix(XMMATRIX &);
	void GetOrthoMatrix(XMMATRIX &);

	void GetVideoCardInfo(char *, int &);

	// For 2D/3D rendering
	void TurnZBufferOn();
	void TurnZBufferOff();

	// For text rendering
	void TurnAlphaBlendingOn();
	void TurnAlphaBlendingOff();

private:
	bool m_vsync_enabled;
	int m_videoCardMemory;
	char m_videoCardDescription[128];

	IDXGISwapChain *m_swapChain;
	ID3D11Device *m_device;
	ID3D11DeviceContext *m_deviceContext;
	ID3D11RenderTargetView *m_renderTargetView;
	ID3D11Texture2D *m_depthStencilBuffer;
	ID3D11DepthStencilView *m_depthStencilView;

	ID3D11DepthStencilState *m_depthEnabledStencilState;
	ID3D11DepthStencilState *m_depthDisabledStencilState;
	ID3D11BlendState* m_alphaEnabledBlendingState;
	ID3D11BlendState* m_alphaDisabledBlendingState;
	
	ID3D11RasterizerState *m_rasterState;

	XMMATRIX m_projectionMatrix;
	XMMATRIX m_worldMatrix;
	XMMATRIX m_orthoMatrix;
	// m_viewMatrix is/will be located in CCamera class
};